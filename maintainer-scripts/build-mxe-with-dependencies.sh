#!/bin/sh

set -e

usage()
{
    echo "$basename($0) <Debug | Release>"
}

usage

BUILD_MODE=$1

if [ "x${BUILD_MODE}" = "x" ]
then
BUILD_MODE="Release"
fi


if [ ! ${BUILD_MODE} = "Debug" ]
then
echo "Not Debug"
if [ ! ${BUILD_MODE} = "Release" ]
then
echo "Not Release"
BUILD_MODE="Release"
fi
fi

echo "Build mode: ${BUILD_MODE}"

HOME_DEVEL_DIR="${HOME}/devel"

cd ${HOME_DEVEL_DIR} || exit 1

PAPPSO_MXE_BUILD_DIR="${HOME_DEVEL_DIR}/pappsomspp/build-area/mxe"
cd ${PAPPSO_MXE_BUILD_DIR}
printf "\nNow in $(pwd)\n"
printf "            Going to erase all the files: please, <ENTER> to continue or Ctl-C to stop.\n"
read message
rm -rf *
reset
clear
x86_64-w64-mingw32.shared-cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=${BUILD_MODE} ../../development -DMXE=1
cmake --build .
make dllinstall
echo "Done installing PAPPSO libs."


XPERTMASS_MXE_BUILD_DIR="${HOME_DEVEL_DIR}/xpertmass/build-area/mxe"
cd ${XPERTMASS_MXE_BUILD_DIR}
printf "\nNow in $(pwd)\n"
printf "            Going to erase all the files: please, <ENTER> to continue or Ctl-C to stop.\n"
read message
rm -rf *
reset
clear
x86_64-w64-mingw32.shared-cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=${BUILD_MODE} ../../development -DMXE=1
cmake --build .
make dllinstall
echo "Done installing XPERTMASS libs."


MASSXPERT_MXE_BUILD_DIR="${HOME_DEVEL_DIR}/massxpert2/build-area/mxe"
cd ${MASSXPERT_MXE_BUILD_DIR}
printf "\nNow in $(pwd)\n"
printf "            Going to erase all the files: please, <ENTER> to continue or Ctl-C to stop.\n"
read message
rm -rf *
reset
clear
x86_64-w64-mingw32.shared-cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=${BUILD_MODE} ../../development -DMXE=1
cmake --build .
make winpackage
echo "Done making the mineXpert2 MS Windows package."

exit 0


