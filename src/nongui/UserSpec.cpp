/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QDebug>
#include <QObject>
#include <QString>


/////////////////////// Std includes
#include <stdlib.h>


/////////////////////// Local includes
#include "UserSpec.hpp"


namespace MsXpS
{

namespace massxpert
{


  UserSpec::UserSpec()
  {
    return;
  }


  UserSpec::~UserSpec()
  {
    return;
  }


  void
  UserSpec::setUserName(const QString &userName)
  {
    // On UNIX-like: /home/<user>
    // On MS Window: C:/Users/<username>
    
    if(userName.isEmpty())
      {
        m_userName = getenv("USER");

        if(m_userName.isEmpty())
        {
          m_userName = getenv("USERNAME");

          if(m_userName.isEmpty())
            m_userName = QObject::tr("NOT_SET");

        }
      }
    else
      m_userName = userName;

    //qDebug() << "User name was detected: " << m_userName;
  }


  const QString &
  UserSpec::userName() const
  {
    return m_userName;
  }


  void
  UserSpec::setConfigDir(const QString &configDir)
  {
    m_configDir = configDir;
  }


  const QString &
  UserSpec::configDir() const
  {
    return m_configDir;
  }


  void
  UserSpec::setWorkDir(const QString &workDir)
  {
    m_workDir = workDir;
  }


  const QString &
  UserSpec::workDir() const
  {
    return m_workDir;
  }


  void
  UserSpec::setdataDir(const QString &dataDir)
  {
    m_dataDir = dataDir;
  }


  const QString &
  UserSpec::dataDir() const
  {
    return m_dataDir;
  }

} // namespace massxpert

} // namespace MsXpS
