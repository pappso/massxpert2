/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QDebug>
#include <QStringList>
#include <QRegularExpression>
#include <QString>


/////////////////////// Local includes
#include "ConfigSetting.hpp"


namespace MsXpS
{

namespace massxpert
{


ConfigSetting::ConfigSetting(const QString &applicationName)
  : m_applicationName{applicationName}
{
}


ConfigSetting::ConfigSetting(const ConfigSetting &other)
{
  m_applicationName = other.m_applicationName;
  m_prefixDelimiter = other.m_prefixDelimiter;

  m_userType = other.m_userType;
  m_key      = other.m_key;
  m_value    = other.m_value;

  m_title   = other.m_title;
  m_comment = other.m_comment;
}


ConfigSetting &
ConfigSetting::operator=(const ConfigSetting &other)
{
  if(&other == this)
    return *this;

  m_applicationName = other.m_applicationName;
  m_prefixDelimiter = other.m_prefixDelimiter;

  m_userType = other.m_userType;
  m_key      = other.m_key;
  m_value    = other.m_value;

  m_title   = other.m_title;
  m_comment = other.m_comment;

  return *this;
}


ConfigSetting::~ConfigSetting()
{
}


void
ConfigSetting::setApplicationName(const QString &applicationName)
{
  m_applicationName = applicationName;
}


QString
ConfigSetting::getApplicationName()
{
  return m_applicationName;
}


QString
ConfigSetting::text() const
{
  QString text = m_title;
  text += "\n";

  if(m_userType == UserType::USER_TYPE_USER)
    text += "User type : user\n";
  else
    text += "User type : system\n";

  text += QString("%1 = %2\n").arg(m_key).arg(m_value.toString());

  text += m_comment;
  text += "\n";

  return text;
}


} // namespace massxpert

} // namespace MsXpS
