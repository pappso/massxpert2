/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef MASS_LIST_HPP
#define MASS_LIST_HPP


/////////////////////// Qt includes
#include <QtGlobal>
#include <QStringList>
#include <QDebug>


namespace MsXpS {


	namespace massxpert
	{


class MassList
{
  private:
  QString m_name;
  QString m_comment;

  QString m_massText;
  QStringList m_massStringList;
  QList<double> m_massList;

  public:
  MassList(QString);
  MassList(QString, const QString &);
  MassList(QString, const QStringList &);
  MassList(QString, const QList<double> &);
  MassList(const MassList &);

  ~MassList();

  MassList &operator=(const MassList &);

  void setName(QString);
  QString name();

  void setComment(QString);
  const QString &comment() const;

  int size() const;

  const QList<double> &massList() const;

  double at(int) const;

  const QString &massText() const;

  int makeMassList();
  int makeMassText();

  void sortAscending();
  void sortDescending();

  void applyMass(double);
  void removeLessThan(double);
};

} // namespace massxpert

} // namespace MsXpS


#endif // MASS_LIST_HPP
