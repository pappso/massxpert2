/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Local includes
#include <libXpertMass/Oligomer.hpp>


namespace MsXpS
{

namespace massxpert
{


class CleaveOligomer : public libXpertMass::Oligomer
{

  friend class Cleaver;

  public:
  CleaveOligomer(libXpertMass::Polymer *,
                 const QString &name,
                 const QString &description,
                 bool modified,
                 const libXpertMass::Ponderable &  = libXpertMass::Ponderable(),
                 int startIndex               = -1,
                 int endIndex                 = -1,
                 libXpertMass::Formula             = libXpertMass::Formula(),
                 int partialCleavage          = 0,
                 const libXpertMass::CalcOptions & = libXpertMass::CalcOptions());

  CleaveOligomer(libXpertMass::Polymer *,
                 const QString &name,
                 const QString &description,
                 bool modified,
                 const libXpertMass::Ponderable &  = libXpertMass::Ponderable(),
                 int startIndex               = -1,
                 int endIndex                 = -1,
                 int partialCleavage          = 0,
                 const libXpertMass::CalcOptions & = libXpertMass::CalcOptions());

  CleaveOligomer(libXpertMass::Polymer *polymer,
                 const QString &name,
                 const QString &description,
                 bool modified,
                 const Ponderable &ponderable,
                 const libXpertMass::IonizeRule &ionizeRule,
                 const libXpertMass::CalcOptions &calcOptions,
                 bool isIonized,
                 int startIndex,
                 int endIndex,
                 int partialCleavage);

  CleaveOligomer(libXpertMass::Polymer *,
                 const QString &name,
                 const QString &description,
                 bool modified,
                 double mono                  = 0,
                 double avg                   = 0,
                 int startIndex               = -1,
                 int endIndex                 = -1,
                 int partialCleavage          = 0,
                 const libXpertMass::CalcOptions & = libXpertMass::CalcOptions());

  CleaveOligomer(const CleaveOligomer &);

  void setPartialCleavage(int);
  int partialCleavage() const;

  virtual QString elementalComposition() override;

  protected:
  libXpertMass::Formula m_formula;
  int m_partialCleavage;
};

typedef std::shared_ptr<CleaveOligomer> CleaveOligomerSPtr;
typedef std::shared_ptr<const CleaveOligomer> CleaveOligomerCstSPtr;

} // namespace massxpert

} // namespace MsXpS

