/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef FRAGMENTER_HPP
#define FRAGMENTER_HPP


/////////////////////// Local includes
#include <libXpertMass/FragSpec.hpp>
#include "FragOptions.hpp"
#include <libXpertMass/CalcOptions.hpp>
#include <libXpertMass/Polymer.hpp>
#include <libXpertMass/IonizeRule.hpp>
#include "OligomerList.hpp"
#include "CrossLinkedRegion.hpp"
#include <libXpertMass/PolChemDefEntity.hpp>

namespace MsXpS
{


namespace massxpert
{


class Fragmenter
{

  public:
  Fragmenter(libXpertMass::Polymer *,
             libXpertMass::PolChemDefCstSPtr,
             const QList<FragOptions *> &fragOptionList,
             const libXpertMass::CalcOptions &,
             const libXpertMass::IonizeRule &);

  Fragmenter(const Fragmenter &);
  ~Fragmenter();

  void addFragOptions(FragOptions *);

  void setOligomerList(OligomerList *);
  OligomerList *oligomerList();

  bool fragment();
  int fragmentEndNone(FragOptions &);
  int fragmentEndLeft(FragOptions &);
  int fragmentEndRight(FragOptions &);

  bool
  accountFragRule(libXpertMass::FragRule *, bool, int, int, libXpertMass::Ponderable *);
  int accountFormulas(OligomerList *, FragOptions &, QString, int);
  OligomerList *accountIonizationLevels(OligomerList *, FragOptions &);

  void emptyOligomerList();

  private:
  libXpertMass::Polymer *mp_polymer;
  libXpertMass::PolChemDefCstSPtr mcsp_polChemDef;
  QList<FragOptions *> m_fragOptionList;
  libXpertMass::CalcOptions m_calcOptions;
  libXpertMass::IonizeRule m_ionizeRule;

  // Pointer to an oligomer list which WE DO NOT OWN.
  OligomerList *mp_oligomerList;

  // A list of CrossLinkedRegion instances that we compute in case
  // there are cross-links in the fragmented sequence.
  QList<CrossLinkedRegion *> m_crossLinkedRegionList;
};

} // namespace massxpert

} // namespace MsXpS


#endif // FRAGMENTER_HPP
