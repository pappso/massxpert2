/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Local includes
#include "CleaveOligomer.hpp"


namespace MsXpS
{

namespace massxpert
{


CleaveOligomer::CleaveOligomer(libXpertMass::Polymer *polymer,
                               const QString &name,
                               const QString &description,
                               bool modified,
                               const libXpertMass::Ponderable &ponderable,
                               int startIndex,
                               int endIndex,
                               libXpertMass::Formula formula,
                               int partialCleavage,
                               const libXpertMass::CalcOptions &calcOptions)
  : libXpertMass::Oligomer(polymer,
                      name,
                      description,
                      modified,
                      ponderable,
                      startIndex,
                      endIndex,
                      calcOptions),
    m_formula(formula),
    m_partialCleavage(partialCleavage)
{
}


CleaveOligomer::CleaveOligomer(libXpertMass::Polymer *polymer,
                               const QString &name,
                               const QString &description,
                               bool modified,
                               const libXpertMass::Ponderable &ponderable,
                               int startIndex,
                               int endIndex,
                               int partialCleavage,
                               const libXpertMass::CalcOptions &calcOptions)
  : libXpertMass::Oligomer(polymer,
                      name,
                      description,
                      modified,
                      ponderable,
                      startIndex,
                      endIndex,
                      calcOptions),
    m_partialCleavage(partialCleavage)
{
}


CleaveOligomer::CleaveOligomer(libXpertMass::Polymer *polymer,
                               const QString &name,
                               const QString &description,
                               bool modified,
                               const Ponderable &ponderable,
                               const libXpertMass::IonizeRule &ionizeRule,
                               const libXpertMass::CalcOptions &calcOptions,
                               bool isIonized,
                               int startIndex,
                               int endIndex,
                               int partialCleavage)
  : libXpertMass::Oligomer(polymer,
                      name,
                      description,
                      modified,
                      ponderable,
                      ionizeRule,
                      calcOptions,
                      isIonized,
                      startIndex,
                      endIndex),
    m_partialCleavage(partialCleavage)
{
}


CleaveOligomer::CleaveOligomer(libXpertMass::Polymer *polymer,
                               const QString &name,
                               const QString &description,
                               bool modified,
                               double mono,
                               double avg,
                               int startIndex,
                               int endIndex,
                               int partialCleavage,
                               const libXpertMass::CalcOptions &calcOptions)
  : libXpertMass::Oligomer(polymer,
                      name,
                      description,
                      modified,
                      mono,
                      avg,
                      startIndex,
                      endIndex,
                      calcOptions),
    m_partialCleavage(partialCleavage)
{
}


CleaveOligomer::CleaveOligomer(const CleaveOligomer &other)
  : libXpertMass::Oligomer(other),
    m_formula(other.m_formula),
    m_partialCleavage(other.m_partialCleavage)
{
}


void
CleaveOligomer::setPartialCleavage(int value)
{
  m_partialCleavage = value;
}


int
CleaveOligomer::partialCleavage() const
{
  return m_partialCleavage;
}


QString
CleaveOligomer::elementalComposition()
{
  // qDebug();
  return m_formula.toString();
}

} // namespace massxpert

} // namespace MsXpS
