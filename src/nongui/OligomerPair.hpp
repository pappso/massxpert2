/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// Qt includes
#include <QString>


/////////////////////// Local includes
#include <libXpertMass/globals.hpp>
#include <libXpertMass/PropListHolder.hpp>
#include <libXpertMass/Oligomer.hpp>

namespace MsXpS
{

namespace massxpert
{


class OligomerPair : public libXpertMass::PropListHolder
{
  protected:
  const libXpertMass::OligomerSPtr msp_oligomer1;
  const libXpertMass::OligomerSPtr msp_oligomer2;
  libXpertMass::MassType m_massType;
  double m_error;
  bool m_isMatching;
  QString m_name;

  public:
  OligomerPair(const libXpertMass::OligomerSPtr,
               const libXpertMass::OligomerSPtr,
               libXpertMass::MassType,
               double          = 0,
               bool isMatching = false,
               const QString & = QString());

  OligomerPair(const OligomerPair &);

  virtual ~OligomerPair();

  QString name();

  const libXpertMass::OligomerSPtr oligomer1() const;
  const libXpertMass::OligomerSPtr oligomer2() const;

  void setMassType(libXpertMass::MassType);
  libXpertMass::MassType massType() const;

  void setError(double);
  double error() const;

  void setMatching(bool);
  bool isMatching() const;

  double mass1();
  int charge1();

  double mass2();
  int charge2();
};

typedef std::shared_ptr<OligomerPair> OligomerPairSPtr;
typedef std::shared_ptr<const OligomerPair> OligomerPairCstSPtr;


} // namespace massxpert

} // namespace MsXpS

