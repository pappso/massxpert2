/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Local includes
#include "CleaveOptions.hpp"
#include <libXpertMass/PolChemDefEntity.hpp>


namespace MsXpS
{

namespace massxpert
{


CleaveOptions::CleaveOptions(libXpertMass::PolChemDefCstSPtr polChemDefCstSPtr,
                             QString name,
                             QString pattern,
                             int partials,
                             bool sequenceEmbedded)
  : libXpertMass::CleaveSpec(polChemDefCstSPtr, name, pattern),
    m_partials(partials),
    m_sequenceEmbedded(sequenceEmbedded)
{
  // Have to be 1 and not 0, otherwise the setting functions ensuring
  // that m_startIonizeLevel>=m_endIonizeLevel will bug.
  m_startIonizeLevel = 1;
  m_endIonizeLevel   = 1;
}


CleaveOptions::CleaveOptions(const libXpertMass::CleaveSpec &cleavespec,
                             int partials,
                             bool sequenceEmbedded)
  : libXpertMass::CleaveSpec(cleavespec),
    m_partials(partials),
    m_sequenceEmbedded(sequenceEmbedded)
{
  // Have to be 1 and not 0, otherwise the setting functions ensuring
  // that m_startIonizeLevel>=m_endIonizeLevel will bug.
  m_startIonizeLevel = 1;
  m_endIonizeLevel   = 1;
}


CleaveOptions::CleaveOptions(const CleaveOptions &other)
  : libXpertMass::CleaveSpec(other),
    m_partials(other.m_partials),
    m_startIonizeLevel(other.m_startIonizeLevel),
    m_endIonizeLevel(other.m_endIonizeLevel),
    m_sequenceEmbedded(other.m_sequenceEmbedded)
{
}


CleaveOptions::~CleaveOptions()
{
}


CleaveOptions &
CleaveOptions::operator=(const CleaveOptions &other)
{
  if(&other == this)
    return *this;

  libXpertMass::CleaveSpec::operator=(other);

  m_partials         = other.m_partials;
  m_startIonizeLevel = other.m_startIonizeLevel;
  m_endIonizeLevel   = other.m_endIonizeLevel;
  m_sequenceEmbedded = other.m_sequenceEmbedded;

  return *this;
}

void
CleaveOptions::setPartials(int value)
{
  Q_ASSERT(value >= 0);

  m_partials = value;
}

int
CleaveOptions::partials() const
{
  return m_partials;
}


void
CleaveOptions::setStartIonizeLevel(int value)
{
  int local = (value < 0) ? abs(value) : value;

  if(local <= m_endIonizeLevel)
    {
      m_startIonizeLevel = local;
    }
  else
    {
      m_startIonizeLevel = m_endIonizeLevel;
      m_endIonizeLevel   = local;
    }
}


int
CleaveOptions::startIonizeLevel() const
{
  return m_startIonizeLevel;
}


void
CleaveOptions::setEndIonizeLevel(int value)
{
  int local = (value < 0) ? abs(value) : value;

  if(local > m_startIonizeLevel)
    {
      m_endIonizeLevel = local;
    }
  else
    {
      m_startIonizeLevel = m_endIonizeLevel;
      m_endIonizeLevel   = local;
    }
}


int
CleaveOptions::endIonizeLevel() const
{
  return m_endIonizeLevel;
}


void
CleaveOptions::setIonizeLevels(int value1, int value2)
{
  if(abs(value1) <= abs(value2))
    {
      m_startIonizeLevel = abs(value1);
      m_endIonizeLevel   = abs(value2);
    }
  else
    {
      m_startIonizeLevel = abs(value2);
      m_endIonizeLevel   = abs(value1);
    }
}


void
CleaveOptions::setSequenceEmbedded(bool value)
{
  m_sequenceEmbedded = value;
}


bool
CleaveOptions::isSequenceEmbedded() const
{
  return m_sequenceEmbedded;
}

} // namespace massxpert

} // namespace MsXpS
