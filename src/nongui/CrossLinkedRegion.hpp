/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef CROSS_LINKED_REGION_HPP
#define CROSS_LINKED_REGION_HPP
//#warning "Entering CROSS_LINKED_REGION_HPP"


/////////////////////// Local includes
#include <libXpertMass/CrossLink.hpp>


namespace MsXpS {


	namespace massxpert
	{



class CrossLinkedRegion
{
  private:
  int m_startIndex;
  int m_endIndex;
  // We do not own the cross-links, we cannot destroy them because
  // that would destroy the cross-links in the polymer sequence,
  // which we do not want!!
  QList<libXpertMass::CrossLink *> m_crossLinkList;


  public:
  CrossLinkedRegion();
  CrossLinkedRegion(int, int);
  CrossLinkedRegion(const CrossLinkedRegion &);

  ~CrossLinkedRegion();

  void setStartIndex(int);
  int startIndex();

  void setEndIndex(int);
  int endIndex();

  const QList<libXpertMass::CrossLink *> &crossLinkList() const;

  int appendCrossLink(libXpertMass::CrossLink *);
  int appendCrossLinks(const QList<libXpertMass::CrossLink *> &);

  int removeCrossLink(libXpertMass::CrossLink *);
  int removeCrossLinkAt(int);
};

} // namespace massxpert

} // namespace MsXpS


#endif // CROSS_LINKED_REGION_HPP
