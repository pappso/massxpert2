/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef CONFIG_SETTING_HPP
#define CONFIG_SETTING_HPP

/////////////////////// Qt includes
#include <QString>
#include <QMap>


/////////////////////// Local includes
#include "globals.hpp"

namespace MsXpS
{

namespace massxpert
{


class ConfigSetting
{
  private:
  QString m_applicationName;
  QChar m_prefixDelimiter = gConfigKeyStringPrefixDelimiter;

  public:
  // Is the key for user or system config settings (USER_TYPE_USER ||
  // USER_TYPE_SYSTEM)?
  int m_userType;

  // The key is a string like dataDir or polChemDefsDir or docDir...
  QString m_key;

  // The value can be bool, int, double or QString.
  QVariant m_value;

  QString m_title;
  QString m_comment;

  ConfigSetting(const QString &applicationName);
  ConfigSetting(const ConfigSetting &other);
  ConfigSetting &operator=(const ConfigSetting &other);
  ~ConfigSetting();

  void setApplicationName(const QString &applicationName);
  QString getApplicationName();

  QString text() const;
};

} // namespace massxpert

} // namespace MsXpS


#endif /* CONFIG_SETTING_HPP */
