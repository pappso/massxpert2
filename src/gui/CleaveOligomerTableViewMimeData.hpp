/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef CLEAVE_OLIGOMER_TABLE_VIEW_MIME_DATA_HPP
#define CLEAVE_OLIGOMER_TABLE_VIEW_MIME_DATA_HPP


/////////////////////// Qt includes
#include <QMimeData>


/////////////////////// Local includes
#include "CleaveOligomerTableView.hpp"
#include "SequenceEditorWnd.hpp"
#include "CleavageDlg.hpp"


namespace MsXpS
{

	namespace massxpert
	{



class CleaveOligomerTableViewMimeData : public QMimeData
{
  Q_OBJECT

  private:
  const CleaveOligomerTableView *mp_tableView;
  SequenceEditorWnd *mp_editorWnd;
  CleavageDlg *mp_cleavageDlg;

  QStringList m_formats;


  QString selectionAsPlainText() const;

  protected:
  QVariant retrieveData(const QString &, QMetaType) const;

  public:
  CleaveOligomerTableViewMimeData(const CleaveOligomerTableView *,
                                  SequenceEditorWnd *,
                                  CleavageDlg *);
  ~CleaveOligomerTableViewMimeData();

  const CleaveOligomerTableView *tableView() const;
  QStringList formats() const;
};

} // namespace massxpert

} // namespace MsXpS


#endif // CLEAVE_OLIGOMER_TABLE_VIEW_MIME_DATA_HPP
