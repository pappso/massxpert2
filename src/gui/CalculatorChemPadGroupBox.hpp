/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef CALCULATOR_CHEM_PAD_GROUPBOX_HPP
#define CALCULATOR_CHEM_PAD_GROUPBOX_HPP


/////////////////////// Qt includes
#include <QGroupBox>
#include <QList>
#include <QVBoxLayout>
#include <QFrame>
#include <QGridLayout>

/////////////////////// Local includes
#include "ChemPadButton.hpp"


namespace MsXpS
{

	namespace massxpert
	{



class CalculatorChemPadGroupBox : public QGroupBox
{
  Q_OBJECT

  private:
  QList<QWidget *> m_widgetList;
  QVBoxLayout *mpa_vboxLayout;
  QFrame *mpa_frame;
  QGridLayout *mpa_gridLayout;


  public:
  CalculatorChemPadGroupBox(QWidget * = 0);
  CalculatorChemPadGroupBox(const QString &, QWidget * = 0);

  void addChemPadButton(ChemPadButton *, int, int);

  ~CalculatorChemPadGroupBox();

  public slots:
  void toggled(bool);
};

} // namespace massxpert

} // namespace MsXpS


#endif // CALCULATOR_CHEM_PAD_GROUPBOX_HPP
