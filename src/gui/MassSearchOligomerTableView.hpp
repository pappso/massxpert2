/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef MASS_SEARCH_OLIGOMER_TABLE_VIEW_HPP
#define MASS_SEARCH_OLIGOMER_TABLE_VIEW_HPP


/////////////////////// Qt includes
#include <QTableView>


/////////////////////// Local includes
#include "../nongui/OligomerList.hpp"


namespace MsXpS
{

	namespace massxpert
	{



class MassSearchDlg;

class MassSearchOligomerTableView : public QTableView
{
  Q_OBJECT

  private:
  MassSearchDlg *mp_parentDlg;

  OligomerList *mp_oligomerList;

  // For drag operations.
  QPoint m_dragStartPos;

  // We do not want to hide the parent class function here.
  using QAbstractItemView::startDrag;
  void startDrag();

  ///////// Contextual menu for copying to clipboard of mono/avg
  ///////// masses.
  QMenu *contextMenu;
  QAction *copyMonoAct;
  QAction *copyAvgAct;
  /////////

  protected:
  void currentChanged(const QModelIndex &, const QModelIndex &);
  void copyMassList(int);

  public:
  MassSearchOligomerTableView(QWidget *parent = 0);
  ~MassSearchOligomerTableView();

  void setOligomerList(OligomerList *);
  const OligomerList *oligomerList();

  void setParentDlg(MassSearchDlg *);
  MassSearchDlg *parentDlg();

  int selectedOligomers(OligomerList *, int = 0) const;
  QString *selectedOligomersAsPlainText(
    QString         = "$",
    bool            = false,
    bool            = false,
    libXpertMass::MassType = libXpertMass::MassType::MASS_MONO) const;

  void mousePressEvent(QMouseEvent *);
  void mouseMoveEvent(QMouseEvent *);

  public slots:
  void itemActivated(const QModelIndex &);
  void copyMono();
  void copyAvg();
};

} // namespace massxpert

} // namespace MsXpS


#endif // MASS_SEARCH_OLIGOMER_TABLE_VIEW_HPP
