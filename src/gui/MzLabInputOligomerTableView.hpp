/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */

#pragma once


/////////////////////// Qt includes
#include <QTableView>


/////////////////////// libmass includes
#include <libXpertMass/Oligomer.hpp>


/////////////////////// Local includes
#include "../nongui/OligomerList.hpp"



namespace MsXpS
{

	namespace massxpert
	{



class MzLabWnd;
class MzLabInputOligomerTableViewModel;
class MzLabInputOligomerTableViewDlg;


class MzLabInputOligomerTableView : public QTableView
{
  Q_OBJECT

  private:
  MzLabInputOligomerTableViewDlg *mp_parentDlg;
  MzLabWnd *mp_mzLabWnd;

  MzLabInputOligomerTableViewModel *mp_sourceModel;

  // We do not own that oligomer list.
  OligomerList *mp_oligomerList;

  int testCleaveOligomerDataDropped(QDropEvent *, OligomerList *);
  int testMassSearchOligomerDataDropped(QDropEvent *, OligomerList *);
  int testSimpleTextDataDropped(QDropEvent *, OligomerList *);
  int testSimpleTextDataDroppedOrPasted(QString &, OligomerList *);

  protected:
  void dragEnterEvent(QDragEnterEvent *event);
  void dragMoveEvent(QDragMoveEvent *event);
  void dropEvent(QDropEvent *event);
  libXpertMass::MassType pasteOrDropMassTypeCheck();

  public:
  MzLabInputOligomerTableView(QWidget *parent = 0);
  ~MzLabInputOligomerTableView();

  void setSourceModel(MzLabInputOligomerTableViewModel *);
  MzLabInputOligomerTableViewModel *sourceModel();

  void setOligomerList(OligomerList *);
  const OligomerList *oligomerList();

  void setParentDlg(MzLabInputOligomerTableViewDlg *);
  MzLabInputOligomerTableViewDlg *parentDlg();

  void setMzLabWnd(MzLabWnd *);
  MzLabWnd *mzLabWnd();

  void mousePressEvent(QMouseEvent *);
  void pasteEvent();

  QString *selectedDataAsPlainText();

  libXpertMass::OligomerSPtr parseOnlyMassLine(QStringList &);
  libXpertMass::OligomerSPtr parseMassAndChargeLine(QStringList &);
  libXpertMass::OligomerSPtr parseFullLine(QStringList &);

  public slots:
  void itemActivated(const QModelIndex &);
};

} // namespace massxpert

} // namespace MsXpS

