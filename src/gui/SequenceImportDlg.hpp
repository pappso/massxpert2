/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef SEQUENCE_IMPORT_DLG_HPP
#define SEQUENCE_IMPORT_DLG_HPP

/////////////////////// Qt includes
#include <QStringList>


/////////////////////// Local includes
#include "ui_SequenceImportDlg.h"
#include "AbstractSeqEdWndDependentDlg.hpp"
#include <libXpertMass/Sequence.hpp>
#include "SequenceEditorWnd.hpp"


namespace MsXpS
{

namespace massxpert
{


  class SequenceImportDlg : public AbstractSeqEdWndDependentDlg
  {
    Q_OBJECT

    private:
    Ui::SequenceImportDlg m_ui;

    // These are pointers that we not own.
    QStringList *mpa_chainSequenceStringList;
    QStringList *mpa_chainIdStringList;

    public:
    SequenceImportDlg(SequenceEditorWnd *editorWnd,
                      /* no libXpertMass::Polymer **/
                      /* no libXpertMass::PolChemDef **/
                      const QString &settingsFilePath,
                      const QString &applicationName,
                      const QString &description,
                      QStringList *chaingStringList,
                      QStringList *chainIdStringList);

    ~SequenceImportDlg();

    bool initialize();

    public slots:
    void listWidgetSelectionChanged();
  };

} // namespace massxpert

} // namespace MsXpS


#endif // SEQUENCE_IMPORT_DLG_HPP
