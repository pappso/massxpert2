/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes


/////////////////////// Local includes
#include "CleaveOligomerTableViewMimeData.hpp"


namespace MsXpS
{

namespace massxpert
{


  CleaveOligomerTableViewMimeData::CleaveOligomerTableViewMimeData(
    const CleaveOligomerTableView *tableView,
    SequenceEditorWnd *editorWnd,
    CleavageDlg *cleavageDlg)
    : mp_tableView(tableView),
      mp_editorWnd(editorWnd),
      mp_cleavageDlg(cleavageDlg)
  {
    m_formats << "text/plain";
  }


  CleaveOligomerTableViewMimeData::~CleaveOligomerTableViewMimeData()
  {
  }


  const CleaveOligomerTableView *
  CleaveOligomerTableViewMimeData::tableView() const
  {
    return mp_tableView;
  }


  QStringList
  CleaveOligomerTableViewMimeData::formats() const
  {
    return m_formats;
  }

  QVariant
  CleaveOligomerTableViewMimeData::retrieveData(const QString &format,
                                                QMetaType preferredType) const
  {
    if(format == "text/plain")
      {
        return selectionAsPlainText();
      }
    else
      {
        return QMimeData::retrieveData(format, preferredType);
      }
  }


  QString
  CleaveOligomerTableViewMimeData::selectionAsPlainText() const
  {
    // We just do an export in the form of a simple text. Only the
    // selected items should go in the export.
    QString *text = mp_tableView->selectedOligomersAsPlainText();

    QString returnedString = *text;

    delete text;

    return returnedString;
  }


} // namespace massxpert

} // namespace MsXpS
