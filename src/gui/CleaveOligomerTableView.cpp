/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QApplication>
#include <QDrag>
#include <QDebug>
#include <QMouseEvent>
#include <QMessageBox>
#include <QFileInfo>


/////////////////////// Local includes
#include "../nongui/globals.hpp"
#include "CleaveOligomerTableView.hpp"
#include "CleaveOligomerTableViewSortProxyModel.hpp"
#include <libXpertMass/Oligomer.hpp>
#include "CleavageDlg.hpp"
#include "CleaveOligomerTableViewMimeData.hpp"


namespace MsXpS
{

namespace massxpert
{


CleaveOligomerTableView::CleaveOligomerTableView(QWidget *parent)
  : QTableView(parent)
{

  setAlternatingRowColors(true);

  setSortingEnabled(true);
  setDragEnabled(true);

  connect(this,
          SIGNAL(activated(const QModelIndex &)),
          this,
          SLOT(itemActivated(const QModelIndex &)));


  QHeaderView *headerView = horizontalHeader();
  headerView->setSectionsClickable(true);
  headerView->setSectionsMovable(true);

  ////// Create the actions for the contextual menu.

  // Copy Mono
  copyMonoAct = new QAction(tr("Copy Mono To Clipboard"), this);
  copyMonoAct->setStatusTip(
    tr("Copies the monoisotopic mass list "
       "to the clipboard"));
  connect(copyMonoAct, SIGNAL(triggered()), this, SLOT(copyMono()));

  // Copy Avg
  copyAvgAct = new QAction(tr("Copy Avg To Clipboard"), this);
  copyMonoAct->setStatusTip(
    tr("Copies the average mass list "
       "to the clipboard"));
  connect(copyAvgAct, SIGNAL(triggered()), this, SLOT(copyAvg()));

  // And now create the contextual menu and add the actions to it.
  contextMenu = new QMenu(tr("Copy Mass List"), this);
  contextMenu->addAction(copyMonoAct);
  contextMenu->addAction(copyAvgAct);
}


CleaveOligomerTableView::~CleaveOligomerTableView()
{
}


void
CleaveOligomerTableView::setOligomerList(OligomerList *oligomerList)
{
  mp_oligomerList = oligomerList;
}


const OligomerList *
CleaveOligomerTableView::oligomerList()
{
  return mp_oligomerList;
}


CleavageDlg *
CleaveOligomerTableView::parentDlg()
{
  return mp_parentDlg;
}

void
CleaveOligomerTableView::setParentDlg(CleavageDlg *dlg)
{
  Q_ASSERT(dlg);
  mp_parentDlg = dlg;
}


int
CleaveOligomerTableView::selectedOligomers(OligomerList *oligomerList,
                                           int index) const
{
  if(!oligomerList)
    qFatal("Fatal error at %s@%d. Aborting.", __FILE__, __LINE__);

  int count = 0;

  int localIndex = 0;

  // How many oligomers are there in the list passed as argument?
  int oligomerCount = oligomerList->size();

  if(index > oligomerCount)
    qFatal("Fatal error at %s@%d. Aborting.", __FILE__, __LINE__);

  // If index is -1 , then we are asked to append the oligomers to
  // the list.
  if(index == -1)
    localIndex = oligomerList->size();

  // For each selected oligomer, duplicate it and append to the list
  // passed as argument.

  // We first have to get the selection model for the proxy model.

  QItemSelectionModel *selModel = selectionModel();

  // Now get the selection ranges.

  QItemSelection proxyItemSelection = selModel->selection();

  QSortFilterProxyModel *sortModel =
    static_cast<QSortFilterProxyModel *>(model());

  QItemSelection sourceItemSelection =
    sortModel->mapSelectionToSource(proxyItemSelection);

  QModelIndexList modelIndexList = sourceItemSelection.indexes();

  int modelIndexListSize = modelIndexList.size();

  // Attention, if we select one single row, our modelIndexList will
  // be of size 7, because in one single row there are seven cells:
  // each cell for each column, and there are 7 columns. Thus, when
  // we iterate in the modelIndexList, we'll have to take care of
  // this and make sure we are not putting each selected row's
  // oligomer seven times. For this, we make sure we are not
  // handling the same row twice or more, by storing the processed
  // rows in a list of integers and by checking for existence of
  // that row each time a new index is processed.

  QList<int> processedRowList;

  for(int iter = 0; iter < modelIndexListSize; ++iter)
    {
      QModelIndex oligomerIndex = modelIndexList.at(iter);

      Q_ASSERT(oligomerIndex.isValid());

      // Get to know what's the row of the index, so that we can get
      // to the oligomer.

      int row = oligomerIndex.row();

      if(processedRowList.contains(row))
        continue;
      else
        processedRowList.append(row);

      // Older version
      // CleaveOligomer *oligomer =
      // static_cast<CleaveOligomer *>(mp_oligomerList->at(row).get());
      // CleaveOligomerSPtr new_oligomer_sp =
      // std::make_shared<CleaveOligomer>(*oligomer);

      CleaveOligomerSPtr oligomer_sp =
        std::dynamic_pointer_cast<CleaveOligomer>(mp_oligomerList->at(row));

      CleaveOligomerSPtr new_oligomer_sp =
        std::make_shared<CleaveOligomer>(*oligomer_sp);

      // Create a NoDeletePointerProp, which might be used later by
      // the user of the list of oligomers to highlight regions in
      // the sequence editor.

      libXpertMass::NoDeletePointerProp *prop = new libXpertMass::NoDeletePointerProp(
        "SEQUENCE_EDITOR_WND", static_cast<void *>(mp_parentDlg->editorWnd()));

      new_oligomer_sp->appendProp(prop);

      oligomerList->insert(localIndex, new_oligomer_sp);

      ++localIndex;
      ++count;
    }

  return count;
}


QString *
CleaveOligomerTableView::selectedOligomersAsPlainText(
  const QString &pattern,
  QString delimiter,
  bool withSequence,
  bool forXpertMiner,
  libXpertMass::MassType massType) const
{
  // Let's get all the currently selected oligomers in one list.
  OligomerList oligomerList;

  // Append the selected oligomers to the empty list.
  int appendedOligomerCount = selectedOligomers(&oligomerList, -1);

  // Sanity check
  if(appendedOligomerCount != oligomerList.size())
    qFatal("Fatal error at %s@%d. Aborting.", __FILE__, __LINE__);

  // There are two possibilities:
  // 1. The pattern string is not empty, in which case it should be used to
  // craft the output.
  // 2. The pattern string is empty, in which case the delimiter is used to
  // craft a default generally-usable output.

  bool withPattern;

  if(pattern.isEmpty() || forXpertMiner)
    {
      withPattern = false;

      // If delimiter is empty, then set to "$".
      if(delimiter.isEmpty())
        delimiter = "$";
    }
  else
    {
      withPattern = true;
    }

  // The text that we will return, that must be allocated on the heap.
  QString *output = new QString;

  // Slot to get the text from the called functions.
  QString *text = nullptr;

  while(!oligomerList.isEmpty())
    {
      // libXpertMass::OligomerSPtr oligomer_sp = oligomerList.takeFirst();

      CleaveOligomerSPtr oligomer_sp =
        std::dynamic_pointer_cast<CleaveOligomer>(oligomerList.takeFirst());

      if(withPattern)
        {
          text =
            selectedOligomersAsPlainTextWithPattern(oligomer_sp.get(), pattern);
          if(text == nullptr)
            qFatal(
              "Fatal error at %s@%d. Program aborted.", __FILE__, __LINE__);

          *output += *text;
          delete text;
        }
      else
        {
          text = selectedOligomersAsPlainTextWithDelimiter(oligomer_sp.get(),
                                                           delimiter,
                                                           withSequence,
                                                           forXpertMiner,
                                                           massType);
          if(text == nullptr)
            qFatal(
              "Fatal error at %s@%d. Program aborted.", __FILE__, __LINE__);

          *output += *text;
          delete text;
        }

      // At this point we have done the work for the current oligomer, make
      // sure we delete it now.

      oligomer_sp.reset();
    }

  // Terminate the string with a new line.
  *output += QString("\n");

  return output;
}


QString *
CleaveOligomerTableView::selectedOligomersAsPlainTextWithDelimiter(
  libXpertMass::Oligomer *p_oligomer,
  const QString &delimiter,
  bool withSequence,
  bool forXpertMiner,
  libXpertMass::MassType massType) const
{
  if(p_oligomer == nullptr)
    qFatal("Fatal error at %s@%d. Program aborted.", __FILE__, __LINE__);

  // For export to XpertMiner, we only want the masses asked for:
  // libXpertMass::MassType::MASS_MONO or MASS_AVG. Also, we want the format to be
  // :

  // mass <delim> charge <delim> name <delim> coords

  QString *text = new QString;

  if(!forXpertMiner)
    *text += QString("\n%1%2").arg(p_oligomer->description()).arg(delimiter);

  if(forXpertMiner && massType == libXpertMass::MassType::MASS_AVG)
    {
    }
  else
    {
      *text += QString("%1%2")
                 .arg(p_oligomer->monoString(libXpertMass::OLIGOMER_DEC_PLACES))
                 .arg(delimiter);
    }

  if(forXpertMiner && massType == libXpertMass::MassType::MASS_MONO)
    {
    }
  else
    {
      *text += QString("%1%2")
                 .arg(p_oligomer->avgString(libXpertMass::OLIGOMER_DEC_PLACES))
                 .arg(delimiter);
    }

  *text += QString("%1%2").arg(p_oligomer->charge()).arg(delimiter);

  *text += QString("%1%2").arg(p_oligomer->name()).arg(delimiter);

  *text +=
    QString("%1%2")
      .arg(
        static_cast<libXpertMass::CoordinateList *>(p_oligomer)->positionsAsText())
      .arg(delimiter);

  if(!forXpertMiner)
    *text += QString("%1%2").arg(p_oligomer->isModified()).arg(delimiter);

  // We cannot export the sequence if data are for XpertMiner
  if(!forXpertMiner && withSequence)
    {
      QString *sequence = p_oligomer->monomerText();

      *text += QString("\n%1").arg(*sequence);

      delete sequence;
    }

  // Terminate the stanza
  *text += QString("\n");

  return text;
}


QString *
CleaveOligomerTableView::selectedOligomersAsPlainTextWithPattern(
  libXpertMass::Oligomer *p_oligomer, const QString &pattern) const
{
  // We receive a pattern that we use to craft the output. The following
  // shortcuts are used:
  // %f - filename of mxp file
  // %m - mono mass
  // %a - avg mass
  // %z - charge
  // %s - sequence
  // %c - coordinates
  // %e - enzyme
  // %o - modif (boolean)
  // %n - oligomer name
  // %p - partial
  // %i - polymer sequence name (identity)

  if(p_oligomer == nullptr)
    qFatal("Fatal error at %s@%d. Program aborted.", __FILE__, __LINE__);

  QString *text = new QString;

  QChar prevChar = ' ';

  for(int iter = 0; iter < pattern.size(); ++iter)
    {
      QChar curChar = pattern.at(iter);

      // qDebug() << __FILE__ << __LINE__
      // << "Current char:" << curChar;

      if(curChar == '\\')
        {
          if(prevChar == '\\')
            {
              *text += '\\';
              prevChar = ' ';
              continue;
            }
          else
            {
              prevChar = '\\';
              continue;
            }
        }

      if(curChar == '%')
        {
          if(prevChar == '\\')
            {
              *text += '%';
              prevChar = ' ';
              continue;
            }
          else
            {
              prevChar = '%';
              continue;
            }
        }

      if(curChar == 'n')
        {
          if(prevChar == '\\')
            {
              *text += QString("\n");

              // Because a newline only works if it is followed by something,
              // and if the user wants a termination newline, then we need to
              // duplicate that new line char if we are at the end of the
              // string.

              if(iter == pattern.size() - 1)
                {
                  // This is the last character of the line, then, duplicate
                  // the newline so that it actually creates a new line in the
                  // text.

                  *text += QString("\n");
                }

              prevChar = ' ';
              continue;
            }
        }

      if(prevChar == '%')
        {
          // The current character might have a specific signification.
          if(curChar == 'f')
            {
              QFileInfo fileInfo(
                mp_parentDlg->editorWnd()->polymer()->filePath());
              if(fileInfo.exists())
                *text += fileInfo.fileName();
              else
                *text += "Untitled";
              prevChar = ' ';
              continue;
            }
          if(curChar == 'm')
            {
              *text += p_oligomer->monoString(libXpertMass::OLIGOMER_DEC_PLACES);
              prevChar = ' ';
              continue;
            }
          if(curChar == 'a')
            {
              *text += p_oligomer->avgString(libXpertMass::OLIGOMER_DEC_PLACES);
              prevChar = ' ';
              continue;
            }
          if(curChar == 'z')
            {
              *text += QString("%1").arg(p_oligomer->charge());
              prevChar = ' ';
              continue;
            }
          if(curChar == 's')
            {
              QString *sequence = p_oligomer->monomerText();

              *text += *sequence;

              delete sequence;

              prevChar = ' ';
              continue;
            }
          if(curChar == 'l')
            {
              QString *sequence      = p_oligomer->monomerText();
              QString elidedSequence = libXpertMass::elideText(*sequence);
              delete sequence;
              *text += elidedSequence;

              prevChar = ' ';
              continue;
            }
          if(curChar == 'c')
            {
              *text += static_cast<libXpertMass::CoordinateList *>(p_oligomer)
                         ->positionsAsText();
              prevChar = ' ';
              continue;
            }
          if(curChar == 'e')
            {
              CleaveOligomer *cleaveOligomer =
                static_cast<CleaveOligomer *>(p_oligomer);

              *text += cleaveOligomer->description();
              prevChar = ' ';
              continue;
            }
          if(curChar == 'o')
            {
              *text += QString("%1").arg(p_oligomer->isModified());
              prevChar = ' ';
              continue;
            }
          if(curChar == 'n')
            {
              *text += p_oligomer->name();
              prevChar = ' ';
              continue;
            }
          if(curChar == 'p')
            {
              CleaveOligomer *cleaveOligomer =
                static_cast<CleaveOligomer *>(p_oligomer);

              *text += QString("%1").arg(cleaveOligomer->partialCleavage());
              prevChar = ' ';
              continue;
            }
          if(curChar == 'i')
            {
              *text += mp_parentDlg->editorWnd()->polymer()->name();
              prevChar = ' ';
              continue;
            }

          // At this point the '%' is not followed by any special character
          // above, so we skip them both from the text. If the '%' is to be
          // printed, then it needs to be escaped.

          continue;
        }
      // End of
      // if(prevChar == '%')

      // The character prior this current one was not '%' so we just append
      // the current character.
      *text += curChar;
    }
  // End of
  // for (int iter = 0; iter < pattern.size(); ++iter)

  return text;
}


void
CleaveOligomerTableView::mousePressEvent(QMouseEvent *mouseEvent)
{
  if(mouseEvent->buttons() & Qt::LeftButton)
    {
      m_dragStartPos = mouseEvent->pos();
    }
  else if(mouseEvent->buttons() & Qt::RightButton)
    {
      contextMenu->popup(mouseEvent->globalPosition().toPoint());
      return;
    }

  QTableView::mousePressEvent(mouseEvent);
}


void
CleaveOligomerTableView::mouseMoveEvent(QMouseEvent *mouseEvent)
{
  if(mouseEvent->buttons() & Qt::LeftButton)
    {
      int distance = (mouseEvent->pos() - m_dragStartPos).manhattanLength();

      if(distance >= QApplication::startDragDistance())
        {
          startDrag();
          return;
        }
    }

  QTableView::mousePressEvent(mouseEvent);
}


void
CleaveOligomerTableView::startDrag()
{
  CleaveOligomerTableViewMimeData *mimeData =
    new CleaveOligomerTableViewMimeData(
      this, mp_parentDlg->editorWnd(), mp_parentDlg);

  QDrag *drag = new QDrag(this);
  drag->setMimeData(mimeData);
  //    drag->setPixmap(QPixmap(":/images/greenled.png"));
  drag->exec(Qt::CopyAction);
}


void
CleaveOligomerTableView::currentChanged(const QModelIndex &current,
                                        const QModelIndex &previous)
{
  if(!current.isValid())
    return;

  CleaveOligomerTableViewSortProxyModel *sortModel =
    static_cast<CleaveOligomerTableViewSortProxyModel *>(model());

  QModelIndex sourceIndex = sortModel->mapToSource(current);

  int row = sourceIndex.row();

  // Get to the list of oligomers that is referenced in this
  // tableView (that list actually belongs to the CleavageDlg
  // instance.

  CleaveOligomerSPtr oligomer_sp =
    std::dynamic_pointer_cast<CleaveOligomer>(mp_oligomerList->at(row));

  // If the oligomers obtained with the cleavage are old and the
  // sequence has been changed since the cleavage, then the
  // oligomers might point to a sequence element that is no more. We
  // want to avoid such kind of errors.

  if(oligomer_sp->startIndex() >= oligomer_sp->polymer()->size() ||
     oligomer_sp->endIndex() >= oligomer_sp->polymer()->size())
    {
      QMessageBox::warning(this,
                           tr("massXpert - Cleavage"),
                           tr("%1@%2\n"
                              "The monomer indices do not correspond "
                              "to a valid polymer sequence range.\n"
                              "Avoid modifying the sequence while "
                              "working with cleavages.")
                             .arg(__FILE__)
                             .arg(__LINE__),
                           QMessageBox::Ok);

      return;
    }

  QString *text = oligomer_sp->monomerText();

  // We are getting text for an oligomer; it cannot be empty,
  // because that would mean the oligomer has no monomers. In that
  // case it is not conceivable that the oligomer be in the cleavage
  // product list.

  Q_ASSERT(!text->isEmpty());

  *text += QString(" -- %1").arg(oligomer_sp->description());

  // Get the formula of the oligomer and display it all along.

  QString formula = oligomer_sp->elementalComposition();

  *text += QString(" -- Formula: %1").arg(formula);

  mp_parentDlg->updateOligomerSequence(text);

  delete text;

  // Get the mass calculation engine's options out of the oligomer,
  // so that we can display them correctly.

  libXpertMass::CalcOptions calcOptions = oligomer_sp->calcOptions();

  mp_parentDlg->updateCleavageDetails(calcOptions);

  QTableView::currentChanged(current, previous);
}


void
CleaveOligomerTableView::itemActivated(const QModelIndex &index)
{
  if(!index.isValid())
    return;

  CleaveOligomerTableViewSortProxyModel *sortModel =
    static_cast<CleaveOligomerTableViewSortProxyModel *>(model());

  QModelIndex sourceIndex = sortModel->mapToSource(index);

  int row = sourceIndex.row();

  // Get to the list of oligomers that is referenced in this
  // tableView (that list actually belongs to the CleavageDlg
  // instance.

  CleaveOligomer *oligomer =
    static_cast<CleaveOligomer *>(mp_oligomerList->at(row).get());

  SequenceEditorWnd *editorWnd = mp_parentDlg->editorWnd();

  libXpertMass::CoordinateList *coordinateList =
    static_cast<libXpertMass::CoordinateList *>(oligomer);

  // Remove the previous selection, so that we can start fresh.
  editorWnd->mpa_editorGraphicsView->resetSelection();

  for(int iter = 0; iter < coordinateList->size(); ++iter)
    {
      libXpertMass::Coordinates *coordinates = coordinateList->at(iter);

      int start = coordinates->start();
      int end   = coordinates->end();

      if(start >= oligomer->polymer()->size() ||
         end >= oligomer->polymer()->size())
        {
          QMessageBox::warning(this,
                               tr("massXpert - Cleavage"),
                               tr("%1@%2\n"
                                  "The monomer indices do not correspond "
                                  "to a valid polymer sequence range.\n"
                                  "Avoid modifying the sequence while "
                                  "working with cleavages.")
                                 .arg(__FILE__)
                                 .arg(__LINE__),
                               QMessageBox::Ok);

          return;
        }

      editorWnd->mpa_editorGraphicsView->setSelection(
        *coordinates, true, false);
    }

  editorWnd->updateSelectedSequenceMasses();
}


///////// Contextual menu for copying to clipboard of mono/avg
///////// masses.
void
CleaveOligomerTableView::copyMono()
{
  return copyMassList(libXpertMass::MassType::MASS_MONO);
}


void
CleaveOligomerTableView::copyAvg()
{
  return copyMassList(libXpertMass::MassType::MASS_AVG);
}


void
CleaveOligomerTableView::copyMassList(int monoOrAvg)
{
  QString massList;

  // We want to prepare a textual list of masses (either MONO or
  // AVG) of all the oligomers in the tableview, exactly as they are
  // currently displayed (that is, according to the proxy's model).

  QSortFilterProxyModel *sortModel =
    static_cast<QSortFilterProxyModel *>(model());

  // Get number of rows under the model.
  int rowCount = sortModel->rowCount();

  for(int iter = 0; iter < rowCount; ++iter)
    {
      // qDebug() << __FILE__ << __LINE__
      //          << "proxyIter:" << iter;

      QModelIndex proxyIndex  = sortModel->index(iter, 0);
      QModelIndex sourceIndex = sortModel->mapToSource(proxyIndex);

      int sourceRow = sourceIndex.row();

      // qDebug() << __FILE__ << __LINE__
      //          << "sourceRow:" << sourceRow;

      CleaveOligomer *oligomer =
        static_cast<CleaveOligomer *>(mp_oligomerList->at(sourceRow).get());

      if(monoOrAvg == libXpertMass::MassType::MASS_MONO)
        massList += oligomer->monoString(libXpertMass::OLIGOMER_DEC_PLACES);
      else if(monoOrAvg == libXpertMass::MassType::MASS_AVG)
        massList += oligomer->avgString(libXpertMass::OLIGOMER_DEC_PLACES);
      else
        qFatal("Fatal error at %s@%d. Aborting.", __FILE__, __LINE__);

      // End the mass item with a new line.
      massList += "\n";
    }

  if(massList.isEmpty())
    return;

  QClipboard *clipboard = QApplication::clipboard();

  clipboard->setText(massList, QClipboard::Clipboard);
}

} // namespace massxpert

} // namespace MsXpS
