/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QDialog>
#include <QDebug>
#include <QSettings>


/////////////////////// Local includes
#include "CalculatorRecorderDlg.hpp"
#include "CalculatorWnd.hpp"


namespace MsXpS
{

namespace massxpert
{


  CalculatorRecorderDlg::CalculatorRecorderDlg(
    QWidget *parent, const QString &configSettingsFilePath)
    : QDialog{parent}, m_configSettingsFilePath{configSettingsFilePath}
  {
    m_ui.setupUi(this);
    setSizeGripEnabled(true);

    m_ui.textEdit->append(
      tr("Calculator recorder\n"
         "===================\n\n"));

    QSettings settings(m_configSettingsFilePath, QSettings::IniFormat);

    restoreGeometry(
      settings.value("calculator_recorder_dlg/geometry").toByteArray());

    connect(this, SIGNAL(rejected()), this, SLOT(close()));
  }


  CalculatorRecorderDlg::~CalculatorRecorderDlg()
  {
  }


  void
  CalculatorRecorderDlg::record(const QString &string)
  {
    m_ui.textEdit->append(string);
  }


  ////////////////////////// SLOTS ///////////////////////////
  void
  CalculatorRecorderDlg::close()
  {
    // Before closing set the parent checkbox to proper state.
    CalculatorWnd *wnd = static_cast<CalculatorWnd *>(parentWidget());

    QSettings settings(m_configSettingsFilePath, QSettings::IniFormat);

    settings.setValue("calculator_recorder_dlg/geometry", saveGeometry());

    wnd->recorderDlgClosed();
  }

} // namespace massxpert

} // namespace MsXpS
