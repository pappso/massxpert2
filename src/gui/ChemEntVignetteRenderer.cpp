/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QDebug>


/////////////////////// Local includes
#include "ChemEntVignetteRenderer.hpp"


namespace MsXpS
{

namespace massxpert
{

  ChemEntVignetteRenderer::ChemEntVignetteRenderer(
    QObject *parent, QHash<QString, ChemEntVignetteRenderer *> *cevrHash)
    : QSvgRenderer(parent), mp_hash(cevrHash)
  {
    //   qDebug() << __FILE__ << __LINE__
    // 	    << "ChemEntVignetteRenderer()" << this;

    m_refCount = 0;
  }


  ChemEntVignetteRenderer::ChemEntVignetteRenderer(
    const QString &filePath,
    QObject *parent,
    QHash<QString, ChemEntVignetteRenderer *> *cevrHash)
    : QSvgRenderer(filePath, parent), mp_hash(cevrHash)
  {
    m_refCount = 0;
  }


  ChemEntVignetteRenderer::ChemEntVignetteRenderer(
    const QByteArray &contents,
    QObject *parent,
    QHash<QString, ChemEntVignetteRenderer *> *cevrHash)
    : QSvgRenderer(contents, parent), mp_hash(cevrHash)
  {
    m_refCount = 0;
  }


  ChemEntVignetteRenderer::~ChemEntVignetteRenderer()
  {
    //   qDebug() << __FILE__ << __LINE__
    // 	    << "~ChemEntVignetteRenderer()" << this;
  }


  void
  ChemEntVignetteRenderer::setHash(
    QHash<QString, ChemEntVignetteRenderer *> *cevrHash)
  {
    Q_ASSERT(cevrHash);

    mp_hash = cevrHash;
  }


  void
  ChemEntVignetteRenderer::setRefCount(int value)
  {
    m_refCount = value;
  }


  int
  ChemEntVignetteRenderer::refCount()
  {
    return m_refCount;
  }


  void
  ChemEntVignetteRenderer::incrementRefCount()
  {
    //   qDebug() << __FILE__ << __LINE__
    // 	    << "ChemEntVignetteRenderer::incrementRefCount()" << this
    // 	    << "New value:" << m_refCount + 1;

    ++m_refCount;
  }


  void
  ChemEntVignetteRenderer::decrementRefCount()
  {
    //   qDebug() << __FILE__ << __LINE__
    // 	    << "ChemEntVignetteRenderer::decrementRefCount()" << this
    // 	    << "New value:" << m_refCount - 1;

    --m_refCount;

    if(!m_refCount)
      {
        // If this renderer was referenced in a hash, then remove it
        // from the hash.

        if(mp_hash)
          {
            QString key = mp_hash->key(this);

            int res = mp_hash->remove(key);

            if(res != 1)
              qFatal(
                "Fatal error at %s@%d. Program aborted.", __FILE__, __LINE__);

            delete this;
          }
      }
  }

} // namespace massxpert

} // namespace MsXpS
