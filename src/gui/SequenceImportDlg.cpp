/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QMessageBox>


/////////////////////// Local includes
#include "SequenceImportDlg.hpp"


namespace MsXpS
{

namespace massxpert
{


  enum
  {
    NONE  = 0 << 1,
    LEFT  = 1 << 1,
    RIGHT = 2 << 1,
  };

  SequenceImportDlg::SequenceImportDlg(SequenceEditorWnd *editorWnd,
                                       /* no libXpertMass::Polymer **/
                                       /* no libXpertMass::PolChemDef **/
                                       const QString &settingsFilePath,
                                       const QString &applicationName,
                                       const QString &description,
                                       QStringList *chainStringList,
                                       QStringList *chainIdStringList)
    : AbstractSeqEdWndDependentDlg(editorWnd,
                                   0 /* polymer **/,
                                   0 /*polChemDef **/,
                                   settingsFilePath,
                                   "SequenceImportDlg",
                                   applicationName,
                                   description),
      mpa_chainSequenceStringList(chainStringList),
      mpa_chainIdStringList(chainIdStringList)
  {
    // We are taking ownership of the strings, thus the variable name is
    // mpa_<name>
    if(!mpa_chainSequenceStringList || !mpa_chainIdStringList)
      qFatal("Fatal error at %s@%d. Program aborted.", __FILE__, __LINE__);

    if(!initialize())
      qFatal("Fatal error at %s@%d. Program aborted.", __FILE__, __LINE__);
  }


  SequenceImportDlg::~SequenceImportDlg()
  {
    delete mpa_chainIdStringList;
    delete mpa_chainSequenceStringList;
  }


  bool
  SequenceImportDlg::initialize()
  {
    m_ui.setupUi(this);

    setAttribute(Qt::WA_DeleteOnClose);

    connect(m_ui.importedSequencesListWidget,
            SIGNAL(itemSelectionChanged()),
            this,
            SLOT(listWidgetSelectionChanged()));

    if(mpa_chainSequenceStringList->size() != mpa_chainIdStringList->size())
      {
        QMessageBox::critical(
          this,
          tr("massXpert - libXpertMass::Polymer libXpertMass::Sequence Importer"),
          tr("Failed to import the sequence, "
             "The number of chain IDs is different from "
             "the number of chain sequences."),
          QMessageBox::Ok);

        return true;
      }

    // If the lists are empty we have nothing to do.
    if(!mpa_chainSequenceStringList->size())
      return true;

    //  Now populate the ID string list widget.

    QString iterString;

    for(int iter = 0; iter < mpa_chainSequenceStringList->size(); ++iter)
      {
        // First add a new item in the list widget.
        iterString = mpa_chainIdStringList->at(iter);
        m_ui.importedSequencesListWidget->addItem(iterString);
      }

    // Select the first ID item, of which we'll display the
    // corresponding sequence.

    m_ui.importedSequencesListWidget->setCurrentRow(0);

    // And now add the string to the text edit widget.
    iterString = mpa_chainSequenceStringList->at(0);
    m_ui.importedSequencesTextEdit->setPlainText(iterString);

    return true;
  }


  void
  SequenceImportDlg::listWidgetSelectionChanged()
  {
    // Get the index of the currently selected item.
    int rowIndex = m_ui.importedSequencesListWidget->currentRow();

    // And now put the corresponding string to the text edit widget.
    QString chainSequence = mpa_chainSequenceStringList->at(rowIndex);

    //     qDebug() << __FILE__ << __LINE__
    // 	     << "Current item index:" << rowIndex << "\n"
    // 	     << "Current sequence:"
    // 	     << chainSequence;

    m_ui.importedSequencesTextEdit->clear();
    m_ui.importedSequencesTextEdit->setPlainText(chainSequence);
  }


} // namespace massxpert

} // namespace MsXpS
