/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Local includes
#include "MzCalculationTreeViewItem.hpp"


namespace MsXpS
{

namespace massxpert
{


  MzCalculationTreeViewItem::MzCalculationTreeViewItem(
    const QList<QVariant> &data, MzCalculationTreeViewItem *parent)
  {
    mp_parentItem = parent;
    m_itemData    = data;
    mp_ionizable  = 0;
  }


  MzCalculationTreeViewItem::~MzCalculationTreeViewItem()
  {
    qDeleteAll(m_childItemsList);
  }


  MzCalculationTreeViewItem *
  MzCalculationTreeViewItem::parent()
  {
    return mp_parentItem;
  }


  void
  MzCalculationTreeViewItem::appendChild(MzCalculationTreeViewItem *item)
  {
    m_childItemsList.append(item);
  }


  void
  MzCalculationTreeViewItem::insertChild(int index,
                                         MzCalculationTreeViewItem *item)
  {
    m_childItemsList.insert(index, item);
  }


  MzCalculationTreeViewItem *
  MzCalculationTreeViewItem::child(int row)
  {
    return m_childItemsList.value(row);
  }


  MzCalculationTreeViewItem *
  MzCalculationTreeViewItem::takeChild(int row)
  {
    MzCalculationTreeViewItem *item = m_childItemsList.takeAt(row);

    return item;
  }


  const QList<MzCalculationTreeViewItem *> &
  MzCalculationTreeViewItem::childItems()
  {
    return m_childItemsList;
  }


  int
  MzCalculationTreeViewItem::childCount() const
  {
    return m_childItemsList.count();
  }


  int
  MzCalculationTreeViewItem::columnCount() const
  {
    return m_itemData.count();
  }


  QVariant
  MzCalculationTreeViewItem::data(int column) const
  {
    return m_itemData.value(column);
  }


  bool
  MzCalculationTreeViewItem::setData(int column, const QVariant &value)
  {
    m_itemData[column].setValue(value.toString());

    return true;
  }


  int
  MzCalculationTreeViewItem::row() const
  {
    if(mp_parentItem)
      return mp_parentItem->m_childItemsList.indexOf(
        const_cast<MzCalculationTreeViewItem *>(this));

    return 0;
  }


  void
  MzCalculationTreeViewItem::setIonizable(libXpertMass::Ionizable *ionizable)
  {
    Q_ASSERT(ionizable);
    mp_ionizable = ionizable;
  }


  libXpertMass::Ionizable *
  MzCalculationTreeViewItem::ionizable()
  {
    return mp_ionizable;
  }

} // namespace massxpert

} // namespace MsXpS
