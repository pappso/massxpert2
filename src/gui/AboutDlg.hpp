/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes
#include <QDialog>
#include <QGraphicsScene>


/////////////////////// Local includes
#include "ui_AboutDlg.h"


namespace MsXpS
{
namespace massxpert
{


class AboutDlg : public QDialog
{
  Q_OBJECT

		friend class ProgramWindow;

  private:
  public:
  AboutDlg(QWidget *parent, const QString &applicationName = "massXpert");

  ~AboutDlg();

  void setApplicationFeaturesText();
  void makeUserManualCommandLinkConnections();
  void setHistoryText();

  protected:
  QString m_applicationName;

  Ui::AboutDlg m_ui;

  // Allocated but ownership taken by the graphicsView.
  QGraphicsScene *mp_graphicsScene;

  void setupGraphicsView();
  void showHowToCiteTab();
};


} // namespace massxpert

} // namespace MsXpS

