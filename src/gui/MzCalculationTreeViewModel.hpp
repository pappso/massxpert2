/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef MZ_CALCULATION_TREEVIEW_MODEL_HPP
#define MZ_CALCULATION_TREEVIEW_MODEL_HPP


/////////////////////// Qt includes
#include <QTreeView>


/////////////////////// Local includes
#include "MzCalculationTreeViewItem.hpp"
#include "MzCalculationDlg.hpp"
#include <libXpertMass/Ionizable.hpp>

namespace MsXpS
{

	namespace massxpert
	{



class MzCalculationTreeViewItem;
class MzCalculationDlg;


class MzCalculationTreeViewModel : public QAbstractItemModel
{
  Q_OBJECT

  private:
  QList<libXpertMass::Ionizable *> *mp_list;
  MzCalculationTreeViewItem *mpa_rootItem;
  QTreeView *mp_treeView;
  MzCalculationDlg *mp_parentDlg;

  public:
  MzCalculationTreeViewModel(QList<libXpertMass::Ionizable *> *, QObject *);

  ~MzCalculationTreeViewModel();

  MzCalculationDlg *getParentDlg();

  void setTreeView(QTreeView *);
  QTreeView *treeView();

  QVariant data(const QModelIndex &, int) const;

  QVariant headerData(int, Qt::Orientation, int = Qt::DisplayRole) const;

  Qt::ItemFlags flags(const QModelIndex &) const;

  QModelIndex index(int, int, const QModelIndex & = QModelIndex()) const;

  QModelIndex parent(const QModelIndex &) const;

  int rowCount(const QModelIndex & = QModelIndex()) const;
  int columnCount(const QModelIndex & = QModelIndex()) const;

  void removeAll();

  void addIonizable(libXpertMass::Ionizable *);
  bool setupModelData(MzCalculationTreeViewItem *);
};

} // namespace massxpert

} // namespace MsXpS


#endif // MZ_CALCULATION_TREEVIEW_MODEL_HPP
