/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


namespace MsXpS
{

	namespace massxpert

	{

#include <QByteArray>

QByteArray defaultModifVignette(
  "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n"
  "<!-- Created with Sodipodi(\"http://www.sodipodi.com/\") -->\n"
  "<svg\n"
  "   xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n"
  "   xmlns:cc=\"http://web.resource.org/cc/\"\n"
  "   xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
  "   xmlns:svg=\"http://www.w3.org/2000/svg\"\n"
  "   xmlns=\"http://www.w3.org/2000/svg\"\n"
  "   xmlns:sodipodi=\"http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd\"\n"
  "   xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\"\n"
  "   version=\"1\"\n"
  "   x=\"0\"\n"
  "   y=\"0\"\n"
  "   width=\"125\"\n"
  "   height=\"125\"\n"
  "   id=\"svg101\"\n"
  "   sodipodi:version=\"0.32\"\n"
  "   sodipodi:docname=\"default-modif-vignette.svg\"\n"
  "   "
  "sodipodi:docbase=\"/home/rusconi/devel/massxpert/data/polChemDefs/"
  "protein-1-letter\"\n"
  "   inkscape:version=\"0.44.1\">\n"
  "  <metadata\n"
  "     id=\"metadata6\">\n"
  "    <rdf:RDF>\n"
  "      <cc:Work\n"
  "         rdf:about=\"\">\n"
  "        <dc:format>image/svg+xml</dc:format>\n"
  "        <dc:type\n"
  "           rdf:resource=\"http://purl.org/dc/dcmitype/StillImage\" />\n"
  "      </cc:Work>\n"
  "    </rdf:RDF>\n"
  "  </metadata>\n"
  "  <sodipodi:namedview\n"
  "     id=\"base\"\n"
  "     showguides=\"true\"\n"
  "     inkscape:guide-bbox=\"true\"\n"
  "     inkscape:zoom=\"3.024\"\n"
  "     inkscape:cx=\"62.5\"\n"
  "     inkscape:cy=\"62.5\"\n"
  "     inkscape:window-width=\"810\"\n"
  "     inkscape:window-height=\"576\"\n"
  "     inkscape:window-x=\"860\"\n"
  "     inkscape:window-y=\"30\"\n"
  "     inkscape:current-layer=\"svg101\" />\n"
  "  <defs\n"
  "     id=\"defs103\" />\n"
  "  <path\n"
  "     "
  "style=\"font-size:50px;font-style:normal;font-variant:normal;font-weight:"
  "bold;font-stretch:normal;text-align:start;line-height:125%;writing-mode:lr-"
  "tb;text-anchor:start;fill:lime;fill-opacity:1;stroke:none;stroke-width:1px;"
  "stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:"
  "Bitstream Vera Sans\"\n"
  "     d=\"M 108.53444,32.338169 L 99.720963,32.338169 L 99.720963,31.14188 C "
  "99.720954,29.807259 99.989508,28.627248 100.52663,27.601841 C "
  "101.06373,26.560192 102.19491,25.241834 103.92018,23.646763 L "
  "105.48268,22.230747 C 106.4104,21.384416 107.08586,20.586891 "
  "107.50905,19.838169 C 107.94848,19.089497 108.16821,18.3408 "
  "108.16823,17.592075 C 108.16821,16.452781 107.77759,15.565737 "
  "106.99635,14.930943 C 106.21509,14.279931 105.12459,13.954411 "
  "103.72487,13.95438 C 102.4065,13.954411 100.98235,14.231103 "
  "99.452408,14.784458 C 97.922453,15.321597 96.327403,16.12726 "
  "94.667252,17.20145 L 94.667252,9.5354347 C 96.636647,8.8518768 "
  "98.435148,8.34732 100.06276,8.0217628 C 101.69035,7.696279 "
  "103.26099,7.5335187 104.77467,7.5334816 C 108.74601,7.5335187 "
  "111.77335,8.34732 113.8567,9.9748878 C 115.94001,11.586249 "
  "116.98168,13.946273 116.9817,17.054966 C 116.98168,18.650044 "
  "116.6643,20.082334 116.02956,21.351841 C 115.39477,22.605118 "
  "114.31241,23.956028 112.78249,25.404575 L 111.21999,26.796177 C "
  "110.1132,27.805308 109.38891,28.61911 109.04713,29.237583 C "
  "108.70532,29.839812 108.53442,30.507129 108.53444,31.239536 L "
  "108.53444,32.338169 M 99.720963,35.95145 L 108.53444,35.95145 L "
  "108.53444,44.642857 L 99.720963,44.642857 L 99.720963,35.95145\"\n"
  "     id=\"text1869\" />\n"
  "  <path\n"
  "     "
  "style=\"font-size:50px;font-style:normal;font-variant:normal;font-weight:"
  "bold;font-stretch:normal;text-align:start;line-height:125%;writing-mode:lr-"
  "tb;text-anchor:start;fill:#ccc;fill-opacity:1;stroke:none;stroke-width:1px;"
  "stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:"
  "Bitstream Vera Sans\"\n"
  "     d=\"M 108.53444,104.33817 L 99.720963,104.33817 L 99.720963,103.14188 "
  "C 99.720954,101.80726 99.989508,100.62724 100.52663,99.601837 C "
  "101.06373,98.560189 102.19491,97.241831 103.92018,95.646759 L "
  "105.48268,94.230743 C 106.4104,93.384412 107.08586,92.586887 "
  "107.50905,91.838165 C 107.94848,91.089493 108.16821,90.340796 "
  "108.16823,89.592072 C 108.16821,88.452777 107.77759,87.565733 "
  "106.99635,86.930939 C 106.21509,86.279927 105.12459,85.954407 "
  "103.72487,85.954376 C 102.4065,85.954407 100.98235,86.231099 "
  "99.452408,86.784454 C 97.922453,87.321593 96.327403,88.127256 "
  "94.667252,89.201447 L 94.667252,81.535431 C 96.636647,80.851873 "
  "98.435148,80.347316 100.06276,80.021759 C 101.69035,79.696275 "
  "103.26099,79.533515 104.77467,79.533478 C 108.74601,79.533515 "
  "111.77335,80.347316 113.8567,81.974884 C 115.94001,83.586245 "
  "116.98168,85.946269 116.9817,89.054962 C 116.98168,90.65004 "
  "116.6643,92.08233 116.02956,93.351837 C 115.39477,94.605114 "
  "114.31241,95.956025 112.78249,97.404572 L 111.21999,98.796173 C "
  "110.1132,99.805305 109.38891,100.61911 109.04713,101.23758 C "
  "108.70532,101.83981 108.53442,102.50712 108.53444,103.23953 L "
  "108.53444,104.33817 M 99.720963,107.95145 L 108.53444,107.95145 L "
  "108.53444,116.64285 L 99.720963,116.64285 L 99.720963,107.95145\"\n"
  "     id=\"text1874\" />\n"
  "  <path\n"
  "     "
  "style=\"font-size:50px;font-style:normal;font-variant:normal;font-weight:"
  "bold;font-stretch:normal;text-align:start;line-height:125%;writing-mode:lr-"
  "tb;text-anchor:start;fill:red;fill-opacity:1;stroke:none;stroke-width:1px;"
  "stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:"
  "Bitstream Vera Sans\"\n"
  "     d=\"M 22.534439,32.338169 L 13.720963,32.338169 L 13.720963,31.14188 C "
  "13.720954,29.807259 13.989508,28.627248 14.526627,27.601841 C "
  "15.063726,26.560192 16.19491,25.241834 17.920181,23.646763 L "
  "19.482681,22.230747 C 20.4104,21.384416 21.085856,20.586891 "
  "21.509048,19.838169 C 21.948485,19.089497 22.168211,18.3408 "
  "22.168228,17.592075 C 22.168211,16.452781 21.777587,15.565737 "
  "20.996353,14.930943 C 20.215088,14.279931 19.124594,13.954411 "
  "17.724869,13.95438 C 16.406498,13.954411 14.982346,14.231103 "
  "13.452408,14.784458 C 11.922453,15.321597 10.327403,16.12726 "
  "8.6672516,17.20145 L 8.6672516,9.5354347 C 10.636647,8.8518768 "
  "12.435148,8.34732 14.062759,8.0217628 C 15.690353,7.696279 "
  "17.26099,7.5335187 18.774673,7.5334816 C 22.74601,7.5335187 "
  "25.773351,8.34732 27.856705,9.9748878 C 29.940013,11.586249 "
  "30.981679,13.946273 30.981705,17.054966 C 30.981679,18.650044 "
  "30.664296,20.082334 30.029556,21.351841 C 29.394766,22.605118 "
  "28.312411,23.956028 26.782486,25.404575 L 25.219986,26.796177 C "
  "24.113196,27.805308 23.388913,28.61911 23.047134,29.237583 C "
  "22.70532,29.839812 22.534422,30.507129 22.534439,31.239536 L "
  "22.534439,32.338169 M 13.720963,35.95145 L 22.534439,35.95145 L "
  "22.534439,44.642857 L 13.720963,44.642857 L 13.720963,35.95145\"\n"
  "     id=\"text1878\" />\n"
  "  <path\n"
  "     "
  "style=\"font-size:50px;font-style:normal;font-variant:normal;font-weight:"
  "bold;font-stretch:normal;text-align:start;line-height:125%;writing-mode:lr-"
  "tb;text-anchor:start;fill:blue;fill-opacity:1;stroke:none;stroke-width:1px;"
  "stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:"
  "Bitstream Vera Sans\"\n"
  "     d=\"M 22.534439,104.33817 L 13.720963,104.33817 L 13.720963,103.14188 "
  "C 13.720954,101.80726 13.989508,100.62724 14.526627,99.601837 C "
  "15.063726,98.560189 16.19491,97.241831 17.920181,95.646759 L "
  "19.482681,94.230743 C 20.4104,93.384412 21.085856,92.586887 "
  "21.509048,91.838165 C 21.948485,91.089493 22.168211,90.340796 "
  "22.168228,89.592072 C 22.168211,88.452777 21.777587,87.565733 "
  "20.996353,86.930939 C 20.215088,86.279927 19.124594,85.954407 "
  "17.724869,85.954376 C 16.406498,85.954407 14.982346,86.231099 "
  "13.452408,86.784454 C 11.922453,87.321593 10.327403,88.127256 "
  "8.6672516,89.201447 L 8.6672516,81.535431 C 10.636647,80.851873 "
  "12.435148,80.347316 14.062759,80.021759 C 15.690353,79.696275 "
  "17.26099,79.533515 18.774673,79.533478 C 22.74601,79.533515 "
  "25.773351,80.347316 27.856705,81.974884 C 29.940013,83.586245 "
  "30.981679,85.946269 30.981705,89.054962 C 30.981679,90.65004 "
  "30.664296,92.08233 30.029556,93.351837 C 29.394766,94.605114 "
  "28.312411,95.956025 26.782486,97.404572 L 25.219986,98.796173 C "
  "24.113196,99.805305 23.388913,100.61911 23.047134,101.23758 C "
  "22.70532,101.83981 22.534422,102.50712 22.534439,103.23953 L "
  "22.534439,104.33817 M 13.720963,107.95145 L 22.534439,107.95145 L "
  "22.534439,116.64285 L 13.720963,116.64285 L 13.720963,107.95145\"\n"
  "     id=\"text1882\" />\n"
  "</svg>\n"
  "\n");

} // namespace massxpert

} // namespace MsXpS

