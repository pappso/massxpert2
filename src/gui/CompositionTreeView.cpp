/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QDebug>
#include <QHeaderView>


/////////////////////// Local includes
#include "CompositionTreeViewSortProxyModel.hpp"
#include "CompositionTreeView.hpp"
#include <libXpertMass/Monomer.hpp>
#include "CompositionsDlg.hpp"


namespace MsXpS
{

namespace massxpert
{


  class CompositionTreeViewModel;
  class CompositionTreeViewItem;


  CompositionTreeView::CompositionTreeView(QWidget *parent) : QTreeView(parent)
  {

    setAlternatingRowColors(true);
    setAllColumnsShowFocus(true);
    setSortingEnabled(true);

    QHeaderView *headerView = header();

    headerView->setSectionsClickable(true);
    headerView->setSectionsMovable(true);
  }


  CompositionTreeView::~CompositionTreeView()
  {
  }


  CompositionsDlg *
  CompositionTreeView::getParentDlg()
  {
    return mp_parentDlg;
  }

  void
  CompositionTreeView::setParentDlg(CompositionsDlg *dlg)
  {
    Q_ASSERT(dlg);
    mp_parentDlg = dlg;
  }

} // namespace massxpert

} // namespace MsXpS
