/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes
#include <QDialog>
#include <libXpertMass/PolChemDefEntity.hpp>

/////////////////////// Local includes
#include "SequenceEditorWnd.hpp"


namespace MsXpS
{

namespace massxpert
{


  class AbstractSeqEdWndDependentDlg : public QDialog
  {
    Q_OBJECT

    public:
    AbstractSeqEdWndDependentDlg(SequenceEditorWnd *seqEditorWnd,
                                 libXpertMass::Polymer *polymer,
                                 libXpertMass::PolChemDefCstSPtr polChemDefCstSPtr,
                                 const QString &configSettingsFilePath,
                                 const QString &wndTypeName,
                                 const QString &applicationName,
                                 const QString &description);

    ~AbstractSeqEdWndDependentDlg();

    virtual bool initialize() = 0;

    SequenceEditorWnd *editorWnd() const;

    protected:
    SequenceEditorWnd *mp_editorWnd;

    libXpertMass::Polymer *mp_polymer;
    const libXpertMass::PolChemDefCstSPtr mcsp_polChemDef;

    QString m_configSettingsFilePath;

    // This string will be of the form "CleavageDlg" for example, and will be
    // used to create named sections in the QSettings-based configsettings file.
    QString m_wndTypeName;
    QString m_applicationName;
    QString m_windowDescription;

    virtual void writeSettings();
    virtual void readSettings();

    virtual void displayWindowTitle();

    virtual void closeEvent(QCloseEvent *event);
  };

} // namespace massxpert

} // namespace MsXpS

