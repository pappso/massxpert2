/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QDebug>

/////////////////////// Local includes
#include "CalculatorChemPadGroupBox.hpp"


namespace MsXpS
{

namespace massxpert
{


  CalculatorChemPadGroupBox::CalculatorChemPadGroupBox(QWidget *parent)
    : QGroupBox(parent)
  {
    mpa_vboxLayout = new QVBoxLayout;
    setLayout(mpa_vboxLayout);

    mpa_frame = new QFrame;
    mpa_vboxLayout->addWidget(mpa_frame);


    mpa_gridLayout = new QGridLayout;
    mpa_frame->setLayout(mpa_gridLayout);
  }

  CalculatorChemPadGroupBox::CalculatorChemPadGroupBox(const QString &title,
                                                       QWidget *parent)
    : QGroupBox(title, parent)
  {
    mpa_vboxLayout = new QVBoxLayout;
    setLayout(mpa_vboxLayout);

    mpa_frame = new QFrame;
    mpa_vboxLayout->addWidget(mpa_frame);

    mpa_gridLayout = new QGridLayout;
    mpa_frame->setLayout(mpa_gridLayout);
  }

  CalculatorChemPadGroupBox::~CalculatorChemPadGroupBox()
  {
  }


  void
  CalculatorChemPadGroupBox::addChemPadButton(ChemPadButton *button,
                                              int row,
                                              int column)
  {
    if(!button)
      return;

    mpa_gridLayout->addWidget(button, row, column);
  }


  ////////////////////////////// SLOTS ///////////////////////////////
  void
  CalculatorChemPadGroupBox::toggled(bool checked)
  {
    mpa_frame->setVisible(checked);
  }


} // namespace massxpert

} // namespace MsXpS
