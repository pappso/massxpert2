/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef POL_CHEM_DEF_WND_HPP
#define POL_CHEM_DEF_WND_HPP


/////////////////////// Local includes
#include "ui_PolChemDefWnd.h"
#include "AbstractMainTaskWindow.hpp"
#include <libXpertMass/PolChemDef.hpp>


namespace MsXpS
{

namespace massxpert
{


// class AtomDefDlg;
class IsotopeDefDlg;
class MonomerDefDlg;
class ModifDefDlg;
class CleaveSpecDefDlg;
class FragSpecDefDlg;
class CrossLinkerDefDlg;


class PolChemDefWnd : public AbstractMainTaskWindow
{
  Q_OBJECT

  // friend class AtomDefDlg;
  friend class IsotopeDefDlg;
  friend class MonomerDefDlg;
  friend class ModifDefDlg;
  friend class CleaveSpecDefDlg;
  friend class FragSpecDefDlg;
  friend class CrossLinkerDefDlg;

  public:
  PolChemDefWnd(ProgramWindow *parent,
                const QString &polChemDefFilePath,
                const QString &applicationName,
                const QString &description);

  ~PolChemDefWnd();

  bool m_forciblyClose = false;

  libXpertMass::PolChemDefSPtr msp_polChemDef;


  bool maybeSave();
  bool saveFile();

  bool setSingularData();

  // bool checkAtomList();
  bool checkIsotopicData();

  void updateWindowTitle();

  public slots:

  // SINGULAR ENTITIES
  void nameEditFinished();
  void leftCapEditFinished();
  void rightCapEditFinished();
  void ionizeFormulaEditFinished();
  void ionizeChargeChanged();
  void ionizeLevelChanged();

  // PLURAL ENTITIES
  // void atomPushButtonClicked();
  void isotopePushButtonClicked();
  void monomerPushButtonClicked();
  void modifPushButtonClicked();
  void crossLinkerPushButtonClicked();
  void cleavagePushButtonClicked();
  void fragmentationPushButtonClicked();


  // SAVE/SAVE AS
  bool save();
  bool saveAs();

  // VALIDATION
  int validate();

  private:
  Ui::PolChemDefWnd m_ui;

  // Init to 0 all the window pointers, because we'll need to check
  // the construction of the these windows.
  // AtomDefDlg *mpa_atomDefDlg               = 0;
  IsotopeDefDlg *mpa_isotopeDefDlg         = 0;
  MonomerDefDlg *mpa_monomerDefDlg         = 0;
  ModifDefDlg *mpa_modifDefDlg             = 0;
  CleaveSpecDefDlg *mpa_cleaveSpecDefDlg   = 0;
  FragSpecDefDlg *mpa_fragSpecDefDlg       = 0;
  CrossLinkerDefDlg *mpa_crossLinkerDefDlg = 0;

  bool initialize();

  void closeEvent(QCloseEvent *event);
};

} // namespace massxpert

} // namespace MsXpS


#endif // POL_CHEM_DEF_WND_HPP
