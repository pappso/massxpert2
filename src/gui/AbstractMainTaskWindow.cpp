/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QSettings>
#include <QCloseEvent>
#include <QDebug>


/////////////////////// Local includes
#include "AbstractMainTaskWindow.hpp"
#include "ProgramWindow.hpp"
#include "../nongui/UserSpec.hpp"


namespace MsXpS
{

namespace massxpert
{


  AbstractMainTaskWindow::AbstractMainTaskWindow(ProgramWindow *parent,
                                                 const QString &wndTypeName,
                                                 const QString &applicationName,
                                                 const QString &description)
    : // Note that we do not want these main task windows to be real
      // QMainWindow, in the sense that we want them to be destroyed when the
      // parent widget (that is the ProgramWindow) is destroyed.
      QMainWindow(static_cast<QWidget *>(parent), Qt::Widget),
      mp_parentWnd(parent),
      m_wndTypeName(wndTypeName),
      m_applicationName(applicationName),
      m_windowDescription(description)
  {
    if(!parent)
      qFatal("Fatal error at %s@%d. Program aborted.", __FILE__, __LINE__);

    setWindowTitle(
      QString("%1 - %2").arg(m_applicationName).arg(m_windowDescription));

    setAttribute(Qt::WA_DeleteOnClose);
  }

  AbstractMainTaskWindow::~AbstractMainTaskWindow()
  {

    // qDebug() << __FILE__ << __LINE__
    // << "Enter ~AbstractMainTaskWindow.";

    // qDebug() << __FILE__ << __LINE__
    // << "Exit ~AbstractMainTaskWindow.";
  }


  void
  AbstractMainTaskWindow::closeEvent(QCloseEvent *event)
  {
    // qDebug() << __FILE__ << __LINE__
    // << "Enter AbstractMainTaskWindow::closeEvent.";

    writeSettings();

    event->accept();

    // qDebug() << __FILE__ << __LINE__
    // << "Exit AbstractMainTaskWindow::closeEvent.";
  }


  void
  AbstractMainTaskWindow::writeSettings()
  {
    QSettings settings(mp_parentWnd->configSettingsFilePath(),
                       QSettings::IniFormat);

    settings.beginGroup(m_wndTypeName);
    settings.setValue("geometry", saveGeometry());
    settings.endGroup();
  }


  void
  AbstractMainTaskWindow::readSettings()
  {
    QSettings settings(mp_parentWnd->configSettingsFilePath(),
                       QSettings::IniFormat);

    settings.beginGroup(m_wndTypeName);
    restoreGeometry(settings.value("geometry").toByteArray());
    settings.endGroup();
  }

  QString
  AbstractMainTaskWindow::configSettingsFilePath()
  {
    return mp_parentWnd->configSettingsFilePath();
  }

  const UserSpec &
  AbstractMainTaskWindow::userSpec()
  {
    return mp_parentWnd->userSpec();
  }


  const ConfigSettings *
  AbstractMainTaskWindow::configSettings() const
  {
    return mp_parentWnd->configSettings();
  }


  const ProgramWindow *
  AbstractMainTaskWindow::mainWindow() const
  {
    return mp_parentWnd;
  }

} // namespace massxpert

} // namespace MsXpS
