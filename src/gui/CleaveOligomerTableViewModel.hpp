/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef CLEAVE_OLIGOMER_TABLE_VIEW_MODEL_HPP
#define CLEAVE_OLIGOMER_TABLE_VIEW_MODEL_HPP


/////////////////////// Qt includes
#include <QDebug>
#include <QAbstractTableModel>


/////////////////////// Local includes
#include "../nongui/OligomerList.hpp"
#include "CleavageDlg.hpp"

namespace MsXpS
{

	namespace massxpert
	{



enum
{
  CLEAVE_OLIGO_PARTIAL_COLUMN,
  CLEAVE_OLIGO_NAME_COLUMN,
  CLEAVE_OLIGO_COORDINATES_COLUMN,
  CLEAVE_OLIGO_MONO_COLUMN,
  CLEAVE_OLIGO_AVG_COLUMN,
  CLEAVE_OLIGO_CHARGE_COLUMN,
  CLEAVE_OLIGO_FORMULA_COLUMN,
  CLEAVE_OLIGO_MODIF_COLUMN,
  CLEAVE_OLIGO_TOTAL_COLUMNS
};


class CleaveOligomerTableViewModel : public QAbstractTableModel
{
  Q_OBJECT

  public:
  CleaveOligomerTableViewModel(OligomerList *, QObject *);
  ~CleaveOligomerTableViewModel();

  const OligomerList *oligomerList();
  const CleavageDlg *parentDlg() const;

  void setTableView(CleaveOligomerTableView *);
  CleaveOligomerTableView *tableView();

  int rowCount(const QModelIndex &parent = QModelIndex()) const;
  int columnCount(const QModelIndex &parent = QModelIndex()) const;

  QVariant headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const;

  QVariant data(const QModelIndex &parent = QModelIndex(),
                int role                  = Qt::DisplayRole) const;

  int addOligomers(const OligomerList &);
  int removeOligomers(int, int);

  private:
  OligomerList *mp_oligomerList;
  CleavageDlg *mp_parentDlg;
  CleaveOligomerTableView *mp_tableView;

};

} // namespace massxpert

} // namespace MsXpS


#endif // CLEAVE_OLIGOMER_TABLE_VIEW_MODEL_HPP
