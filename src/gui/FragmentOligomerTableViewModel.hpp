/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef FRAGMENT_OLIGOMER_TABLE_VIEW_MODEL_HPP
#define FRAGMENT_OLIGOMER_TABLE_VIEW_MODEL_HPP


/////////////////////// Qt includes
#include <QDebug>
#include <QAbstractTableModel>


/////////////////////// Local includes
#include "../nongui/OligomerList.hpp"
#include "FragmentationDlg.hpp"


namespace MsXpS
{

namespace massxpert
{


enum
{
  FRAGMENT_OLIGO_PATTERN_COLUMN,
  FRAGMENT_OLIGO_NAME_COLUMN,
  FRAGMENT_OLIGO_COORDINATES_COLUMN,
  FRAGMENT_OLIGO_MONO_COLUMN,
  FRAGMENT_OLIGO_AVG_COLUMN,
  FRAGMENT_OLIGO_CHARGE_COLUMN,
  FRAGMENT_OLIGO_FORMULA_COLUMN,
  FRAGMENT_OLIGO_MODIF_COLUMN,
  FRAGMENT_OLIGO_TOTAL_COLUMNS
};


class FragmentOligomerTableViewModel : public QAbstractTableModel
{
  Q_OBJECT
  public:
  FragmentOligomerTableViewModel(OligomerList *, QObject *);
  ~FragmentOligomerTableViewModel();

  const OligomerList *oligomerList();
  FragmentationDlg *parentDlg();

  void setTableView(FragmentOligomerTableView *);
  FragmentOligomerTableView *tableView();

  int rowCount(const QModelIndex &parent = QModelIndex()) const;
  int columnCount(const QModelIndex &parent = QModelIndex()) const;

  QVariant headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const;

  QVariant data(const QModelIndex &parent = QModelIndex(),
                int role                  = Qt::DisplayRole) const;

  int addOligomers(const OligomerList &);
  int removeOligomers(int, int);

  private:
  OligomerList *mp_oligomerList;
  FragmentationDlg *mp_parentDlg;
  FragmentOligomerTableView *mp_tableView;
};

} // namespace massxpert

} // namespace MsXpS


#endif // FRAGMENT_OLIGOMER_TABLE_VIEW_MODEL_HPP
