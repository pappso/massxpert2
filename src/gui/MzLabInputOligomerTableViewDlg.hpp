/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef MZ_LAB_INPUT_OLIGOMER_TABLE_VIEW_DLG_HPP
#define MZ_LAB_INPUT_OLIGOMER_TABLE_VIEW_DLG_HPP
// #pragma message("including MZ_LAB_INPUT_OLIGOMER_TABLE_VIEW_DLG_HPP")

/////////////////////// Qt includes
#include <QMainWindow>


/////////////////////// Local includes
#include <libXpertMass/globals.hpp>
#include "ui_MzLabInputOligomerTableViewDlg.h"
#include "MzLabInputOligomerTableViewSortProxyModel.hpp"
#include "SequenceEditorWnd.hpp"

namespace MsXpS
{

	namespace massxpert
	{



class MzLabInputOligomerTableView;
class MzLabInputOligomerTableViewModel;
class MzLabInputOligomerTableViewSortProxyModel;

class MzLabInputOligomerTableViewDlg : public QDialog
{
  Q_OBJECT

  private:
  Ui::MzLabInputOligomerTableViewDlg m_ui;

  MzLabWnd *mp_mzLabWnd;
  SequenceEditorWnd *mp_sequenceEditorWnd;

  QString m_name;
  QString m_history;

  // The list of oligomers that will be handled via the QTableView
  // widget.
  OligomerList m_oligomerList;

  // As of version 3.6.0, there is no more any difference between
  // "normal" oligomers and fragment oligomers since there has been
  // a total rewrite of the way fragmentation-generated oligomers
  // ionize. So there is no more the m_isFragment member datum.

  libXpertMass::MassType m_massType;
  // Indicates if data were already imported with a mass type, so
  // that if another import is done with another mass type, then the
  // user is alerted that he is mixing masses of different mass
  // types. When the dialog is constructed the value is "NONE" since
  // we do not know what kind of masses the user will import.
  libXpertMass::MassType m_previousMassType;

  MzLabInputOligomerTableViewModel *mpa_oligomerTableViewModel;
  MzLabInputOligomerTableViewSortProxyModel *mpa_proxyModel;

  void setupTableView();

  void closeEvent(QCloseEvent *event);

  public:
  MzLabInputOligomerTableViewDlg(QWidget *, QString, libXpertMass::MassType);

  ~MzLabInputOligomerTableViewDlg();

  void setName(const QString &);
  QString name();

  const OligomerList &oligomerList();

  SequenceEditorWnd *sequenceEditorWnd();

  QString textFieldDelimiter();

  int duplicateOligomerData(const OligomerList &);

  libXpertMass::MassType massType();
  libXpertMass::MassType previousMassType();
  void setPreviousMassType(libXpertMass::MassType);

  libXpertMass::MassType massTypeQuery();

  void applyFormula(const libXpertMass::Formula &, bool);
  void applyMass(double, bool);
  void applyThreshold(double, bool, bool);
  void applyChargeIncrement(int, bool);
  void applyIonizeRule(const libXpertMass::IonizeRule &, bool);

  public slots:
  void monoMassCheckBoxStateChanged(int);
  void avgMassCheckBoxStateChanged(int);
  void exportToClipboard();
  void getFromClipboard();
  SequenceEditorWnd *connectSeqEditorWnd(const QString &);
};

} // namespace massxpert

} // namespace MsXpS


#endif // MZ_LAB_INPUT_OLIGOMER_TABLE_VIEW_DLG_HPP
// #pragma message("done...including MZ_LAB_INPUT_OLIGOMER_TABLE_VIEW_DLG_HPP")
