/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef FRAG_SPEC_DEF_DLG_HPP
#define FRAG_SPEC_DEF_DLG_HPP


/////////////////////// Local includes
#include "AbstractPolChemDefDependentDlg.hpp"
#include "ui_FragSpecDefDlg.h"
#include <libXpertMass/PolChemDef.hpp>


namespace MsXpS
{

namespace massxpert
{


  class PolChemDef;
  class PolChemDefWnd;
  class FragSpec;
  class FragRule;


  class FragSpecDefDlg : public AbstractPolChemDefDependentDlg
  {
    Q_OBJECT

    public:
    FragSpecDefDlg(libXpertMass::PolChemDefSPtr polChemDefSPtr,
                   PolChemDefWnd *polChemDefWnd,
                   const QString &settingsFilePath,
                   const QString &applicationName,
                   const QString &description);

    ~FragSpecDefDlg();

    bool initialize();

    void updateFragSpecDetails(libXpertMass::FragSpec *);
    void updateFragRuleDetails(libXpertMass::FragRule *);

    void clearAllDetails();

    public slots:

    // ACTIONS
    void addFragSpecPushButtonClicked();
    void removeFragSpecPushButtonClicked();
    void moveUpFragSpecPushButtonClicked();
    void moveDownFragSpecPushButtonClicked();

    void addFragRulePushButtonClicked();
    void removeFragRulePushButtonClicked();
    void moveUpFragRulePushButtonClicked();
    void moveDownFragRulePushButtonClicked();

    void applyFragSpecPushButtonClicked();
    void applyFragRulePushButtonClicked();

    bool validatePushButtonClicked();

    // VALIDATION
    bool validate();

    // WIDGETRY
    void fragSpecListWidgetItemSelectionChanged();
    void fragRuleListWidgetItemSelectionChanged();

    private:
    Ui::FragSpecDefDlg m_ui;

    QList<libXpertMass::FragSpec *> *mp_list;

    void closeEvent(QCloseEvent *event);

    void writeSettings();
    void readSettings();
  };

} // namespace massxpert

} // namespace MsXpS


#endif // FRAG_SPEC_DEF_DLG_HPP
