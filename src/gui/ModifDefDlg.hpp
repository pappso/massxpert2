/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef MODIF_DEF_DLG_HPP
#define MODIF_DEF_DLG_HPP


/////////////////////// Local includes
#include "AbstractPolChemDefDependentDlg.hpp"
#include "ui_ModifDefDlg.h"
#include <libXpertMass/PolChemDef.hpp>


namespace MsXpS
{

namespace massxpert
{


  class PolChemDef;
  class PolChemDefWnd;
  class Modif;


  class ModifDefDlg : public AbstractPolChemDefDependentDlg
  {
    Q_OBJECT

    public:
    ModifDefDlg(libXpertMass::PolChemDefSPtr polChemDefSPtr,
                PolChemDefWnd *polChemDefWnd,
                const QString &settingsFilePath,
                const QString &applicationName,
                const QString &description);

    ~ModifDefDlg();

    bool initialize();

    void updateModifDetails(libXpertMass::Modif *);

    void clearAllDetails();

    public slots:

    // ACTIONS
    void addModifPushButtonClicked();
    void removeModifPushButtonClicked();
    void moveUpModifPushButtonClicked();
    void moveDownModifPushButtonClicked();

    void applyModifPushButtonClicked();

    bool validatePushButtonClicked();

    // VALIDATION
    bool validate();

    // WIDGETRY
    void modifListWidgetItemSelectionChanged();

    private:
    Ui::ModifDefDlg m_ui;

    QList<libXpertMass::Modif *> *mp_list;

    void closeEvent(QCloseEvent *event);

    void writeSettings();
    void readSettings();
  };

} // namespace massxpert

} // namespace MsXpS


#endif // MODIF_DEF_DLG_HPP
