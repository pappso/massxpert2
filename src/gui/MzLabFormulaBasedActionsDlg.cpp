/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QMessageBox>
#include <QCloseEvent>
#include <QDebug>
#include <QSettings>


/////////////////////// libmass includes
#include <libXpertMass/PolChemDefEntity.hpp>
#include <libXpertMass/IonizeRule.hpp>


/////////////////////// Local includes
#include "MzLabFormulaBasedActionsDlg.hpp"
#include "MzLabInputOligomerTableViewDlg.hpp"
#include "MzLabWnd.hpp"


namespace MsXpS
{

namespace massxpert
{


MzLabFormulaBasedActionsDlg::MzLabFormulaBasedActionsDlg(
  QWidget *parent, const QString &configSettingsFilePath)
  : QDialog{parent}, m_configSettingsFilePath{configSettingsFilePath}
{
  Q_ASSERT(parent);

  mp_mzLabWnd = static_cast<MzLabWnd *>(parent);

  m_ui.setupUi(this);

  setWindowTitle(tr("massXpert: mz Lab - Formula-based Actions"));

  m_ui.incrementChargeSpinBox->setRange(-1000000000, 1000000000);

  libXpertMass::PolChemDefCstSPtr polChemDefCstSPtr =
    mp_mzLabWnd->polChemDefCstSPtr();

  libXpertMass::IonizeRule ionizeRule = polChemDefCstSPtr->ionizeRule();

  m_ui.reIonizationFormulaLineEdit->setText(ionizeRule.formula());
  m_ui.reIonizationChargeSpinBox->setValue(ionizeRule.charge());
  m_ui.reIonizationLevelSpinBox->setValue(ionizeRule.level());

  connect(m_ui.applyFormulaPushButton,
          SIGNAL(clicked()),
          this,
          SLOT(applyFormulaPushButton()));

  connect(m_ui.incrementChargePushButton,
          SIGNAL(clicked()),
          this,
          SLOT(incrementChargePushButton()));

  connect(m_ui.reionizePushButton,
          SIGNAL(clicked()),
          this,
          SLOT(reionizePushButton()));

  QSettings settings(m_configSettingsFilePath, QSettings::IniFormat);

  settings.beginGroup("mz_lab_wnd_formula_based_actions");

  restoreGeometry(settings.value("geometry").toByteArray());

  settings.endGroup();
}


MzLabFormulaBasedActionsDlg::~MzLabFormulaBasedActionsDlg()
{
}


void
MzLabFormulaBasedActionsDlg::closeEvent(QCloseEvent *event)
{
  QSettings settings(m_configSettingsFilePath, QSettings::IniFormat);

  settings.beginGroup("mz_lab_wnd_formula_based_actions");

  settings.setValue("geometry", saveGeometry());

  settings.endGroup();

  QDialog::closeEvent(event);
}


void
MzLabFormulaBasedActionsDlg::applyFormulaPushButton()
{
  // The actual work of doing the calculation is performed by the
  // MzLabInputOligomerTableViewDlg itself. So we have to get a
  // pointer to that dialog window. But first, check if the mass
  // entered in the line edit is correct.

  QString text = m_ui.formulaLineEdit->text();
  if(text.isEmpty())
    {
      QMessageBox::warning(this,
                           tr("massXpert: mz Lab"),
                           tr("%1@%2\n"
                              "Please, enter a valid formula.")
                             .arg(__FILE__)
                             .arg(__LINE__));
      return;
    }

  // Further, we have to get a pointer to the polymer chemistry
  // definition in use in this mzLab, because we need to validate
  // the formula.

  libXpertMass::PolChemDefCstSPtr polChemDefCstSPtr =
    mp_mzLabWnd->polChemDefCstSPtr();

  libXpertMass::Formula formula(text);

  libXpertMass::IsotopicDataCstSPtr isotopic_data_csp =
    polChemDefCstSPtr->getIsotopicDataCstSPtr();

  if(!formula.validate(isotopic_data_csp))
    {
      QMessageBox::warning(this,
                           tr("massXpert: mz Lab"),
                           tr("%1@%2\n"
                              "The formula '%3' is not valid.")
                             .arg(__FILE__)
                             .arg(__LINE__)
                             .arg(formula.toString()));
      return;
    }


  // Get the pointer to the input list dialog window.

  MzLabInputOligomerTableViewDlg *dlg = 0;
  dlg                                 = mp_mzLabWnd->inputListDlg();
  if(!dlg)
    {
      QMessageBox::warning(this,
                           tr("massXpert: mz Lab"),
                           tr("%1@%2\n"
                              "Please, select one input list first.")
                             .arg(__FILE__)
                             .arg(__LINE__));
      return;
    }

  // Is the calculation to be performed inside the input list dialog
  // window, or are we going to create a new mz list with the
  // results of that calculation?
  bool inPlace = mp_mzLabWnd->inPlaceCalculation();

  // Finally actually have the calculation performed by the mz input
  // list dialog window.
  dlg->applyFormula(formula, inPlace);
}


void
MzLabFormulaBasedActionsDlg::incrementChargePushButton()
{
  // The actual work of doing the calculation is performed by the
  // MzLabInputOligomerTableViewDlg itself. So we have to get a
  // pointer to that dialog window. But first, check if the threshold
  // entered in the line edit is empty/correct or not.

  int increment = m_ui.incrementChargeSpinBox->value();

  if(!increment)
    {
      QMessageBox::warning(this,
                           tr("massXpert: mz Lab"),
                           tr("%1@%2\n"
                              "Please, enter a valid increment.")
                             .arg(__FILE__)
                             .arg(__LINE__),
                           QMessageBox::Ok);

      return;
    }

  // Get the pointer to the input list dialog window.

  MzLabInputOligomerTableViewDlg *dlg = 0;
  dlg                                 = mp_mzLabWnd->inputListDlg();
  if(!dlg)
    {
      QMessageBox::warning(this,
                           tr("massXpert: mz Lab"),
                           tr("%1@%2\n"
                              "Please, select one input list first.")
                             .arg(__FILE__)
                             .arg(__LINE__));
      return;
    }

  // Is the calculation to be performed inside the input list dialog
  // window, or are we going to create a new mz list with the
  // results of that calculation?
  bool inPlace = mp_mzLabWnd->inPlaceCalculation();

  // Finally actually have the calculation performed by the mz input
  // list dialog window.
  dlg->applyChargeIncrement(increment, inPlace);
}


void
MzLabFormulaBasedActionsDlg::reionizePushButton()
{
  // The actual work of doing the calculation is performed by the
  // MzLabInputOligomerTableViewDlg itself. So we have to get a
  // pointer to that dialog window. But first, check if the threshold
  // entered in the line edit is empty/correct or not.

  libXpertMass::IonizeRule ionizeRule;

  ionizeRule.setFormula(m_ui.reIonizationFormulaLineEdit->text());
  ionizeRule.setCharge(m_ui.reIonizationChargeSpinBox->value());
  ionizeRule.setLevel(m_ui.reIonizationLevelSpinBox->value());

  // Further, we have to get a pointer to the polymer chemistry
  // definition in use in this mzLab, because we need to validate
  // the ionization rule.

  libXpertMass::PolChemDefCstSPtr polChemDefCstSPtr =
    mp_mzLabWnd->polChemDefCstSPtr();

  libXpertMass::IsotopicDataCstSPtr isotopic_data_csp =
    polChemDefCstSPtr->getIsotopicDataCstSPtr();

  if(!ionizeRule.validate(isotopic_data_csp))
    {
      QMessageBox::warning(this,
                           tr("massXpert: mz Lab"),
                           tr("%1@%2\n"
                              "Failed to validate ionization rule.")
                             .arg(__FILE__)
                             .arg(__LINE__),
                           QMessageBox::Ok);

      return;
    }

  // Get the pointer to the input list dialog window.

  MzLabInputOligomerTableViewDlg *dlg = 0;
  dlg                                 = mp_mzLabWnd->inputListDlg();
  if(!dlg)
    {
      QMessageBox::warning(this,
                           tr("massXpert: mz Lab"),
                           tr("%1@%2\n"
                              "Please, select one input list first.")
                             .arg(__FILE__)
                             .arg(__LINE__));
      return;
    }

  // Is the calculation to be performed inside the input list dialog
  // window, or are we going to create a new mz list with the
  // results of that calculation?
  bool inPlace = mp_mzLabWnd->inPlaceCalculation();

  // Finally actually have the calculation performed by the mz input
  // list dialog window.
  dlg->applyIonizeRule(ionizeRule, inPlace);
}


} // namespace massxpert

} // namespace MsXpS
