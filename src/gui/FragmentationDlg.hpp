/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef FRAGMENTATION_DLG_HPP
#define FRAGMENTATION_DLG_HPP


/////////////////////// Qt includes
#include <QSortFilterProxyModel>
#include <libXpertMass/PolChemDefEntity.hpp>


/////////////////////// libmassgui includes
#include <libXpertMassGui/MassPeakShaperConfigDlg.hpp>
#include <pappsomspp/trace/trace.h>


/////////////////////// Local includes
#include "ui_FragmentationDlg.h"
#include "AbstractSeqEdWndDependentDlg.hpp"
#include "../nongui/Fragmenter.hpp"
#include "SequenceEditorWnd.hpp"
#include "FragmentOligomerTableViewModel.hpp"
#include "FragmentOligomerTableViewSortProxyModel.hpp"
#include "../nongui/FragOptions.hpp"


namespace MsXpS
{

namespace massxpert
{


class FragmentOligomerTableViewModel;
class FragmentOligomerTableViewSortProxyModel;

class FragmentationDlg : public AbstractSeqEdWndDependentDlg
{
  Q_OBJECT


  public:
  FragmentationDlg(SequenceEditorWnd *editorWnd,
                   libXpertMass::Polymer *polymer,
                   const libXpertMass::PolChemDefCstSPtr polChemDefCstSPtr,
                   const QString &configSettingsFilePath,
                   const QString &applicationName,
                   const QString &description,
                   const libXpertMass::CalcOptions &calcOptions,
                   const libXpertMass::IonizeRule *ionizeRule);

  ~FragmentationDlg();

  bool initialize();

  bool populateSelectedOligomerData();
  void populateFragSpecList();

  void updateFragmentationDetails(const libXpertMass::CalcOptions &);
  void updateOligomerSequence(QString *);

  bool calculateTolerance(double);

  // The results-exporting functions. ////////////////////////////////
  void prepareResultsTxtString();
  bool exportResultsClipboard();
  bool exportResultsFile();
  bool selectResultsFile();
  //////////////////////////////////// The results-exporting functions.

  public slots:
  void fragment();
  void exportResults(int);
  void filterOptions(bool);
  void filterOptionsToggled();
  void filterPattern();
  void filterMonoMass();
  void filterAvgMass();
  void filterCharge();

  void massSpectrumSynthesisMenuActivated(int index);
  void traceColorPushButtonClicked();
  bool loadIsotopicDataFromFile();

  signals:

  void displayMassSpectrumSignal(const QString &title,
                                 const QByteArray &color_byte_array,
                                 pappso::TraceCstSPtr trace);

  private:
  Ui::FragmentationDlg m_ui;

  // The results-exporting strings. ////////////////////////////////
  QString *mpa_resultsString = nullptr;
  QString m_resultsFilePath;
  //////////////////////////////////// The results-exporting strings.

  OligomerList m_oligomerList;

  libXpertMass::IsotopicDataSPtr msp_isotopicData = nullptr;
  libXpertMass::MassPeakShaperConfig m_massPeakShaperConfig;

  pappso::Trace m_syntheticMassSpectrum;

  libXpertMass::CalcOptions m_calcOptions;
  const libXpertMass::IonizeRule *mp_ionizeRule = nullptr;

  Fragmenter *mpa_fragmenter = nullptr;

  // For the mass spectra that are synthesized and served.
  QByteArray m_colorByteArray;

  void writeSettings();
  void readSettings();

  FragmentOligomerTableViewModel *mpa_oligomerTableViewModel = nullptr;
  FragmentOligomerTableViewSortProxyModel *mpa_proxyModel = nullptr;

  libXpertMassGui::MassPeakShaperConfigDlg *mp_massPeakShaperConfigDlg = nullptr;

  // For the filtering of the data in the treeview.
  QAction *filterAct = nullptr;
  double m_tolerance;
  QWidget *mp_focusWidget = nullptr;

  void setupTableView();
  void configureMassPeakShaper();
  void synthesizeMassSpectra();
};

} // namespace massxpert

} // namespace MsXpS


#endif // FRAGMENTATION_DLG_HPP
