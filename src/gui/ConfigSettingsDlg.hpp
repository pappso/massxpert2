/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef CONFIG_SETTINGS_DLG_HPP
#define CONFIG_SETTINGS_DLG_HPP


/////////////////////// Qt includes
#include <QDir>


/////////////////////// Local includes
#include "ui_ConfigSettingsDlg.h"
#include "../nongui/ConfigSettings.hpp"
#include <QGridLayout>
#include <QSplitter>
#include <QScrollArea>
#include <QWidget>
#include <QVBoxLayout>
#include <QLineEdit>


namespace MsXpS
{

	namespace massxpert
	{



class ConfigSettingsDlg : public QDialog
{
  Q_OBJECT

  private:
  Ui::ConfigSettingsDlg m_ui;

  QString m_applicationName;
  ConfigSettings *mp_configSettings;
  QString m_configSettingsFilePath;

  void closeEvent(QCloseEvent *event);

  QGridLayout *mp_groupBoxGridLayout = nullptr;
  QSplitter *mp_splitter             = nullptr;
  QScrollArea *mp_scrollArea         = nullptr;

  QWidget *mp_scrollAreaWidgetContents               = nullptr;
  QGridLayout *mp_scrollAreaWidgetContentsGridLayout = nullptr;
  QVBoxLayout *mp_scrollAreaVBoxLayout               = nullptr;

  QMap<QLineEdit *, QPushButton *> m_lepbMap;
  QMap<QLineEdit *, ConfigSetting *> m_lecsMap;

  QWidget *setupNewWidgetry(ConfigSetting *configSetting);

  public:
  ConfigSettingsDlg(const QString &applicationName,
                    ConfigSettings *configSettings,
                    const QString &configSettingsFilePath,
                    bool failedConfig = false);

  ~ConfigSettingsDlg();

  bool initialize(bool failedConfig);

  bool checkDataDir(const QDir &);

  public slots:
  void browseDirPushButtonClicked();
  void saveSettingsPushButtonClicked();
  void eraseSettingsPushButtonClicked();
  void cancelPushButtonClicked();
};

} // namespace massxpert

} // namespace MsXpS


#endif // CONFIG_SETTINGS_DLG_HPP
