/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */

namespace MsXpS
{

	namespace massxpert
	{

#include <QByteArray>

QByteArray defaultCrossLinkerVignette(
  "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n"
  "<!-- Created with Sodipodi(\"http://www.sodipodi.com/\") -->\n"
  "<svg\n"
  "   xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n"
  "   xmlns:cc=\"http://web.resource.org/cc/\"\n"
  "   xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
  "   xmlns:svg=\"http://www.w3.org/2000/svg\"\n"
  "   xmlns=\"http://www.w3.org/2000/svg\"\n"
  "   xmlns:xlink=\"http://www.w3.org/1999/xlink\"\n"
  "   xmlns:sodipodi=\"http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd\"\n"
  "   xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\"\n"
  "   version=\"1\"\n"
  "   x=\"0\"\n"
  "   y=\"0\"\n"
  "   width=\"125\"\n"
  "   height=\"125\"\n"
  "   id=\"svg101\"\n"
  "   sodipodi:version=\"0.32\"\n"
  "   sodipodi:docname=\"cross-link-default-vignette.svg\"\n"
  "   "
  "sodipodi:docbase=\"/home/rusconi/devel/massxpert/data/polChemDefs/"
  "protein-1-letter\"\n"
  "   inkscape:version=\"0.45.1\"\n"
  "   inkscape:output_extension=\"org.inkscape.output.svg.inkscape\">\n"
  "  <metadata\n"
  "     id=\"metadata6\">\n"
  "    <rdf:RDF>\n"
  "      <cc:Work\n"
  "         rdf:about=\"\">\n"
  "        <dc:format>image/svg+xml</dc:format>\n"
  "        <dc:type\n"
  "           rdf:resource=\"http://purl.org/dc/dcmitype/StillImage\" />\n"
  "      </cc:Work>\n"
  "    </rdf:RDF>\n"
  "  </metadata>\n"
  "  <sodipodi:namedview\n"
  "     id=\"base\"\n"
  "     inkscape:zoom=\"6.728\"\n"
  "     inkscape:cx=\"62.5\"\n"
  "     inkscape:cy=\"62.5\"\n"
  "     inkscape:window-width=\"1519\"\n"
  "     inkscape:window-height=\"1034\"\n"
  "     inkscape:window-x=\"0\"\n"
  "     inkscape:window-y=\"25\"\n"
  "     inkscape:current-layer=\"svg101\" />\n"
  "  <defs\n"
  "     id=\"defs103\">\n"
  "    <linearGradient\n"
  "       id=\"linearGradient3197\">\n"
  "      <stop\n"
  "         style=\"stop-color:#000000;stop-opacity:1;\"\n"
  "         offset=\"0\"\n"
  "         id=\"stop3199\" />\n"
  "      <stop\n"
  "         id=\"stop3205\"\n"
  "         offset=\"0.5\"\n"
  "         style=\"stop-color:#000000;stop-opacity:1;\" />\n"
  "      <stop\n"
  "         style=\"stop-color:#000000;stop-opacity:0.87628865;\"\n"
  "         offset=\"0.75\"\n"
  "         id=\"stop3207\" />\n"
  "      <stop\n"
  "         style=\"stop-color:#000000;stop-opacity:0.38144329;\"\n"
  "         offset=\"1\"\n"
  "         id=\"stop3201\" />\n"
  "    </linearGradient>\n"
  "    <linearGradient\n"
  "       id=\"linearGradient3177\">\n"
  "      <stop\n"
  "         id=\"stop3189\"\n"
  "         offset=\"0\"\n"
  "         style=\"stop-color:#000000;stop-opacity:1;\" />\n"
  "      <stop\n"
  "         style=\"stop-color:#000000;stop-opacity:0.50980395;\"\n"
  "         offset=\"0.5\"\n"
  "         id=\"stop3195\" />\n"
  "      <stop\n"
  "         style=\"stop-color:#000000;stop-opacity:0.97938144;\"\n"
  "         offset=\"1\"\n"
  "         id=\"stop3193\" />\n"
  "    </linearGradient>\n"
  "    <linearGradient\n"
  "       inkscape:collect=\"always\"\n"
  "       xlink:href=\"#linearGradient3177\"\n"
  "       id=\"linearGradient3187\"\n"
  "       x1=\"13.974026\"\n"
  "       y1=\"99.651367\"\n"
  "       x2=\"55.130276\"\n"
  "       y2=\"99.651367\"\n"
  "       gradientUnits=\"userSpaceOnUse\" />\n"
  "    <radialGradient\n"
  "       inkscape:collect=\"always\"\n"
  "       xlink:href=\"#linearGradient3197\"\n"
  "       id=\"radialGradient3203\"\n"
  "       cx=\"61.157383\"\n"
  "       cy=\"105.7453\"\n"
  "       fx=\"61.157383\"\n"
  "       fy=\"105.7453\"\n"
  "       r=\"20.578125\"\n"
  "       gradientTransform=\"matrix(1,0,0,0.5094914,0,51.868981)\"\n"
  "       gradientUnits=\"userSpaceOnUse\" />\n"
  "    <radialGradient\n"
  "       inkscape:collect=\"always\"\n"
  "       xlink:href=\"#linearGradient3197\"\n"
  "       id=\"radialGradient3213\"\n"
  "       gradientUnits=\"userSpaceOnUse\"\n"
  "       gradientTransform=\"matrix(1,0,0,0.5094914,-26.605232,47.707269)\"\n"
  "       cx=\"61.157383\"\n"
  "       cy=\"105.7453\"\n"
  "       fx=\"61.157383\"\n"
  "       fy=\"105.7453\"\n"
  "       r=\"20.578125\" />\n"
  "    <radialGradient\n"
  "       inkscape:collect=\"always\"\n"
  "       xlink:href=\"#linearGradient3197\"\n"
  "       id=\"radialGradient3217\"\n"
  "       gradientUnits=\"userSpaceOnUse\"\n"
  "       gradientTransform=\"matrix(1,0,0,0.5094914,27.300832,47.707269)\"\n"
  "       cx=\"61.157383\"\n"
  "       cy=\"105.7453\"\n"
  "       fx=\"61.157383\"\n"
  "       fy=\"105.7453\"\n"
  "       r=\"20.578125\" />\n"
  "  </defs>\n"
  "  <path\n"
  "     id=\"path2184\"\n"
  "     d=\"M 50.235508,95.260925 C 44.877842,95.260925 40.579258,99.955779 "
  "40.579258,105.76092 C 40.579258,111.56607 44.877843,116.22968 "
  "50.235508,116.22968 L 72.079258,116.22968 C 77.436923,116.22968 "
  "81.735508,111.56607 81.735508,105.76092 C 81.735508,99.955782 "
  "77.436923,95.260925 72.079258,95.260925 L 50.235508,95.260925 z M "
  "52.704258,98.698425 L 69.923008,98.698425 C 74.15172,98.698425 "
  "77.548008,101.84966 77.548008,105.76092 C 77.548008,109.67219 "
  "74.151719,112.82343 69.923008,112.82343 L 52.704258,112.82343 C "
  "48.475546,112.82343 45.048008,109.6722 45.048008,105.76092 C "
  "45.048008,101.84965 48.475547,98.698425 52.704258,98.698425 z \"\n"
  "     "
  "style=\"opacity:0.98999999;fill:url(#radialGradient3203);fill-opacity:1.0;"
  "stroke:none;stroke-width:3;stroke-miterlimit:4;stroke-opacity:1\" />\n"
  "  <path\n"
  "     "
  "style=\"opacity:0.98999999;fill:url(#radialGradient3213);fill-opacity:1;"
  "stroke:none;stroke-width:3;stroke-miterlimit:4;stroke-opacity:1\"\n"
  "     d=\"M 23.630276,91.099213 C 18.27261,91.099213 13.974026,95.794067 "
  "13.974026,101.59921 C 13.974026,107.40435 18.272611,112.06796 "
  "23.630276,112.06796 L 45.474026,112.06796 C 50.831691,112.06796 "
  "55.130276,107.40435 55.130276,101.59921 C 55.130276,95.79407 "
  "50.831691,91.099213 45.474026,91.099213 L 23.630276,91.099213 z M "
  "26.099026,94.536713 L 43.317776,94.536713 C 47.546488,94.536713 "
  "50.942776,97.687948 50.942776,101.59921 C 50.942776,105.51047 "
  "47.546487,108.66171 43.317776,108.66171 L 26.099026,108.66171 C "
  "21.870314,108.66171 18.442776,105.51048 18.442776,101.59921 C "
  "18.442776,97.687938 21.870315,94.536713 26.099026,94.536713 z \"\n"
  "     id=\"path3211\" />\n"
  "  <path\n"
  "     id=\"path3215\"\n"
  "     d=\"M 77.53634,91.099213 C 72.178674,91.099213 67.88009,95.794067 "
  "67.88009,101.59921 C 67.88009,107.40435 72.178675,112.06796 "
  "77.53634,112.06796 L 99.380094,112.06796 C 104.73775,112.06796 "
  "109.03634,107.40435 109.03634,101.59921 C 109.03634,95.79407 "
  "104.73775,91.099213 99.380094,91.099213 L 77.53634,91.099213 z M "
  "80.00509,94.536713 L 97.223844,94.536713 C 101.45255,94.536713 "
  "104.84884,97.687948 104.84884,101.59921 C 104.84884,105.51047 "
  "101.45255,108.66171 97.223844,108.66171 L 80.00509,108.66171 C "
  "75.776378,108.66171 72.34884,105.51048 72.34884,101.59921 C "
  "72.34884,97.687938 75.776379,94.536713 80.00509,94.536713 z \"\n"
  "     "
  "style=\"opacity:0.98999999;fill:url(#radialGradient3217);fill-opacity:1;"
  "stroke:none;stroke-width:3;stroke-miterlimit:4;stroke-opacity:1\" />\n"
  "</svg>\n");

} // namespace massxpert

} // namespace MsXpS

