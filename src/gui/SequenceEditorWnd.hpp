/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes
#include <QMainWindow>
#include <QGraphicsScene>
#include <QClipboard>
#include <QProgressBar>


/////////////////////// pappsomspp includes


/////////////////////// libmass includes
#include <libXpertMass/CalcOptions.hpp>
#include <libXpertMass/IonizeRule.hpp>
#include <libXpertMass/PolChemDef.hpp>


/////////////////////// libmassgui includes
#include <libXpertMassGui/MassPeakShaperConfigDlg.hpp>


/////////////////////// Local includes
#include "ui_SequenceEditorWnd.h"
#include "AbstractMainTaskWindow.hpp"
#include "MonomerCodeEvaluator.hpp"
#include "PolChemDefRendering.hpp"
#include "SequenceEditorGraphicsView.hpp"


namespace MsXpS
{

namespace massxpert
{


class SequenceEditorGraphicsView;


class SequenceEditorFindDlg;

class SequenceEditorWnd : public AbstractMainTaskWindow
{
  Q_OBJECT

  public:
  SequenceEditorWnd(ProgramWindow *parent,
                    const QString &applicationName,
                    const QString &description);

  ~SequenceEditorWnd();

  bool m_forciblyClose   = 0;
  bool m_postInitialized = 0;

  SequenceEditorGraphicsView *mpa_editorGraphicsView;
  QGraphicsScene *mpa_editorGraphicsScene;

  QProgressBar *progressBar();

  bool openSequence(QString &);
  bool newSequence(QString &);
  bool maybeSave();

  void updateMonomerPosition(int);

  PolChemDefRendering *preparePolChemDefRendering(const QString &);
  void populateCalculationOptions();
  bool populateMonomerCodeList(bool = false);

  bool readFile(const QString &);

  const PolChemDefRendering *polChemDefRendering() const;
  PolChemDefRendering *polChemDefRendering();

  libXpertMass::Polymer *polymer();
  QList<libXpertMass::Prop *> *propList();

  const libXpertMass::CalcOptions &calcOptions() const;
  const libXpertMass::IonizeRule &ionizeRule() const;

  void clearCompletionsListSelection();
  void completionsListSelectAt(int);

  void setWindowModified(bool);

  void updateWindowTitle();

  void getsFocus();

  void wholeSequenceMasses(double * = 0, double * = 0);
  void selectedSequenceMasses(double * = 0, double * = 0);

  void updatePolymerEndsModifs();

  void newCalculator(QString, QString);

  signals:
  void polymerSequenceModifiedSignal();
  void polymerSequenceWndAboutToClose(SequenceEditorWnd *);
  void displayMassSpectrumSignal(const QString &title,
                                 const QByteArray &color_byte_array,
                                 pappso::TraceCstSPtr trace);


  public slots:
  bool save();
  bool saveAs();

  void updateMassesNewDecimalsOptions();
  void updateWholeSequenceMasses(bool = false);
  void updateSelectedSequenceMasses(bool = false);

  // libXpertMass::Sequence importers.
  void importRaw();
  void importPdbProt();

  void exportClipboard();
  void exportFile();
  bool exportSelectFile();

  void clipboardCopy(QClipboard::Mode = QClipboard::Clipboard);
  void clipboardCut(QClipboard::Mode = QClipboard::Clipboard);
  void clipboardPaste(QClipboard::Mode = QClipboard::Clipboard);
  void clipboardClear(QClipboard::Mode);
  void findSequence();

  void vignetteSizeChanged();
  void nameLineEditChanged(const QString &text);

  void calculationOptionsChanged();

  void leftModifOptionsChanged();
  void forceLeftModifOptionsChanged();
  void rightModifOptionsChanged();
  void forceRightModifOptionsChanged();

  void multiRegionSelectionOptionChanged(int);
  bool isMultiRegionSelection();

  void regionSelectionOligomerOptionChanged(bool);
  void regionSelectionResChainOptionChanged(bool);

  int coordinatesManuallyEdited(const QString &);

  void multiSelectionRegionOptionChanged(int);
  bool isMultiSelectionRegion();

  void monomerModifOptionChanged(int);

  void monomerCrossLinkOptionChanged(int);

  void modifMonomer();

  void modifPolymer();
  void modifLeftEnd();
  void modifRightEnd();

  void crossLinkMonomers();

  void cleave();
  void fragment();
  void massSearch();
  void mzCalculation();
  void compositions();
  void pkaPhPi();

  void decimalPlacesOptions();

  void newCalculatorWholeSequenceMasses();
  void newCalculatorSelectedSequenceMasses();

  void crossLinksPartiallyEncompassedSlot(int);

  void vignetteListWidgetItemDoubleClicked(QListWidgetItem *);

  QString getSequenceName() const;

  private:
  Ui::SequenceEditorWnd m_ui;

  libXpertMassGui::MassPeakShaperConfigDlg *mp_massPeakShaperConfigDlg = nullptr;

  libXpertMass::IsotopicDataSPtr msp_isotopicData = nullptr;

  // The results-exporting strings. ////////////////////////////////
  QString *mpa_resultsString;
  QString m_resultsFilePath;
  //////////////////////////////////// The results-exporting strings.

  QMenu *fileMenu;
  QMenu *fileImportMenu;
  QMenu *editMenu;
  QMenu *chemistryMenu;
  QMenu *optionsMenu;
  QMenu *calculatorMenu;

  QAction *closeAct;
  QAction *saveAct;
  QAction *saveAsAct;
  QAction *importRawAct;
  QAction *importPdbProtAct;
  QAction *exportClipboardAct;
  QAction *exportFileAct;
  QAction *exportSelectFileAct;

  QAction *clipboardCopyAct;
  QAction *clipboardCutAct;
  QAction *clipboardPasteAct;
  QAction *findSequenceAct;

  QAction *modifMonomerAct;
  QAction *modifPolymerAct;
  QAction *crossLinkMonomersAct;
  QAction *cleaveAct;
  QAction *fragmentAct;
  QAction *massSearchAct;
  QAction *mzCalculationAct;
  QAction *compositionsAct;
  QAction *pkaPhPiAct;

  QAction *decimalPlacesOptionsAct;

  QAction *newCalculatorWholeSequenceMassesAct;
  QAction *newCalculatorSelectedSequenceMassesAct;

  // Allocated but reparented to QStatusBar.
  QProgressBar *mp_progressBar;

  libXpertMass::Polymer *mpa_polymer = 0;
  PolChemDefRendering *mp_polChemDefRendering;
  libXpertMass::CalcOptions m_calcOptions;
  libXpertMass::IonizeRule m_ionizeRule;

  QList<libXpertMass::Prop *> m_propList;

libXpertMass::MassPeakShaperConfig m_massPeakShaperConfig;

  pappso::Trace m_syntheticMassSpectrum;

  // For the mass spectra that are synthesized and served.
  QByteArray m_colorByteArray;

  void writeSettings();
  void readSettings();

  // Before the creation of the polChemDef/polymer relationship.
  bool initialize();

  // Creates the polChemDef/polymer relationship.
  bool postInitialize();

  void closeEvent(QCloseEvent *event);

  void focusInEvent(QFocusEvent *event);
  void focusOutEvent(QFocusEvent *event);

  void createActions();
  void createMenus();

  void keyPressEvent(QKeyEvent *);

  void massSpectrumSynthesisMenuActivated(int index);
  bool loadIsotopicDataFromFile();
  void traceColorPushButtonClicked();
  void configureMassPeakShaper();
  void synthesizeMassSpectra();

  // The results-exporting functions. ////////////////////////////////
  void prepareResultsTxtString();
  //////////////////////////////////// The results-exporting functions.
};

} // namespace massxpert

} // namespace MsXpS
