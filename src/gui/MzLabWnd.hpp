/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef MZ_LAB_WND_HPP
#define MZ_LAB_WND_HPP


/////////////////////// Qt includes
#include <QObject>
#include <QMainWindow>
#include <libXpertMass/PolChemDefEntity.hpp>


/////////////////////// Local includes
#include "ui_MzLabWnd.h"
#include "AbstractMainTaskWindow.hpp"
#include <libXpertMass/PolChemDef.hpp>


namespace MsXpS
{

namespace massxpert
{


  class MzLabInputOligomerTableViewDlg;
  class ProgramWindow;
  class OligomerList;
  class OligomerPair;

  class MzLabWnd : public AbstractMainTaskWindow
  {
    Q_OBJECT

    public:
    MzLabWnd(ProgramWindow *parent,
             const QString &polChemDefFilePath,
             const QString &applicationName,
             const QString &description);

    ~MzLabWnd();

    bool m_forciblyClose = false;


    libXpertMass::PolChemDefCstSPtr polChemDefCstSPtr();
    libXpertMass::PolChemDefSPtr polChemDefSPtr();

    const libXpertMass::IonizeRule &ionizeRule() const;

    MzLabInputOligomerTableViewDlg *findDlg(const QString &);
    MzLabInputOligomerTableViewDlg *newInputList(QString, libXpertMass::MassType);

    bool inputListDlg(MzLabInputOligomerTableViewDlg **);
    bool inputListsDlg(MzLabInputOligomerTableViewDlg **,
                       MzLabInputOligomerTableViewDlg **);
    MzLabInputOligomerTableViewDlg *inputListDlg();

    bool inPlaceCalculation();

    bool maybeSave();

    public slots:
    MzLabInputOligomerTableViewDlg *newInputList();

    private:
    Ui::MzLabWnd m_ui;

    QList<double> m_listM1;
    QList<double> m_listM2;

    QStringList m_modifList;

    libXpertMass::PolChemDefSPtr msp_polChemDef;

    QList<MzLabInputOligomerTableViewDlg *> m_dlgList;
    ProgramWindow *mp_mainWindow;

    libXpertMass::IonizeRule m_ionizeRule;

    void destroyDlg(const QString &);

    double calculateTolerance(double);

    private slots:
    void massBasedActionsPushButton();
    void formulaBasedActionsPushButton();
    void matchBasedActionsPushButton();

    void deleteInputListItem();
    void inputListWidgetItemClicked(QListWidgetItem *);

    void updateWindowTitle();

    void ionizationChargeChanged(int);
    void ionizationLevelChanged(int);
    void ionizationFormulaChanged(const QString &);

    void readSettings();
    void writeSettings();

    bool initialize();

    void closeEvent(QCloseEvent *event);
  };

} // namespace massxpert

} // namespace MsXpS


#endif /* MZ_LAB_WND_HPP */
