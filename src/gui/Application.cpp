/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QDir>
#include <QMessageBox>

/////////////////////// Local includes
#include "config.h"
#include "Application.hpp"


namespace MsXpS
{

namespace massxpert
{


void
myMessageOutputFormat(QtMsgType type,
                      const QMessageLogContext &context,
                      const QString &msg)
{
// Define ANSI color codes
#define RESET "\033[0m"
#define RED "\033[31m"
#define GREEN "\033[32m"
#define YELLOW "\033[33m"
#define BLUE "\033[34m"
#define MAGENTA "\033[35m"
#define CYAN "\033[36m"
#define BOLD "\033[1m"

  QByteArray localMsg  = msg.toLocal8Bit();
  const char *file     = context.file ? context.file : "";
  const char *function = context.function ? context.function : "";

  QString prefix;
  QString color;


  switch(type)
  {
    case QtDebugMsg:
    prefix = "DEBUG: ";
    color  = QString("%1").arg(YELLOW);
      break;
    case QtInfoMsg:
    prefix = "INFO: ";
    color  = QString("%1").arg(GREEN);
      break;
    case QtWarningMsg:
    prefix = "WARNING: ";
    color  = QString("%1").arg(CYAN);
      break;
    case QtCriticalMsg:
    prefix = "CRITICAL: ";
    color  = QString("%1").arg(MAGENTA);
      break;
    case QtFatalMsg:
    prefix = "FATAL: ";
    color  = QString("%1").arg(RED);
      break;
  }

  fprintf(stderr,
    "\t\t\t%s%s%s%s:%s%u%s\n\t\t%s\n%s%s%s\n",
    color.toLocal8Bit().constData(),
    prefix.toLocal8Bit().constData(),
    RESET,
    file,
    color.toLocal8Bit().constData(),
    context.line,
    RESET,
    function,
    color.toLocal8Bit().constData(),
    localMsg.constData(),
    RESET);
}


Application::Application(int &argc, char **argv, const QString &applicationName)
  : QApplication{argc, argv}, m_applicationName{applicationName}
{
  QPixmap splashPixmap(":/images/splashscreen.png");

  mpa_splash = new QSplashScreen(splashPixmap, Qt::WindowStaysOnTopHint);
  mpa_splash->show();

  QTimer::singleShot(2000, this, SLOT(destroySplash()));

  // We now have to perform a number of initialization steps.

  QCoreApplication::setOrganizationName(m_applicationName);
  QCoreApplication::setOrganizationDomain("msxpertsuite.org");
  QCoreApplication::setApplicationName(m_applicationName);

  // The default window icon.
  QString pixmapFileName = ":/images/icons/32x32/massxpert2.png";
  QPixmap iconPixmap(pixmapFileName);
  QIcon icon(iconPixmap);
  setWindowIcon(icon);

  // Set the debugging message formatting pattern.
  // qSetMessagePattern(QString("Debug: %{file}@%{line}-%{function}():
  // %{message}"));
  qInstallMessageHandler(myMessageOutputFormat);
}


Application::~Application()
{
}


QString
Application::applicationName()
{
  return m_applicationName;
}


void
Application::destroySplash()
{
  delete mpa_splash;

  mpa_splash = 0;
}


} // namespace massxpert

} // namespace MsXpS
