/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes
#include <libXpertMass/MassDataClient.hpp>
#include <QMainWindow>
#include <QDir>
#include <QStringList>


/////////////////////// libmass includes
#include <libXpertMass/PolChemDef.hpp>
#include <libXpertMass/PolChemDefSpec.hpp>
#include <libXpertMass/MassDataServer.hpp>
#include <libXpertMass/MassDataClient.hpp>


/////////////////////// libmassgui includes
#include <libXpertMassGui/MassDataClientServerConfigDlg.hpp>
#include <libXpertMassGui/IsotopicClusterGeneratorDlg.hpp>
#include <libXpertMassGui/IsotopicClusterShaperDlg.hpp>


/////////////////////// local includes
#include "AboutDlg.hpp"
#include "../nongui/UserSpec.hpp"
#include "../nongui/ConfigSettings.hpp"


class QAction;
class QMenu;
class QTextEdit;


namespace MsXpS
{

namespace massxpert
{


class SequenceEditorWnd;


class ProgramWindow : public QMainWindow
{
  Q_OBJECT

  public:
  ProgramWindow(const QString &applicationName, const QString &description);
  ~ProgramWindow();

  QString configSettingsFilePath() const;
  const UserSpec &userSpec() const;
  const ConfigSettings *configSettings() const;

  bool setConfigSettingsManually();

  void initializeDecimalPlacesOptions();

  void setLastFocusedSeqEdWnd(SequenceEditorWnd *);

  QList<libXpertMass::PolChemDefSpec *> *polChemDefCatList();
  libXpertMass::PolChemDefSpec *polChemDefSpecName(const QString &);
  libXpertMass::PolChemDefSpec *polChemDefSpecFilePath(const QString &);

  void polChemDefCatStringList(QStringList &stringList);

  std::vector<libXpertMass::PolChemDefCstSPtr> polChemDefList();
  libXpertMass::PolChemDefCstSPtr polChemDefByName(const QString &name);

  bool isSequenceEditorWnd(SequenceEditorWnd *) const;

  public slots:
  void openPolChemDef();
  void newPolChemDef();

  void newCalculator();

  void openSequence(const QString &fileName = QString());
  void openSampleSequence();
  void newSequence();
  void delistSequenceEditorWnd(SequenceEditorWnd *wnd);

  void mzLab();

  void about();
  void showAboutDlg();

  void massListSorter();
  void seqManip();
  void showIsotopicClusterGeneratorDlg();
  void showIsotopicClusterShaperDlg();

  signals:

  void aboutToCloseSignal();

  protected:
  void closeEvent(QCloseEvent *);

  private:
  QString m_applicationName;
  QString m_windowDescription;

  UserSpec m_userSpec;
  QString m_configSettingsFilePath;
  ConfigSettings *mpa_configSettings = nullptr;

  void readSettings();
  void writeSettings();

  // Last sequence editor window that got the focus.
  SequenceEditorWnd *mp_lastFocusedSeqEdWnd;

  // The QList of all the sequence editor windows.
  QList<SequenceEditorWnd *> m_sequenceEditorWndList;

  // List of all the polymer chemistry definition specifications that
  // were created during parsing of all the different catalogue files
  // on the system.
  QList<libXpertMass::PolChemDefSpec *> m_polChemDefCatList;

  // List of all the polymer chemistry definitions that are loaded in
  // memory and usable to load polymer sequences.
  std::vector<libXpertMass::PolChemDefCstSPtr> m_polChemDefList;

  libXpertMass::MassDataClient *mpa_massDataClient = nullptr;
  libXpertMass::MassDataServer *mpa_massDataServer = nullptr;

  QMenu *mp_fileMenu;

  QMenu *mp_xpertDefMenu;
  QMenu *mp_xpertCalcMenu;
  QMenu *mp_xpertEditMenu;
  QMenu *mp_xpertMinerMenu;

  QMenu *mp_utilitiesMenu;

  QMenu *mp_helpMenu;

  QAction *mp_openPolChemDefAct;
  QAction *mp_newPolChemDefAct;

  QAction *mp_newCalculatorAct;

  QAction *mp_openSequenceAct;
  QAction *mp_openSampleSequenceAct;
  QAction *mp_newSequenceAct;

  QAction *mp_mzLabAct;

  QAction *mp_massListSorterAct;
  QAction *mp_seqManipAct;
  QAction *mp_isotopicClusterGeneratorDlgAct;
  QAction *mp_isotopicClusterShaperDlgAct;

  QAction *mp_clientServerConfigDlgAct;

  QAction *mp_exitAct;
  QAction *mp_configSettingsAct;

  QAction *mp_aboutAct;
  QAction *mp_aboutQtAct;

  libXpertMassGui::IsotopicClusterGeneratorDlg *mp_isotopicClusterGeneratorDlg =
    nullptr;
  libXpertMassGui::IsotopicClusterShaperDlg *mp_isotopicClusterShaperDlg = nullptr;

  libXpertMassGui::MassDataClientServerConfigDlg *mp_clientServerConfigDlg = nullptr;

  void addToMenu(QObject *, const QStringList &, QMenu *, const char *);

  void createActions();
  void createMenus();
  void createStatusBar();

  void setupWindow();
  bool setupConfigSettings();
  bool initializeSystemConfig();
  bool initializeUserConfig();

  bool startServer();
  void stopServer();
  bool startClient(const QString &ip_address,
                   int port_number,
                   int connection_frequency = 1);
  void stopClient();

  void dispatchReceivedData(const QByteArray &byte_array);

  private slots:

  void massDataToBeServed(const QByteArray &byte_array);
};

} // namespace massxpert

} // namespace MsXpS

