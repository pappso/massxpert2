/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef CLEAVE_OLIGOMER_TABLE_VIEW_SORT_PROXY_MODEL_HPP
#define CLEAVE_OLIGOMER_TABLE_VIEW_SORT_PROXY_MODEL_HPP


/////////////////////// Qt includes
#include <QSortFilterProxyModel>


namespace MsXpS
{

	namespace massxpert
	{



class CleaveOligomerTableViewSortProxyModel : public QSortFilterProxyModel
{
  private:
  bool sortCoordinates(QString, QString) const;
  bool sortName(QString, QString) const;

  int m_partialFilter;

  double m_monoFilter;
  double m_avgFilter;
  double m_tolerance;

  int m_chargeFilter;

  protected:
  bool lessThan(const QModelIndex &, const QModelIndex &) const;

  public:
  CleaveOligomerTableViewSortProxyModel(QObject * = 0);
  ~CleaveOligomerTableViewSortProxyModel();

  void setPartialFilter(int);

  void setMonoFilter(double);
  void setAvgFilter(double);
  void setTolerance(double);

  void setChargeFilter(int);

  bool filterAcceptsRow(int, const QModelIndex &) const;

  void applyNewFilter();
};

} // namespace massxpert

} // namespace MsXpS


#endif // CLEAVE_OLIGOMER_TABLE_VIEW_SORT_PROXY_MODEL_HPP
