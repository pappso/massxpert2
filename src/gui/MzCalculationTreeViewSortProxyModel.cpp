/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QDebug>


/////////////////////// Local includes
#include "MzCalculationTreeViewSortProxyModel.hpp"


namespace MsXpS
{

namespace massxpert
{


  MzCalculationTreeViewSortProxyModel::MzCalculationTreeViewSortProxyModel(
    QObject *parent)
    : QSortFilterProxyModel(parent)
  {
  }


  MzCalculationTreeViewSortProxyModel::~MzCalculationTreeViewSortProxyModel()
  {
  }


  bool
  MzCalculationTreeViewSortProxyModel::lessThan(const QModelIndex &left,
                                                const QModelIndex &right) const
  {
    QVariant leftData  = sourceModel()->data(left);
    QVariant rightData = sourceModel()->data(right);

    if(leftData.typeId() == QMetaType::Int)
      {
        // The Partial column

        int leftValue  = leftData.toInt();
        int rightValue = rightData.toInt();

        return (leftValue < rightValue);
      }
    if(leftData.typeId() == QMetaType::Bool)
      {
        // The libXpertMass::Modif column

        int leftValue  = leftData.toInt();
        int rightValue = rightData.toInt();

        return (leftValue < rightValue);
      }
    else if(leftData.typeId() == QMetaType::Double)
      {
        bool ok = false;

        double leftValue = leftData.toDouble(&ok);
        Q_ASSERT(ok);
        double rightValue = rightData.toDouble(&ok);
        Q_ASSERT(ok);

        return (leftValue < rightValue);
      }

    if(leftData.typeId() == QMetaType::QString)
      {
        QString leftString  = leftData.toString();
        QString rightString = rightData.toString();

        return (QString::localeAwareCompare(leftString, rightString) < 0);
      }

    return true;
  }


} // namespace massxpert

} // namespace MsXpS
