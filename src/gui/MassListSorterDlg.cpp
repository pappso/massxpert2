/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QLocale>
#include <QVBoxLayout>
#include <QLabel>
#include <QGroupBox>
#include <QObject>
#include <QTextEdit>
#include <QComboBox>
#include <QPushButton>
#include <QMainWindow>


/////////////////////// Local includes
#include "MassListSorterDlg.hpp"
#include "../nongui/MassList.hpp"
#include "ProgramWindow.hpp"


namespace MsXpS
{

namespace massxpert
{


  MassListSorterDlg::MassListSorterDlg(QWidget *parent,
                                       const QString &applicationName,
                                       const QString &description)
    : QDialog(parent, Qt::Dialog)
  {
    mp_parent = static_cast<QMainWindow *>(parent);

    QVBoxLayout *mainLayout = new QVBoxLayout;

    QLabel *label = new QLabel(tr("Mass List Sorter Plugin"));

    mainLayout->addWidget(label, 0, Qt::AlignHCenter);

    createEditorGroupBox();
    mainLayout->addWidget(mp_editorGroupBox);

    createActionGroupBox();
    mainLayout->addWidget(mp_actionGroupBox);

    setLayout(mainLayout);

    setAttribute(Qt::WA_DeleteOnClose);

    // Update the window title because the window title element in m_ui might be
    // either erroneous or empty.
    setWindowTitle(QString("%1 - %2").arg(applicationName).arg(description));

    connect(static_cast<ProgramWindow *>(mp_parent),
            &ProgramWindow::aboutToCloseSignal,
            this,
            &MassListSorterDlg::parentAboutToClose);
  }


  void
  MassListSorterDlg::parentAboutToClose()
  {
    // When the program window closes, just reject this dialog box to close it.
    QDialog::reject();
  }


  void
  MassListSorterDlg::createEditorGroupBox()
  {
    mp_editorGroupBox = new QGroupBox(tr("Manipulated Mass Lists"));

    QHBoxLayout *layout = new QHBoxLayout;

    mp_inputEditor = new QTextEdit;
    layout->addWidget(mp_inputEditor);

    mp_outputEditor = new QTextEdit;
    layout->addWidget(mp_outputEditor);

    mp_editorGroupBox->setLayout(layout);
  }


  void
  MassListSorterDlg::createActionGroupBox()
  {
    mp_actionGroupBox = new QGroupBox(tr("Actions"));

    QHBoxLayout *layout = new QHBoxLayout;

    mp_actionComboBox = new QComboBox();

    QStringList menuItems = QStringList()
                            << tr("Ascending Order") << tr("Descending Order");

    mp_actionComboBox->addItems(menuItems);

    layout->addWidget(mp_actionComboBox);

    mp_executePushButton = new QPushButton(tr("&Execute"));
    connect(mp_executePushButton, SIGNAL(clicked()), this, SLOT(execute()));

    layout->addWidget(mp_executePushButton);

    mp_actionGroupBox->setLayout(layout);
  }


  void
  MassListSorterDlg::execute()
  {
    // What's the task to be performed?

    QString comboText = mp_actionComboBox->currentText();

    // What's the text to work on?
    QString docText = mp_inputEditor->toPlainText();

    // Make a mass list with the text.
    MassList inputMassList("INPUT_MASS_LIST", docText);

    // Check the result.
    if(inputMassList.makeMassList() == -1)
      return;

    // Do the sort.
    if(comboText == tr("Ascending Order"))
      inputMassList.sortAscending();
    else if(comboText == tr("Descending Order"))
      inputMassList.sortDescending();

    // Convert list to text.
    inputMassList.makeMassText();

    // Clear the previous output text edit widget.
    mp_outputEditor->clear();

    // Output the new text.
    mp_outputEditor->setPlainText(inputMassList.massText());
  }

} // namespace massxpert

} // namespace MsXpS
