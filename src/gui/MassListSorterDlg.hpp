/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef MASS_LIST_SORTER_DLG_HPP
#define MASS_LIST_SORTER_DLG_HPP

#include <QDialog>
#include <QTextDocument>
#include <QHash>

class QGroupBox;
class QComboBox;
class QPushButton;
class QTextEdit;
class QMainWindow;


namespace MsXpS
{

namespace massxpert
{


  class MassListSorterDlg : public QDialog
  {
    Q_OBJECT

    private:
    void createEditorGroupBox();
    void createActionGroupBox();

    QMainWindow *mp_parent;


    // Allocated but ownership taken by the dialog.  Thus mp_ and not
    // mpa_.
    QTextEdit *mp_inputEditor;
    QTextEdit *mp_outputEditor;
    QGroupBox *mp_editorGroupBox;
    QGroupBox *mp_actionGroupBox;
    QComboBox *mp_actionComboBox;
    QPushButton *mp_executePushButton;

    private slots:
    void execute();
    void parentAboutToClose();

    public:
    MassListSorterDlg(QWidget *parent,
                      const QString &applicationName,
                      const QString &description);
  };

} // namespace massxpert

} // namespace MsXpS


// namespace MsXpS

#endif // MASS_LIST_SORTER_DLG_HPP
