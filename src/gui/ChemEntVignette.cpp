/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QDebug>


/////////////////////// Local includes
#include "ChemEntVignette.hpp"
#include "ChemEntVignetteRenderer.hpp"


namespace MsXpS
{

namespace massxpert
{

  ChemEntVignette::ChemEntVignette(QGraphicsSvgItem *parent)
    : QGraphicsSvgItem(parent)
  {
    mp_owner    = 0;
    mp_renderer = 0;

    //   qDebug() << __FILE__ << __LINE__
    // 	    << "ChemEntVignette()" <<(const void*) this;
  }


  ChemEntVignette::ChemEntVignette(const QString &filePath,
                                   QGraphicsSvgItem *parent)
    : QGraphicsSvgItem(filePath, parent)
  {
    mp_owner    = 0;
    mp_renderer = 0;

    //   qDebug() << __FILE__ << __LINE__
    // 	    << "ChemEntVignette()" <<(const void*) this;
  }


  ChemEntVignette::~ChemEntVignette()
  {
    // We do not own these:
    // mp_owner
    // mp_renderer

    //   qDebug() << __FILE__ << __LINE__
    // 	    << "~ChemEntVignette()" <<(const void*) this;

    mp_renderer->decrementRefCount();
  }


  QString
  ChemEntVignette::name()
  {
    return m_name;
  }


  void
  ChemEntVignette::setName(QString name)
  {
    m_name = name;
  }


  libXpertMass::PolChemDefEntity *
  ChemEntVignette::owner()
  {
    return mp_owner;
  }


  void
  ChemEntVignette::setOwner(libXpertMass::PolChemDefEntity *owner)
  {
    Q_ASSERT(owner);

    mp_owner = owner;
  }


  void
  ChemEntVignette::setSharedRenderer(ChemEntVignetteRenderer *renderer)
  {
    Q_ASSERT(renderer);

    mp_renderer = renderer;
    mp_renderer->incrementRefCount();

    QGraphicsSvgItem::setSharedRenderer(static_cast<QSvgRenderer *>(renderer));
  }


  ChemEntVignetteRenderer *
  ChemEntVignette::renderer()
  {
    return mp_renderer;
  }

} // namespace massxpert

} // namespace MsXpS
