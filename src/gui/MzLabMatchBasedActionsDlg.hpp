/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef MZ_LAB_MATCH_BASED_ACTIONS_DLG_HPP
#define MZ_LAB_MATCH_BASED_ACTIONS_DLG_HPP
// #pragma message("including MZ_LAB_MATCH_BASED_ACTIONS_DLG_HPP")

/////////////////////// Qt includes
#include <QMainWindow>


/////////////////////// Local includes
#include <libXpertMass/globals.hpp>
#include "MzLabInputOligomerTableViewDlg.hpp"
#include "ui_MzLabMatchBasedActionsDlg.h"

namespace MsXpS
{

	namespace massxpert
	{



class MzLabMatchBasedActionsDlg : public QDialog
{
  Q_OBJECT

  private:
  Ui::MzLabMatchBasedActionsDlg m_ui;

  QString m_configSettingsFilePath;

  MzLabWnd *mp_mzLabWnd = 0;

  void closeEvent(QCloseEvent *event);

  public:
  MzLabMatchBasedActionsDlg(QWidget *, const QString &configSettingsFilePath);

  ~MzLabMatchBasedActionsDlg();

  double calculateTolerance(double);

  void fillMolecularMassList(const OligomerList &,
                             QList<double> *,
                             libXpertMass::MassType &);

  private slots:
  void performMatchWorkPushButton();
  void helpPushButton();
};

} // namespace massxpert

} // namespace MsXpS


#endif // MZ_LAB_MATCH_BASED_ACTIONS_DLG_HPP
// #pragma message("done...including MZ_LAB_MATCH_BASED_ACTIONS_DLG_HPP")
