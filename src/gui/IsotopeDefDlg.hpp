/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// libmass includes
#include <libXpertMass/globals.hpp>
#include <libXpertMass/PolChemDef.hpp>
#include <libXpertMass/Isotope.hpp>
#include <libXpertMass/IsotopicData.hpp>


/////////////////////// libmassgui includes
#include <libXpertMassGui/IsotopicDataTableView.hpp>
#include <libXpertMassGui/IsotopicDataTableViewModel.hpp>


/////////////////////// Local includes
#include "AbstractPolChemDefDependentDlg.hpp"
#include "ui_IsotopeDefDlg.h"
#include "PolChemDefWnd.hpp"


namespace MsXpS
{

namespace massxpert
{


class IsotopeDefDlg : public AbstractPolChemDefDependentDlg
{
  Q_OBJECT

  public:
  IsotopeDefDlg(libXpertMass::PolChemDefSPtr polChemDefSPtr,
                PolChemDefWnd *polChemDefWnd,
                const QString &settingsFilePath,
                const QString &applicationName,
                const QString &description);

  ~IsotopeDefDlg();

  bool initialize();

  void updateMassAndProbDetails(std::set<int> rows_set);

  public slots:

  // VALIDATION
  bool validate();

  // WIDGETRY
  void selectionChanged(const QItemSelection &selected,
                        const QItemSelection &deselected);
  void allSelectedIndices(QModelIndexList selected_indices);

  void selectedRowsChanged(std::set<int> rows_set);

  // SAVING
  bool loadData();
  bool save();
  bool saveAs();


  private:
  Ui::IsotopeDefDlg m_ui;

  // The table view model that manages the isotopic data data (one isotope per
  // table row)
  libXpertMassGui::IsotopicDataTableViewModel *mpa_isotopicDataTableViewModel;


  void closeEvent(QCloseEvent *event);

  void writeSettings();
  void readSettings();

  bool setupIsotopicDataTableView();

  void message(const QString &message, int timeout = 3000);
};

} // namespace massxpert

} // namespace MsXpS

