/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QMessageBox>
#include <QFileDialog>
#include <QSettings>

#include <globals.hpp>
#include <pappsomspp/utils.h>

/////////////////////// libmass includes
#include <libXpertMass/PolChemDefEntity.hpp>
#include <libXpertMass/IsotopicDataUserConfigHandler.hpp>
#include <libXpertMass/IsotopicClusterGenerator.hpp>
#include <libXpertMass/IsotopicClusterShaper.hpp>
#include <libXpertMass/IsotopicData.hpp>
#include <libXpertMass/MassPeakShaper.hpp>
#include <libXpertMass/MassPeakShaperConfig.hpp>
#include <libXpertMassGui/ColorSelector.hpp>


/////////////////////// Local includes
#include "../nongui/globals.hpp"
#include "../nongui/FragmentOligomer.hpp"
#include "FragmentationDlg.hpp"


namespace MsXpS
{

namespace massxpert
{


FragmentationDlg::FragmentationDlg(
  SequenceEditorWnd *editorWnd,
  libXpertMass::Polymer *polymer,
  const libXpertMass::PolChemDefCstSPtr polChemDefCstSPtr,
  const QString &configSettingsFilePath,
  const QString &applicationName,
  const QString &description,
  const libXpertMass::CalcOptions &calcOptions,
  const libXpertMass::IonizeRule *ionizeRule)
  : AbstractSeqEdWndDependentDlg(editorWnd,
                                 polymer,
                                 polChemDefCstSPtr,
                                 configSettingsFilePath,
                                 "CompositionsDlg",
                                 applicationName,
                                 description),
    m_calcOptions{calcOptions},
    mp_ionizeRule{ionizeRule}
{
  if(!mp_polymer || !mcsp_polChemDef)
    qFatal("Fatal error at %s@%d. Program aborted.", __FILE__, __LINE__);

  if(!mp_ionizeRule)
    qFatal("Fatal error at %s@%d. Program aborted.", __FILE__, __LINE__);

  if(!initialize())
    qFatal("Fatal error at %s@%d. Program aborted.", __FILE__, __LINE__);
}


FragmentationDlg::~FragmentationDlg()
{
  m_oligomerList.clear();

  delete mpa_resultsString;

  delete mpa_oligomerTableViewModel;
  delete mpa_proxyModel;

  delete mpa_fragmenter;
}


void
FragmentationDlg::writeSettings()
{
  QSettings settings(m_configSettingsFilePath, QSettings::IniFormat);

  settings.beginGroup(m_wndTypeName);
  settings.setValue("geometry", saveGeometry());
  settings.setValue("oligomersSplitter", m_ui.oligomersSplitter->saveState());
  settings.setValue("oligoDetailsSplitter",
                    m_ui.oligoDetailsSplitter->saveState());

  settings.endGroup();
}


void
FragmentationDlg::readSettings()
{
  QSettings settings(m_configSettingsFilePath, QSettings::IniFormat);

  settings.beginGroup(m_wndTypeName);

  restoreGeometry(settings.value("geometry").toByteArray());

  m_ui.oligomersSplitter->restoreState(
    settings.value("oligomersSplitter").toByteArray());

  m_ui.oligoDetailsSplitter->restoreState(
    settings.value("oligoDetailsSplitter").toByteArray());

  settings.endGroup();
}


bool
FragmentationDlg::initialize()
{
  m_ui.setupUi(this);

  // Update the window title because the window title element in m_ui might be
  // either erroneous or empty.
  setWindowTitle(
    QString("%1 - %2").arg(m_applicationName).arg(m_windowDescription));

  readSettings();

  m_ui.delimiterLineEdit->setText("$");

  populateSelectedOligomerData();
  populateFragSpecList();

  m_ui.fragmentationPatternListWidget->setSelectionMode(
    QAbstractItemView::MultiSelection);

  mpa_oligomerTableViewModel = 0;
  mpa_proxyModel             = 0;

  // Set pointers to 0 so that after the setupTableView call below
  // they'll get their proper value. We'll then be able to free all
  // that stuff in the destructor.
  mpa_fragmenter             = 0;
  mpa_proxyModel             = 0;
  mpa_oligomerTableViewModel = 0;

  setupTableView();

  // Set the default color of the mass spectra trace upon mass spectrum
  // synthesis
  QColor color("black");
  // Now prepare the color in the form of a QByteArray
  QDataStream stream(&m_colorByteArray, QIODevice::WriteOnly);
  stream << color;

  // The tolerance when filtering mono/avg masses...
  QStringList stringList;

  stringList << tr("AMU") << tr("PCT") << tr("PPM");

  m_ui.toleranceComboBox->insertItems(0, stringList);

  m_ui.toleranceComboBox->setToolTip(
    tr("AMU: atom mass unit \n"
       "PCT: percent \n"
       "PPM: part per million"));

  filterAct = new QAction(tr("Toggle Filtering"), this);
  filterAct->setShortcut(QKeySequence(Qt::CTRL | Qt::Key_F));
  this->addAction(filterAct);
  connect(filterAct, SIGNAL(triggered()), this, SLOT(filterOptionsToggled()));

  m_ui.filteringOptionsGroupBox->addAction(filterAct);
  // When the dialog box is created it is created with the groupbox
  // unchecked.
  m_ui.filteringOptionsFrame->setVisible(false);

  // When the filtering group box will be opened, the focus will be on the
  // first widget of the groupbox:
  mp_focusWidget = m_ui.filterPatternLineEdit;

  // The results-exporting menus. ////////////////////////////////

  QStringList comboBoxItemList;

  comboBoxItemList << tr("To Clipboard") << tr("To File") << tr("Select File");

  m_ui.exportResultsComboBox->addItems(comboBoxItemList);

  connect(m_ui.exportResultsComboBox,
          SIGNAL(activated(int)),
          this,
          SLOT(exportResults(int)));

  comboBoxItemList.clear();

  // The color button
  connect(m_ui.colorSelectorPushButton,
          &QPushButton::clicked,
          this,
          &FragmentationDlg::traceColorPushButtonClicked);

  comboBoxItemList.insert((int)MassSpectrumSynthesisActions::LOAD_ISOTOPIC_DATA,
                          "Load isotopic data from file");

  comboBoxItemList.insert(
    (int)MassSpectrumSynthesisActions::CONFIGURE_MASS_PEAK_SHAPER,
    "Configure the mass peak shaper");

  comboBoxItemList.insert(
    (int)MassSpectrumSynthesisActions::SYNTHESIZE_MASS_SPECTRA,
    "Synthesize the mass spectra");

  comboBoxItemList.insert(
    (int)MassSpectrumSynthesisActions::COPY_MASS_SPECTRUM_TO_CLIPBOARD,
    "Mass spectrum not yet available");

  comboBoxItemList.insert(
    (int)MassSpectrumSynthesisActions::SAVE_MASS_SPECTRUM_TO_FILE,
    "Mass spectrum not yet available");

  m_ui.massSpectrumSynthesisComboBox->addItems(comboBoxItemList);

  connect(m_ui.massSpectrumSynthesisComboBox,
          SIGNAL(activated(int)),
          this,
          SLOT(massSpectrumSynthesisMenuActivated(int)));

  mpa_resultsString = new QString();

  //////////////////////////////////// The results-exporting menus.

  connect(m_ui.fragmentPushButton, SIGNAL(clicked()), this, SLOT(fragment()));

  connect(m_ui.filterPatternLineEdit,
          SIGNAL(returnPressed()),
          this,
          SLOT(filterPattern()));

  connect(m_ui.filterMonoMassLineEdit,
          SIGNAL(returnPressed()),
          this,
          SLOT(filterMonoMass()));

  connect(m_ui.filterAvgMassLineEdit,
          SIGNAL(returnPressed()),
          this,
          SLOT(filterAvgMass()));

  connect(m_ui.filterChargeLineEdit,
          SIGNAL(returnPressed()),
          this,
          SLOT(filterCharge()));

  connect(m_ui.filteringOptionsGroupBox,
          SIGNAL(clicked(bool)),
          this,
          SLOT(filterOptions(bool)));

  show();

  return true;
}

bool
FragmentationDlg::populateSelectedOligomerData()
{
  libXpertMass::CoordinateList coordList;

  bool res = mp_editorWnd->mpa_editorGraphicsView->selectionIndices(&coordList);

  if(!res)
    {
      QMessageBox::information(this,
                               tr("massxpert - Fragmentation"),
                               tr("No oligomer is selected. "
                                  "Select an oligomer first"),
                               QMessageBox::Ok);
      return false;
    }

  if(coordList.size() > 1)
    {
      QMessageBox::information(this,
                               tr("massxpert - Fragmentation"),
                               tr("Fragmentation simulations with\n"
                                  "multi-region selection are not "
                                  "supported."),
                               QMessageBox::Ok);
      return false;
    }

  m_calcOptions.setCoordinateList(coordList);

  libXpertMass::Coordinates *coordinates = coordList.last();

  m_ui.oligomerStartLabel->setText(QString().setNum(coordinates->start() + 1));

  m_ui.oligomerEndLabel->setText(QString().setNum(coordinates->end() + 1));

  return true;
}


void
FragmentationDlg::populateFragSpecList()
{
  for(int iter = 0; iter < mcsp_polChemDef->fragSpecList().size(); ++iter)
    {
      libXpertMass::FragSpec *fragSpec = mcsp_polChemDef->fragSpecList().at(iter);
      Q_ASSERT(fragSpec);

      m_ui.fragmentationPatternListWidget->addItem(fragSpec->name());
    }

  return;
}


void
FragmentationDlg::setupTableView()
{
  // Model stuff all thought for sorting.
  mpa_oligomerTableViewModel =
    new FragmentOligomerTableViewModel(&m_oligomerList, this);

  mpa_proxyModel = new FragmentOligomerTableViewSortProxyModel(this);
  mpa_proxyModel->setSourceModel(mpa_oligomerTableViewModel);
  mpa_proxyModel->setFilterKeyColumn(-1);

  m_ui.oligomerTableView->setModel(mpa_proxyModel);
  m_ui.oligomerTableView->setParentDlg(this);
  m_ui.oligomerTableView->setOligomerList(&m_oligomerList);
  mpa_oligomerTableViewModel->setTableView(m_ui.oligomerTableView);
}


void
FragmentationDlg::fragment()
{
  // Update the options from the parent window.
  m_calcOptions = mp_editorWnd->calcOptions();

  // And now override the selection data by using the
  // selection-specific function below.
  if(!populateSelectedOligomerData())
    return;

  // For each fragmentation specification selected in the list of
  // available fragmentation specifications we have to construct a
  // FragOptions instance and add it to the list of such frag
  // options. If no item is currently selected, just return.

  // What are the currently selected specifications ?
  QList<QListWidgetItem *> selectionList =
    m_ui.fragmentationPatternListWidget->selectedItems();

  if(selectionList.isEmpty())
    return;

  // We'll need to use the specifications in the polymer chemistry
  // definition... for each frag options object.
  QList<libXpertMass::FragSpec *> refList = mcsp_polChemDef->fragSpecList();

  // We'll have to store all the fragOptions in a list:
  QList<FragOptions *> fragOptionList;

  // We'll need a fragmentation specification each time we iterate in
  // a new specification item below.
  libXpertMass::FragSpec fragSpec(mcsp_polChemDef, "NOT_SET");

  // Finally parse all the different fragmentation specification items
  // currently selected in the list widget.

  for(int iter = 0; iter < selectionList.size(); ++iter)
    {
      QListWidgetItem *item = selectionList.at(iter);

      if(item->isSelected())
        {
          // Need to create a new fragOptions with the correct
          // fragmentation specification. First get its name from the
          // list widget. Then use that name to find the FragSpec
          // from the list of frag specs in the polymer chemistry
          // definition. Finally allocate a new frag options object
          // and set data into it.

          QString name = item->text();

          int res = libXpertMass::FragSpec::isNameInList(name, refList, &fragSpec);

          if(res == -1)
            {
              return;
            }

          FragOptions *fragOptions = new FragOptions(fragSpec);

          // The user might ask that some formulas be applied while
          // making the fragmentation with the selection
          // fragmentation option. Lets feed the FragOptions
          // instance with the related formulas.

          if(m_ui.decompositionsMinusWaterCheckBox->checkState() == Qt::Checked)
            {
              fragOptions->addFormula(QString("-H2O"));
            }

          if(m_ui.decompositionsMinusAmmoniaCheckBox->checkState() ==
             Qt::Checked)
            {
              fragOptions->addFormula(QString("-NH3"));
            }

          if(!m_ui.decompositionsFormulaLineEdit->text().isEmpty())
            {
              libXpertMass::Formula formula(
                m_ui.decompositionsFormulaLineEdit->text());

              // At this point, we should check if the formula is valid.

              libXpertMass::IsotopicDataCstSPtr isotopic_data_csp =
                mcsp_polChemDef->getIsotopicDataCstSPtr();

                qDebug() << "Going to validate decomposition formula" << formula.toString();

              if(!formula.validate(isotopic_data_csp))
                {
                  QMessageBox::information(
                    0,
                    tr("massXpert - Fragmentation"),
                    tr("Failed to validate the decomposition formula."),
                    QMessageBox::Ok);

                  return;
                }

              fragOptions->addFormula(formula);
            }

          // We got the coordinates of the oligomer to fragment in the
          // mp_calcOptions from the sequence editor window.

          libXpertMass::Coordinates *coordinates =
            m_calcOptions.coordinateList().last();

          fragOptions->setStartIndex(coordinates->start());
          fragOptions->setEndIndex(coordinates->end());


          // Set the ionization levels.
          int valueStart = m_ui.ionizeLevelStartSpinBox->value();
          int valueEnd   = m_ui.ionizeLevelEndSpinBox->value();

          if(valueStart > valueEnd)
            {
              fragOptions->setStartIonizeLevel(valueEnd);
              fragOptions->setEndIonizeLevel(valueStart);
            }
          else
            {
              fragOptions->setStartIonizeLevel(valueStart);
              fragOptions->setEndIonizeLevel(valueEnd);
            }


          //   qDebug() << "Start:" << fragOptions.startIonizeLevel();
          //   qDebug() << "End:" << fragOptions.endIonizeLevel();

          // Add the frag options to the list.
          fragOptionList.append(fragOptions);
        }
      else
        continue;
    }

  // At this point the list of frag options should contain the same
  // number of items as the number of items selected in the list
  // widget.

  Q_ASSERT(fragOptionList.size() == selectionList.size());

  // The list in which we'll store all the allocated oligomers.
  OligomerList oligomerList;

  // Now that we have all the fragmentation options in the list, we
  // can allocate the fragmenter! Note that this function might be
  // called many times, which means that we first have to free the
  // mpa_fragmenter allocated member if non-nullptr.

  delete mpa_fragmenter;

  // Update the mass calculation engine's configuration data
  // directly from the sequence editor window.
  m_calcOptions = mp_editorWnd->calcOptions();

  mpa_fragmenter = new Fragmenter(
    mp_polymer, mcsp_polChemDef, fragOptionList, m_calcOptions, *mp_ionizeRule);

  mpa_fragmenter->setOligomerList(&oligomerList);

  if(!mpa_fragmenter->fragment())
    {
      delete mpa_fragmenter;

      QMessageBox::critical(this,
                            tr("massXpert - libXpertMass::Polymer Cleavage"),
                            tr("Failed to perform fragmentation."),
                            QMessageBox::Ok);

      return;
    }

  FragmentOligomerSPtr oligomer_sp =
    std::dynamic_pointer_cast<FragmentOligomer>(oligomerList.first());

  // qDebug()
  //<< "First fragmentation oligomer has formula (elemental composition):"
  //<< oligomer_sp->elementalComposition();

  // At this point we have a brand new set of oligomers in the local
  // list of oligomers (oligomerList). We either stack these on top
  // of the previous ones, or we replace the previous ones with the
  // new ones.  Are we stacking new oligomers on top of the old
  // ones?

  if(!m_ui.stackOligomersCheckBox->isChecked())
    {
      // We are going to remove *all* the previous oligomers.
      mpa_oligomerTableViewModel->removeOligomers(0, -1);
    }

  // At this point we can set up the data to the treeview model.

  int initialOligomers = oligomerList.size();

  // qDebug() << __FILE__ << __LINE__
  //          << "initialOligomers:" << initialOligomers;

  // We are *transferring* the oligomers from oligomerList to the
  // list of oligomers that is in the model : we are not duplicating
  // the oligomers. When the transfer has been done, oligomerList
  // will be empty.
  int addedOligomers = mpa_oligomerTableViewModel->addOligomers(oligomerList);

  // qDebug() << __FILE__ << __LINE__
  //          << "addedOligomers:" << addedOligomers;

  if(initialOligomers != addedOligomers)
    qFatal("Fatal error at %s@%d. Aborting.", __FILE__, __LINE__);

  // Now that we have *transferred* (not copied) all the oligomers
  // in the model, we can clear the oligomerList without freeing the
  // instances it currently contain...
  oligomerList.clear();

  // Set focus to the treeView.
  m_ui.oligomerTableView->setFocus();

  QString title;

  int oligomerCount = mpa_oligomerTableViewModel->rowCount();

  if(!oligomerCount)
    title = tr("Oligomers (empty list)");
  else if(oligomerCount == 1)
    title = tr("Oligomers (one item)");
  else
    title = tr("Oligomers (%1 items)").arg(oligomerCount);

  m_ui.oligomerGroupBox->setTitle(title);

  // Update the fragmentation details so that we know how the oligos were
  // computed.
  updateFragmentationDetails(m_calcOptions);
}


void
FragmentationDlg::updateFragmentationDetails(
  const libXpertMass::CalcOptions &calcOptions)
{
  if(calcOptions.monomerEntities() & libXpertMass::MONOMER_CHEMENT_MODIF)
    m_ui.modificationsCheckBox->setCheckState(Qt::Checked);
  else
    m_ui.modificationsCheckBox->setCheckState(Qt::Unchecked);

  if(calcOptions.polymerEntities() & libXpertMass::POLYMER_CHEMENT_LEFT_END_MODIF)
    m_ui.leftModifCheckBox->setCheckState(Qt::Checked);
  else
    m_ui.leftModifCheckBox->setCheckState(Qt::Unchecked);

  if(calcOptions.polymerEntities() & libXpertMass::POLYMER_CHEMENT_RIGHT_END_MODIF)
    m_ui.rightModifCheckBox->setCheckState(Qt::Checked);
  else
    m_ui.rightModifCheckBox->setCheckState(Qt::Unchecked);

  if(calcOptions.monomerEntities() & libXpertMass::MONOMER_CHEMENT_CROSS_LINK)
    m_ui.crossLinksCheckBox->setCheckState(Qt::Checked);
  else
    m_ui.crossLinksCheckBox->setCheckState(Qt::Unchecked);
}


void
FragmentationDlg::updateOligomerSequence(QString *text)
{
  Q_ASSERT(text);

  m_ui.oligomerSequenceTextEdit->clear();
  m_ui.oligomerSequenceTextEdit->append(*text);
}


bool
FragmentationDlg::calculateTolerance(double mass)
{
  // Get the tolerance that is in its lineEdit.

  QString text     = m_ui.toleranceLineEdit->text();
  double tolerance = 0;
  bool ok          = false;

  if(!text.isEmpty())
    {
      // Convert the string to a double.

      ok        = false;
      tolerance = text.toDouble(&ok);

      if(!tolerance && !ok)
        return false;
    }
  else
    {
      m_tolerance = 0;
    }

  // What's the item currently selected in the comboBox?
  int index = m_ui.toleranceComboBox->currentIndex();

  if(index == 0)
    {
      // MASS_TOLERANCE_AMU
      m_tolerance = tolerance;
    }
  else if(index == 1)
    {
      // MASS_TOLERANCE_PCT
      m_tolerance = (tolerance / 100) * mass;
    }
  else if(index == 2)
    {
      // MASS_TOLERANCE_PPM
      m_tolerance = (tolerance / 1000000) * mass;
    }
  else
    Q_ASSERT(0);

  return true;
}


void
FragmentationDlg::filterOptions(bool checked)
{
  if(!checked)
    {
      mpa_proxyModel->setFilterKeyColumn(-1);

      mpa_proxyModel->applyNewFilter();

      m_ui.filteringOptionsFrame->setVisible(false);
    }
  else
    {
      m_ui.filteringOptionsFrame->setVisible(true);

      // In this case, set focus to the last focused widget in the
      // groupbox or the first widget in the groubox if this is the
      // first time the filtering is used.
      mp_focusWidget->setFocus();
    }
}


void
FragmentationDlg::filterOptionsToggled()
{
  bool isChecked = m_ui.filteringOptionsGroupBox->isChecked();

  m_ui.filteringOptionsGroupBox->setChecked(!isChecked);
  filterOptions(!isChecked);
}


void
FragmentationDlg::filterPattern()
{
  // First off, we have to get the pattern that is in the lineEdit.

  QString text = m_ui.filterPatternLineEdit->text();

  if(text.isEmpty())
    return;

  mpa_proxyModel->setPatternFilter(text);
  mpa_proxyModel->setFilterKeyColumn(0);
  mpa_proxyModel->applyNewFilter();

  mp_focusWidget = m_ui.filterPatternLineEdit;
}


void
FragmentationDlg::filterMonoMass()
{
  // First off, we have to get the mass that is in the lineEdit.

  QString text = m_ui.filterMonoMassLineEdit->text();

  if(text.isEmpty())
    return;

  // Convert the string to a double.

  bool ok     = false;
  double mass = text.toDouble(&ok);

  if(!mass && !ok)
    return;

  // At this point, depending on the item that is currently selected
  // in the comboBox, we'll have to actually compute the tolerance.

  if(!calculateTolerance(mass))
    return;

  mpa_proxyModel->setMonoFilter(mass);
  mpa_proxyModel->setTolerance(m_tolerance);

  mpa_proxyModel->setFilterKeyColumn(3);
  mpa_proxyModel->applyNewFilter();

  mp_focusWidget = m_ui.filterMonoMassLineEdit;
}


void
FragmentationDlg::filterAvgMass()
{
  // First off, we have to get the mass that is in the lineEdit.

  QString text = m_ui.filterAvgMassLineEdit->text();

  if(text.isEmpty())
    return;

  // Convert the string to a double.

  bool ok     = false;
  double mass = text.toDouble(&ok);

  if(!mass && !ok)
    return;

  // At this point, depending on the item that is currently selected
  // in the comboBox, we'll have to actually compute the tolerance.

  if(!calculateTolerance(mass))
    return;

  mpa_proxyModel->setAvgFilter(mass);
  mpa_proxyModel->setTolerance(m_tolerance);

  mpa_proxyModel->setFilterKeyColumn(4);
  mpa_proxyModel->applyNewFilter();

  mp_focusWidget = m_ui.filterAvgMassLineEdit;
}


void
FragmentationDlg::filterCharge()
{
  // First off, we have to get the charge that is in the lineEdit.

  QString text = m_ui.filterChargeLineEdit->text();

  if(text.isEmpty())
    return;

  // Convert the string to a int.

  bool ok    = false;
  int charge = text.toInt(&ok);

  if(!charge && !ok)
    return;

  mpa_proxyModel->setChargeFilter(charge);
  mpa_proxyModel->setFilterKeyColumn(5);
  mpa_proxyModel->applyNewFilter();

  mp_focusWidget = m_ui.filterChargeLineEdit;
}


void
FragmentationDlg::traceColorPushButtonClicked()
{

  QPushButton *colored_push_button = m_ui.colorSelectorPushButton;

  // Allow (true) the user to select a color that has been chosen already.
  QColor color = libXpertMassGui::ColorSelector::chooseColor(true);

  if(color.isValid())
    {
      QPalette palette = colored_push_button->palette();
      palette.setColor(QPalette::Button, color);
      colored_push_button->setAutoFillBackground(true);
      colored_push_button->setPalette(palette);
      colored_push_button->update();

      // Now prepare the color in the form of a QByteArray

      QDataStream stream(&m_colorByteArray, QIODevice::WriteOnly);

      stream << color;
    }
}


void
FragmentationDlg::massSpectrumSynthesisMenuActivated(int index)
{
  if(index == (int)MassSpectrumSynthesisActions::LOAD_ISOTOPIC_DATA)
    {
      loadIsotopicDataFromFile();
      return;
    }
  if(index == (int)MassSpectrumSynthesisActions::CONFIGURE_MASS_PEAK_SHAPER)
    return configureMassPeakShaper();
  else if(index == (int)MassSpectrumSynthesisActions::SYNTHESIZE_MASS_SPECTRA)
    return synthesizeMassSpectra();
  else if(index ==
          (int)MassSpectrumSynthesisActions::COPY_MASS_SPECTRUM_TO_CLIPBOARD)
    {
      if(!m_syntheticMassSpectrum.size())
        {
          m_ui.massSpectrumSynthesisComboBox->setItemText(
            (int)MassSpectrumSynthesisActions::COPY_MASS_SPECTRUM_TO_CLIPBOARD,
            "Mass spectrum not yet available");

          return;
        }

      QClipboard *clipboard = QApplication::clipboard();
      clipboard->setText(m_syntheticMassSpectrum.toString());
    }
  else if(index ==
          (int)MassSpectrumSynthesisActions::SAVE_MASS_SPECTRUM_TO_FILE)
    {
      if(!m_syntheticMassSpectrum.size())
        {
          m_ui.massSpectrumSynthesisComboBox->setItemText(
            (int)MassSpectrumSynthesisActions::SAVE_MASS_SPECTRUM_TO_FILE,
            "Mass spectrum not yet available");

          return;
        }

      QString file_path = QFileDialog::getSaveFileName(
        this,
        tr("Select file to export the mass spectrum  to"),
        QDir::homePath(),
        tr("Data files(*.xy *.XY)"));

      QFile file(file_path);

      if(!file.open(QIODevice::WriteOnly))
        {
          QMessageBox::information(0,
                                   tr("massXpert2 - Export mass spectrum"),
                                   tr("Failed to open file in write mode."),
                                   QMessageBox::Ok);
          return;
        }

      QTextStream stream(&file);
      stream.setEncoding(QStringConverter::Utf8);

      prepareResultsTxtString();

      stream << m_syntheticMassSpectrum.toString();

      file.close();
    }
  else
    qFatal("Programming error. Should never reach this point.");
}


void
FragmentationDlg::configureMassPeakShaper()
{
  if(mp_massPeakShaperConfigDlg == nullptr)
    {
      mp_massPeakShaperConfigDlg = new libXpertMassGui::MassPeakShaperConfigDlg(
        this, m_applicationName, "Mass peak shaper configuration");

      if(mp_massPeakShaperConfigDlg == nullptr)
        qFatal("Programming error. Failed to allocate the dialog window.");
    }

  // The signal below is only emitted when checking parameters worked ok.
  connect(
    mp_massPeakShaperConfigDlg,
    &libXpertMassGui::MassPeakShaperConfigDlg::updatedMassPeakShaperConfigSignal,
    [this](const libXpertMass::MassPeakShaperConfig &config) {
      m_massPeakShaperConfig = config;

      // qDebug().noquote() << "Our local copy of the config:"
      //<< m_massPeakShaperConfig.toString();
    });

  mp_massPeakShaperConfigDlg->activateWindow();
  mp_massPeakShaperConfigDlg->raise();
  mp_massPeakShaperConfigDlg->show();
}


bool
FragmentationDlg::loadIsotopicDataFromFile()
{
  QString file_name = QFileDialog::getOpenFileName(
    this, tr("Load User IsoSpec table"), QDir::home().absolutePath());

  if(file_name.isEmpty())
    {
      QMessageBox::warning(
        this,
        QString("%1 - %2").arg(m_applicationName).arg(m_windowDescription),
        tr("Failed to set the file name."),
        QMessageBox::Ok);

      return false;
    }

  libXpertMass::IsotopicDataUserConfigHandler handler;
  handler.loadData(file_name);

  msp_isotopicData = handler.getIsotopicData();

  if(msp_isotopicData == nullptr || !msp_isotopicData->size())
    {
      QMessageBox::warning(
        this,
        QString("%1 - %2").arg(m_applicationName).arg(m_windowDescription),
       "Failed to load the isotopic data.",
        QMessageBox::Ok);

      return false;
    }

  return true;
}


void
FragmentationDlg::synthesizeMassSpectra()
{

  // Clear the synthetic mass spectrum. Make sure the mass spectrum to clipboard
  // menu item is not "available". Same for to file.
  m_syntheticMassSpectrum.clear();
  m_ui.massSpectrumSynthesisComboBox->setItemText(
    (int)MassSpectrumSynthesisActions::COPY_MASS_SPECTRUM_TO_CLIPBOARD,
    "Mass spectrum not yet available");

  m_ui.massSpectrumSynthesisComboBox->setItemText(
    (int)MassSpectrumSynthesisActions::SAVE_MASS_SPECTRUM_TO_FILE,
    "Mass spectrum not yet available");

  setCursor(Qt::WaitCursor);

  if(!m_massPeakShaperConfig.resolve())
    {
      QMessageBox msgBox;
      QString msg("Please, configure the mass peak shaper.");
      msgBox.setText(msg);
      msgBox.exec();

      configureMassPeakShaper();
    }

  // All the selected oligomers need to be processed such that using both
  // their elemental composition and their charge, we create isotopic
  // clusters that we can then merge into a single mass spectrum.

  OligomerList selected_oligomers;

  std::size_t count =
    m_ui.oligomerTableView->selectedOligomers(&selected_oligomers, -1);

  if(!count)
    {
      QMessageBox msgBox;
      QString msg("Please, select at least one oligomer in the table view.");
      msgBox.setText(msg);
      msgBox.exec();

      setCursor(Qt::ArrowCursor);
      return;
    }

  // qDebug() << "Selected oligomers:" << count;

  // At this point, we need to get a handle on the isotopic data. Note how we
  // want a non-const shared pointer!

  if(msp_isotopicData == nullptr)
    msp_isotopicData = std::make_shared<libXpertMass::IsotopicData>(
      *mp_editorWnd->polChemDefRendering()->getPolChemDef()->getIsotopicDataCstSPtr());

  if(msp_isotopicData == nullptr)
    qFatal("Programming error. The isotopic data cannot be nullptr.");

  // Great, we now we have isotopes to do the calculations!

  // And now we can instantiate a cluster generator

  libXpertMass::IsotopicClusterGenerator isotopic_cluster_generator(
    msp_isotopicData);

  // Construct the formula/charge pairs needed for the isotopic cluster
  // calculations.
  std::vector<libXpertMass::FormulaChargePair> formula_charge_pairs;

  // For debugging purposes.
  // std::size_t oligomer_count = 0;

  for(int iter = 0; iter < selected_oligomers.size(); ++iter)
    {
      FragmentOligomerSPtr oligomer_sp =
        std::dynamic_pointer_cast<FragmentOligomer>(
          selected_oligomers.at(iter));

      int charge           = oligomer_sp->charge();
      QString formula_text = oligomer_sp->elementalComposition();

      // qDebug() << formula_text << charge << oligomer_sp->mono();

      // We do not want to calculate isotopic clusters for fragments that have a
      // minus part in their formula. This might happen when applying formulas
      // like -H20 or -NH3 to ions that do not contain enough such atoms in
      // their formula for the overall formula to keep being only positive. One
      // could end with something like (specifically with immonium ions that are
      // very small) CxHyNz-Ow

      libXpertMass::Formula formula(formula_text);

      // We want to check the formula "fresh", store the symbol/count pairs and
      // reset (although not really needed since we have created the formula at
      // the code line above).
      if(!formula.validate(msp_isotopicData, true, true))
        qFatal("Programming error. The formula failed to validate.");

      if(formula.hasNetMinusPart())
        {
          qDebug()
            << "The oligomer's formula has a net minus part: skipping it.";
          continue;
        }

      // Instantiate on-the-fly the formula/charge pair that is fed to the
      // generator.

      formula_charge_pairs.push_back(
        libXpertMass::FormulaChargePair(formula_text, charge));

      //++oligomer_count;
    }

  // qDebug() << "Prepared" << oligomer_count << "formula/charge pairs";

  // At this point, we can copy all the formula/charge pairs to the cluster
  // generator.

  isotopic_cluster_generator.setFormulaChargePairs(formula_charge_pairs);

  isotopic_cluster_generator.setIsotopicDataType(
    libXpertMass::IsotopicDataType::LIBRARY_CONFIG);

  double cumulated_probabilities = m_ui.cumulatedProbsDoubleSpinBox->value();
  isotopic_cluster_generator.setMaxSummedProbability(cumulated_probabilities);
  double normalization_intensity =
    m_ui.normalizeIntensityDoubleSpinBox->value();

  // Not needed here because it will be taken into account at peak shaping
  // time
  //isotopic_cluster_generator.setNormalizationIntensity(normalization_intensity);
  //qDebug() << "Set the generator's normalization intensity:"
           //<< normalization_intensity;

  // And now perform the work. For each formula/charge pair, the generator will
  // create an isotopic cluster shaped trace associated to the corresponding
  // charge in a libXpertMass::IsotopicClusterChargePair. All these pairs are stored
  // in a vector.

  count = isotopic_cluster_generator.run();

  if(!count)
    {
      qDebug() << "Failed to create any isotopic cluster.";

      QMessageBox::information(
        this,
        QString("%1 - %2").arg(m_applicationName).arg(m_windowDescription),
        "Failed to compute a single isotopic cluster.",
        QMessageBox::Ok);

      setCursor(Qt::ArrowCursor);
      return;
    }

  // qDebug() << "The number of clusters generated:" << count;

  std::vector<libXpertMass::IsotopicClusterChargePair>
    isotopic_cluster_charge_pairs =
      isotopic_cluster_generator.getIsotopicClusterChargePairs();

  // At this point we should use these pairs to create a shape for each. But
  // first reset to 1 the charge because that charge was already accounted for
  // at the generation of the cluster. We do not want to divide m/z again by
  // charge. If charge had been 1, that would be no problem, but let's say the
  // charge was 2, if we did maintain that charge to a value of 2, then we would
  // get a tetra-protonated species cluster.

  // Note how we ask a reference to the pair that is iterated into, otherwise we
  // would get a copy and we would lose the local charge modification.
  for(libXpertMass::IsotopicClusterChargePair &pair : isotopic_cluster_charge_pairs)
    pair.second = 1;

  qDebug();

  // Now instantiate the isotopic cluster shaper and set the clusters' data in
  // it.

  libXpertMass::IsotopicClusterShaper isotopic_cluster_shaper(
    isotopic_cluster_charge_pairs, m_massPeakShaperConfig);

  isotopic_cluster_shaper.setNormalizeIntensity(normalization_intensity);

  // And now run the shaper.
  m_syntheticMassSpectrum = isotopic_cluster_shaper.run();

  if(!m_syntheticMassSpectrum.size())
    {
      qDebug() << "The synthetic mass spectrum has not a single data point.";
      setCursor(Qt::ArrowCursor);
      return;
    }

  m_ui.massSpectrumSynthesisComboBox->setItemText(
    (int)MassSpectrumSynthesisActions::COPY_MASS_SPECTRUM_TO_CLIPBOARD,
    "Mass spectrum to clipboard");

  m_ui.massSpectrumSynthesisComboBox->setItemText(
    (int)MassSpectrumSynthesisActions::SAVE_MASS_SPECTRUM_TO_FILE,
    "Mass spectrum to file");

  setCursor(Qt::ArrowCursor);

  QString trace_title = m_ui.massSpectrumTitleLineEdit->text();

  if(trace_title.isEmpty())
    trace_title = mp_editorWnd->getSequenceName();

  if(trace_title.isEmpty())
    trace_title = mp_editorWnd->polymer()->filePath();

  emit displayMassSpectrumSignal(
    trace_title,
    m_colorByteArray,
    std::make_shared<const pappso::Trace>(m_syntheticMassSpectrum));
}


// The results-exporting functions. ////////////////////////////////
// The results-exporting functions. ////////////////////////////////
// The results-exporting functions. ////////////////////////////////
void
FragmentationDlg::exportResults(int index)
{
  // Remember that we had set up the combobox with the following strings:
  // << tr("To Clipboard")
  // << tr("To File")
  // << tr("Select File");

  if(index == 0)
    {
      exportResultsClipboard();
    }
  else if(index == 1)
    {
      exportResultsFile();
    }
  else if(index == 2)
    {
      selectResultsFile();
    }
  else
    Q_ASSERT(0);
}


void
FragmentationDlg::prepareResultsTxtString()
{
  // Set the delimiter to the char/string that is in the
  // corresponding text line edit widget.

  QString delimiter = m_ui.delimiterLineEdit->text();
  // If delimiter is empty, the function that will prepare the
  // string will put the default character, that is '$'.

  mpa_resultsString->clear();

  // We only put the header info if the user does not want to have
  // the data formatted for XpertMiner, which only can get the data
  // in the format :
  //
  // mass <delim> charge <delim> name <delim> coords
  //
  // Note that name and coords are optional.
  bool forXpertMiner         = false;
  libXpertMass::MassType massType = libXpertMass::MassType::MASS_NONE;

  forXpertMiner = m_ui.forXpertMinerCheckBox->isChecked();

  if(!forXpertMiner)
    {
      *mpa_resultsString += QObject::tr(
        "# \n"
        "# ---------------------------\n"
        "# Fragmentation: \n"
        "# ---------------------------\n");
    }
  else
    {
      // Then, we should ask whether the masses to be exported are
      // the mono or avg masses.

      if(m_ui.monoRadioButton->isChecked())
        massType = libXpertMass::MassType::MASS_MONO;
      else
        massType = libXpertMass::MassType::MASS_AVG;
    }

  bool withSequence = m_ui.withSequenceCheckBox->isChecked();

  QString *text = m_ui.oligomerTableView->selectedOligomersAsPlainText(
    delimiter, withSequence, forXpertMiner, massType);

  *mpa_resultsString += *text;

  delete text;
}


bool
FragmentationDlg::exportResultsClipboard()
{
  prepareResultsTxtString();

  QClipboard *clipboard = QApplication::clipboard();

  clipboard->setText(*mpa_resultsString, QClipboard::Clipboard);

  return true;
}


bool
FragmentationDlg::exportResultsFile()
{
  if(m_resultsFilePath.isEmpty())
    {
      if(!selectResultsFile())
        return false;
    }

  QFile file(m_resultsFilePath);

  if(!file.open(QIODevice::WriteOnly | QIODevice::Append))
    {
      QMessageBox::information(0,
                               tr("massXpert - Export Data"),
                               tr("Failed to open file in append mode."),
                               QMessageBox::Ok);
      return false;
    }

  QTextStream stream(&file);
  stream.setEncoding(QStringConverter::Utf8);

  prepareResultsTxtString();

  stream << *mpa_resultsString;

  file.close();

  return true;
}


bool
FragmentationDlg::selectResultsFile()
{
  m_resultsFilePath =
    QFileDialog::getSaveFileName(this,
                                 tr("Select file to export data to"),
                                 QDir::homePath(),
                                 tr("Data files(*.dat *.DAT)"));

  if(m_resultsFilePath.isEmpty())
    return false;

  return true;
}
//////////////////////////////////// The results-exporting functions.
//////////////////////////////////// The results-exporting functions.
//////////////////////////////////// The results-exporting functions.

} // namespace massxpert

} // namespace MsXpS
