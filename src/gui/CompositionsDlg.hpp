/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef COMPOSITIONS_DLG_HPP
#define COMPOSITIONS_DLG_HPP


/////////////////////// Qt includes
#include <QSortFilterProxyModel>


/////////////////////// Local includes
#include "ui_CompositionsDlg.h"
#include "AbstractSeqEdWndDependentDlg.hpp"
#include "SequenceEditorWnd.hpp"
#include "CompositionTreeViewModel.hpp"
#include "CompositionTreeViewSortProxyModel.hpp"


namespace MsXpS
{

namespace massxpert
{


  enum
  {
    TARGET_ELEMENTAL = 0,
    TARGET_MONOMERIC = 1
  };


  class CompositionTreeViewModel;
  class CompositionTreeViewSortProxyModel;

  class CompositionsDlg : public AbstractSeqEdWndDependentDlg
  {
    Q_OBJECT

    private:
    Ui::CompositionsDlg m_ui;

    // The results-exporting strings. ////////////////////////////////
    QString *mpa_resultsString;
    QString m_resultsFilePath;
    //////////////////////////////////// The results-exporting strings.

    libXpertMass::CalcOptions *mp_calcOptions;
    libXpertMass::IonizeRule *mp_ionizeRule;

    libXpertMass::CoordinateList m_coordinateList;

    QList<libXpertMass::Monomer *> m_monomerList;

    CompositionTreeViewModel *mpa_compositionTreeViewModel;
    CompositionTreeViewSortProxyModel *mpa_compositionProxyModel;
    CompositionTreeViewSortProxyModel *mpa_avgProxyModel;

    bool fetchValidateInputData();

    void writeSettings();
    void readSettings();

    public:
    CompositionsDlg(SequenceEditorWnd *editorWnd,
                    libXpertMass::Polymer *polymer,
                    /* no libXpertMass::PolChemDef **/
                    const QString &configSettingsFilePath,
                    const QString &applicationName,
                    const QString &description,
                    libXpertMass::CalcOptions *calcOptions,
                    libXpertMass::IonizeRule *ionizeRule);

    ~CompositionsDlg();

    bool initialize();

    void setupTreeView();

    void updateIonizationData();

    void freeMonomerList();

    // The results-exporting functions. ////////////////////////////////
    void prepareResultsTxtString(int);
    bool exportResultsClipboard();
    bool exportResultsFile();
    bool selectResultsFile();
    //////////////////////////////////// The results-exporting functions.

    public slots:
    void updateSelectionData();
    void monomericComposition();
    void elementalComposition();
    void exportResults(int);
  };

} // namespace massxpert

} // namespace MsXpS


#endif // COMPOSITIONS_DLG_HPP
