/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QApplication>
#include <QTranslator>
#include <QtGlobal>
#include <QTimer>
#include <QDebug>
#include <QFile>
#include <QStringList>


/////////////////////// Std includes
#include <iostream>


/////////////////////// Local includes
#include "nongui/globals.hpp"
#include "config.h"
#include "gui/ProgramWindow.hpp"
#include "gui/Application.hpp"


using std::cout;

namespace MsXpS
{

namespace massxpert
{

  void printGreetings();
  void printLicense();
  void printHelp();
  void printVersion();
  void printConfig(const QString & = QString());


  void
  printHelp()
  {
    QString help = QObject::tr("The following options are available:\n");
    help += QObject::tr("? | -h | --help : print this help\n");
    help += QObject::tr("-v | --version : print version\n");
    help += QObject::tr("-c | --config : print configuration\n");
    help += QObject::tr("\n");

    cout << help.toStdString();
  }


  void
  printGreetings()
  {
    QString version(VERSION);


    QString greetings = QObject::tr("massXpert2, version %1\n\n").arg(VERSION);

    greetings += QObject::tr("Type 'massXpert2 --help' for help\n\n");

    greetings += QObject::tr(
      "massXpert2 is Copyright 2000-2023\n"
      "by Filippo Rusconi.\n\n"
      "massXpert2 comes with ABSOLUTELY NO WARRANTY.\n"
      "massXpert2 is free software, "
      "covered by the GNU General\n"
      "Public License Version 3, "
      "and you are welcome to change it\n"
      "and/or distribute copies of it under "
      "certain conditions.\n"
      "Check the file COPYING in the distribution "
      "and/or the\n"
      "'Help/About(Ctrl+H)' menu item of the program.\n"
      "\nHappy massXpert'ing!\n\n");


    cout << greetings.toStdString();
  }


  void
  printVersion()
  {
    QString version = QObject::tr(
                        "massXpert2, version %1 -- "
                        "Compiled against Qt, version %2\n")
                        .arg(VERSION)
                        .arg(QT_VERSION_STR);

    cout << version.toStdString();
  }


  void
  printConfig(const QString &execName)
  {
    QString config = QObject::tr(
                       "massXpert2: "
                       "Compiled with the following configuration:\n"
                       "EXECUTABLE BINARY FILE: = %1\n"
                       "BIN_DIR = %2\n"
                       "DATA_DIR = %3\n"
                       "DOC_DIR = %5\n")
                       .arg(execName)
                       .arg(BIN_DIR)
                       .arg(DATA_DIR)
                       .arg(DOC_DIR);

    cout << config.toStdString();
  }

} // namespace massxpert

} // namespace MsXpS


int
main(int argc, char **argv)
{
  // Reads the massxpert2.qrc ressource file.
  Q_INIT_RESOURCE(massxpert2);

  QStringList fileNames;

  // Note that we start with iter = 1 because, the 0 is the program name.

  for(int iter = 1; iter < argc; ++iter)
    {
      QString argument = argv[iter];

      if(argument == "--help" || argument == "-h" || argument == "?")
        {
          MsXpS::massxpert::printHelp();
          return 0;
        }
      else if(argument == "--version" || argument == "-v")
        {
          MsXpS::massxpert::printVersion();
          return 0;
        }
      else if(argument == "--license" || argument == "-l")
        {
          MsXpS::massxpert::printGreetings();
          return 0;
        }
      else if(argument == "--config" || argument == "-c")
        {
          MsXpS::massxpert::printConfig(
            QString("%1 (run as %2)").arg(TARGET_NAME).arg(argv[0]));
          return 0;
        }
      else
        {
          // Check if the string is a filename.
          QFile file(argument);
          if(file.exists())
            {
              fileNames.append(argument);
            }
        }
    }

  // Qt stuff starts here.
  MsXpS::massxpert::Application application(argc, argv, "massXpert2");

  application.processEvents();

  MsXpS::massxpert::ProgramWindow mainWin(application.applicationName(), "Main program window");
  mainWin.show();

  if(fileNames.size())
    {
      for(int iter = 0; iter < fileNames.size(); ++iter)
        {
          QString fileName = fileNames.at(iter);

          mainWin.openSequence(fileName);
        }
    }
  else
    MsXpS::massxpert::printGreetings();

  return application.exec();
}
