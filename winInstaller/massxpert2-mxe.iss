[Setup]
AppName=massXpert2

; Set version number below
#define public version "8.5.0"
AppVersion={#version}

#define public arch "mingw64"
#define public platform "win7+"

;;;;;; On Wine, z: drive is configured to point to /home/rusconi
;;;;;; Check the winecfg window's Drives tab

#define sourceDir "z:/devel/massxpert2/development"

#define buildDir "z:/devel/massxpert2/build-area/mxe"

#define docBuildDir "z:/devel/massxpert2/build-area/unix/doc/user-manual"

#define mxeDllDir "z:/devel/mxe/dlls-and-stuff-for-packages"


; Set version number below
AppVerName=massXpert2 version {#version}
DefaultDirName={commonpf}\massXpert2
DefaultGroupName=massXpert2
OutputDir="{#sourceDir}\winInstaller"

OutputBaseFilename=massXpert2-{#arch}-{#platform}-v{#version}-setup

OutputManifestFile=massXpert2-{#arch}-{#platform}-v{#version}-setup-manifest.txt
ArchitecturesAllowed=x64
ArchitecturesInstallIn64BitMode=x64

LicenseFile="{#sourceDir}/LICENSE"
AppCopyright="Copyright (C) 2009-2023 Filippo Rusconi"

AllowNoIcons=yes
AlwaysShowComponentsList=yes
AllowRootDirectory=no
AllowCancelDuringInstall=yes
AppComments=massXpert2, by Filippo Rusconi"
AppContact="Filippo Rusconi, PhD, Research scientist at CNRS, France"
CloseApplications=yes
CreateUninstallRegKey=yes
DirExistsWarning=yes
WindowResizable=yes
WizardImageFile="{#sourceDir}\images\splashscreen-massxpert2-innosetup.bmp"
WizardImageStretch=yes

[Dirs]
Name: "{app}\data"
Name: "{app}\doc"

[Files]
Source: "{#mxeDllDir}/*"; DestDir: {app}; Flags: ignoreversion recursesubdirs;

Source: "{#sourceDir}/doc/history.html"; DestDir: {app}\doc;

Source: "{#buildDir}/src/massXpert2.exe"; DestDir: {app};

Source: "{#sourceDir}/data/*"; DestDir: {app}\data; Flags: recursesubdirs;

Source: "{#docBuildDir}/massxpert2-doc.pdf"; DestDir: {app}\doc;
Source: "{#docBuildDir}/html/user-manual/*"; Flags: recursesubdirs; DestDir: {app}\doc\html;

[Icons]
Name: "{group}\massXpert2"; Filename: "{app}\massXpert2.exe"; WorkingDir: "{app}"
Name: "{group}\Uninstall massXpert2"; Filename: "{uninstallexe}"

[Run]
Filename: "{app}\massXpert2.exe"; Description: "Launch massXpert2"; Flags: postinstall nowait unchecked

