message("BEGIN - UNIX toolchain with local XPERTMASS libraries")

message("")

set(XpertMass_FOUND 1)
set(XpertMass_INCLUDE_DIRS
"$ENV{HOME}/devel/xpertmass/development/src/XpertMass/includes")
set(XpertMass_LIBRARIES
"$ENV{HOME}/devel/xpertmass/build-area/unix/src/XpertMass/libXpertMass.so" )

if(NOT TARGET XpertMass::Core)

  add_library(XpertMass::Core UNKNOWN IMPORTED GLOBAL)
  set_target_properties(XpertMass::Core PROPERTIES
    IMPORTED_LOCATION ${XpertMass_LIBRARIES}
    INTERFACE_INCLUDE_DIRECTORIES ${XpertMass_INCLUDE_DIRS})

endif()

set(XpertMassGui_FOUND 1)
set(XpertMassGui_INCLUDE_DIRS
  "$ENV{HOME}/devel/xpertmass/development/src/XpertMassGui/includes")
set(XpertMassGui_LIBRARIES
  "$ENV{HOME}/devel/xpertmass/build-area/unix/src/XpertMassGui/libXpertMassGui.so")

if(NOT TARGET XpertMass::Gui)

  add_library(XpertMass::Gui UNKNOWN IMPORTED GLOBAL)
  set_target_properties(XpertMass::Gui PROPERTIES
    IMPORTED_LOCATION ${XpertMassGui_LIBRARIES}
    INTERFACE_INCLUDE_DIRECTORIES ${XpertMassGui_INCLUDE_DIRS})

endif()

message("")
message("END - UNIX toolchain with local XPERTMASS libraries")
