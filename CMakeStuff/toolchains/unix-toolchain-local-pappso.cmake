message("BEGIN - UNIX toolchain with local PAPPSO libraries")
message("")

set(PappsoMSpp_FOUND 1)
set(PappsoMSpp_INCLUDE_DIRS "$ENV{HOME}/devel/pappsomspp/development/src")
set(PappsoMSpp_LIBRARIES
  "$ENV{HOME}/devel/pappsomspp/build-area/unix/src/libpappsomspp.so.0")

if(NOT TARGET PappsoMSpp::Core)

  add_library(PappsoMSpp::Core UNKNOWN IMPORTED GLOBAL)
  set_target_properties(PappsoMSpp::Core PROPERTIES
    IMPORTED_LOCATION ${PappsoMSpp_LIBRARIES}
    INTERFACE_INCLUDE_DIRECTORIES ${PappsoMSpp_INCLUDE_DIRS})

endif()

set(PappsoMSppWidget_FOUND 1)
set(PappsoMSppWidget_LIBRARIES
  "$ENV{HOME}/devel/pappsomspp/build-area/unix/src/pappsomspp/widget/libpappsomspp-widget.so.0")

if(NOT TARGET PappsoMSpp::Widget)

  add_library(PappsoMSpp::Widget UNKNOWN IMPORTED GLOBAL)
  set_target_properties(PappsoMSpp::Widget PROPERTIES
    IMPORTED_LOCATION ${PappsoMSppWidget_LIBRARIES}
    INTERFACE_INCLUDE_DIRECTORIES ${PappsoMSpp_INCLUDE_DIRS})

endif()

include_directories(${include_directories} ${PappsoMSpp_INCLUDE_DIRS} ${PappsoMSpp_INCLUDE_DIRS})

message("")
message("END - UNIX toolchain with local PAPPSO libraries")
