message(STATUS "UNIX toolchain")

set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES /usr/include)
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES /usr/include)

# This is used throughout all the build system files
set(TARGET ${LOWCASE_PROJECT_NAME})

# Find the libIsoSpec library
find_package(IsoSpec++ GLOBAL REQUIRED)


if(PAPPSO_LOCAL_DEV)
  include(${CMAKE_TOOLCHAINS_PATH}/unix-toolchain-local-pappso.cmake)
else()
find_package(PappsoMSpp COMPONENTS Core Widget GLOBAL REQUIRED)
endif()


if(XPERTMASS_LOCAL_DEV)
  include(${CMAKE_TOOLCHAINS_PATH}/unix-toolchain-local-xpertmass.cmake)
else()
  find_package(XpertMass COMPONENTS Core Gui GLOBAL REQUIRED)
endif()

## INSTALL directories
set(BIN_DIR ${CMAKE_INSTALL_PREFIX}/bin)
set(DATA_DIR ${CMAKE_INSTALL_PREFIX}/share/${TARGET}/data)
set(DOC_DIR ${CMAKE_INSTALL_PREFIX}/share/doc/${TARGET})


# The appstream, desktop and icon files
install(FILES org.msxpertsuite.${TARGET}.desktop
  DESTINATION ${CMAKE_INSTALL_PREFIX}/share/applications)

install(FILES org.msxpertsuite.${TARGET}.appdata.xml
  DESTINATION ${CMAKE_INSTALL_PREFIX}/share/metainfo)

install(FILES images/icons/16x16/${TARGET}.png
  DESTINATION
  ${CMAKE_INSTALL_PREFIX}/share/icons/hicolor/16x16/apps)

install(FILES images/icons/32x32/${TARGET}.png
  DESTINATION
  ${CMAKE_INSTALL_PREFIX}/share/icons/hicolor/32x32/apps)

install(FILES images/icons/48x48/${TARGET}.png
  DESTINATION
  ${CMAKE_INSTALL_PREFIX}/share/icons/hicolor/48x48/apps)

install(FILES images/icons/64x64/${TARGET}.png
  DESTINATION
  ${CMAKE_INSTALL_PREFIX}/share/icons/hicolor/64x64/apps)


# Prepare the AppImage recipe file to be used elsewhere,
# for deploying mineXpert2 on non-Debian- or non-Ubuntu platforms.

configure_file(${CMAKE_SOURCE_DIR}/CMakeStuff/${CMAKE_PROJECT_NAME}-appimage-recipe.yml.in
  ${CMAKE_SOURCE_DIR}/appimage/${CMAKE_PROJECT_NAME}-appimage-recipe.yml @ONLY)

## Platform-dependent compiler flags:
include(CheckCXXCompilerFlag)

if (WITH_FPIC)
  add_definitions(-fPIC)
endif()

