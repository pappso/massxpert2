# This file should be included if the command line reads like this:
# x86_64-w64-mingw32.shared-cmake -DCMAKE_BUILD_TYPE=Release -DMXE=1 ..

MESSAGE("MXE (M cross environment) https://mxe.cc/")
message("Please run the configuration like this:")
message("x86_64-w64-mingw32.shared-cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Release ../../development")

set(HOME_DEVEL_DIR $ENV{HOME}/devel)
set(MXE_SHIPPED_DLLS_DIR "$ENV{HOME}/devel/mxe/dlls-and-stuff-for-packages")

set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES /home/rusconi/devel/mxe/usr/x86_64-w64-mingw32.shared/include)
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES /home/rusconi/devel/mxe/usr/x86_64-w64-mingw32.shared/include)

# This is used throughout all the build system files
set(TARGET ${CMAKE_PROJECT_NAME})

# Now that we have the TARGET variable contents, let's configure InnoSetup
CONFIGURE_FILE(${CMAKE_SOURCE_DIR}/CMakeStuff/${LOWCASE_PROJECT_NAME}-mxe.iss.in
  ${CMAKE_SOURCE_DIR}/winInstaller/${LOWCASE_PROJECT_NAME}-mxe.iss @ONLY)


set(IsoSpec++_FOUND 1)
set(IsoSpec++_INCLUDE_DIRS "${HOME_DEVEL_DIR}/isospec/development")
set(IsoSpec++_LIBRARIES "${MXE_SHIPPED_DLLS_DIR}/libIsoSpec++.dll")
if(NOT TARGET IsoSpec++::IsoSpec++)
  add_library(IsoSpec++::IsoSpec++ UNKNOWN IMPORTED)
  set_target_properties(IsoSpec++::IsoSpec++ PROPERTIES
    IMPORTED_LOCATION             "${IsoSpec++_LIBRARIES}"
    INTERFACE_INCLUDE_DIRECTORIES "${IsoSpec++_INCLUDE_DIRS}")
endif()


set(PappsoMSpp_FOUND 1)
set(PappsoMSpp_INCLUDE_DIRS "${HOME_DEVEL_DIR}/pappsomspp/development/src")
set(PappsoMSpp_LIBRARIES
  "${MXE_SHIPPED_DLLS_DIR}/libpappsomspp.dll")
if(NOT TARGET PappsoMSpp::Core)

  add_library(PappsoMSpp::Core UNKNOWN IMPORTED GLOBAL)
  set_target_properties(PappsoMSpp::Core PROPERTIES
    IMPORTED_LOCATION ${PappsoMSpp_LIBRARIES}
    INTERFACE_INCLUDE_DIRECTORIES ${PappsoMSpp_INCLUDE_DIRS})

endif()

set(PappsoMSppWidget_FOUND 1)
set(PappsoMSppWidget_LIBRARIES
  "${MXE_SHIPPED_DLLS_DIR}/libpappsomspp-widget.dll")
if(NOT TARGET PappsoMSpp::Widget)

  add_library(PappsoMSpp::Widget UNKNOWN IMPORTED GLOBAL)
  set_target_properties(PappsoMSpp::Widget PROPERTIES
    IMPORTED_LOCATION ${PappsoMSppWidget_LIBRARIES}
    INTERFACE_INCLUDE_DIRECTORIES ${PappsoMSpp_INCLUDE_DIRS})

endif()

include_directories(${include_directories} ${PappsoMSpp_INCLUDE_DIRS} ${PappsoMSpp_INCLUDE_DIRS})


set(XpertMass_FOUND 1)
set(XpertMass_INCLUDE_DIRS "${HOME_DEVEL_DIR}/xpertmass/development/src/XpertMass/includes")
set(XpertMass_LIBRARIES "${MXE_SHIPPED_DLLS_DIR}/libXpertMass.dll")
if(NOT TARGET XpertMass::Core)

  add_library(XpertMass::Core UNKNOWN IMPORTED GLOBAL)
  set_target_properties(XpertMass::Core PROPERTIES
    IMPORTED_LOCATION ${XpertMass_LIBRARIES}
    INTERFACE_INCLUDE_DIRECTORIES ${XpertMass_INCLUDE_DIRS})

endif()

set(XpertMassGui_INCLUDE_DIRS "${HOME_DEVEL_DIR}/xpertmass/development/src/XpertMassGui/includes")
set(XpertMassGui_LIBRARIES "${MXE_SHIPPED_DLLS_DIR}/libXpertMassGui.dll")
if(NOT TARGET XpertMass::Gui)

  add_library(XpertMass::Gui UNKNOWN IMPORTED GLOBAL)
  set_target_properties(XpertMass::Gui PROPERTIES
    IMPORTED_LOCATION ${XpertMassGui_LIBRARIES}
    INTERFACE_INCLUDE_DIRECTORIES ${XpertMassGui_INCLUDE_DIRS})

endif()

include_directories(${include_directories} ${XpertMass_INCLUDE_DIRS} ${XpertMassGui_INCLUDE_DIRS})



## We can build the package setup executable with this specific command.
add_custom_target(winpackage
      COMMAND wine $ENV{HOME}/.wine/drive_c/Program\ Files\ \(x86\)/Inno\ Setup\ 6/Compil32.exe /cc "z:devel/${LOWCASE_PROJECT_NAME}/development/winInstaller/${LOWCASE_PROJECT_NAME}-mxe.iss"
      WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}/winInstaller
      COMMENT "Build of the Windows Package Setup executable"
      VERBATIM)


## INSTALL directories
# This is the default on windows, but set it nonetheless.
set(CMAKE_INSTALL_PREFIX "C:/Program Files")

set(BIN_DIR ${CMAKE_INSTALL_PREFIX}/${CMAKE_PROJECT_NAME})

set(DOC_DIR ${CMAKE_INSTALL_PREFIX}/${CMAKE_PROJECT_NAME}/doc)

set(DATA_DIR ${CMAKE_INSTALL_PREFIX}/${CMAKE_PROJECT_NAME}/data)

# On Win10 all the code is relocatable.
remove_definitions(-fPIC)

