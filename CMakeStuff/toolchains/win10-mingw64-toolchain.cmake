message("WIN10-MINGW64 environment https://www.msys2.org/")
message("Please run the configuration like this:")
message("cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Debug ../development")

# Comment out because they make the build fail with #include_next math.h not
# found error.
#
# set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES "c:/msys64/mingw64/include")
# set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES "c:/msys64/mingw64/include")


# This is used throughout all the build system files
set(TARGET ${CMAKE_PROJECT_NAME})

set(HOME_DEVEL_DIR "$ENV{HOME}/devel")

CONFIGURE_FILE(${CMAKE_SOURCE_DIR}/CMakeStuff/${LOWCASE_PROJECT_NAME}-mingw64.iss.in
  ${CMAKE_SOURCE_DIR}/winInstaller/${LOWCASE_PROJECT_NAME}-mingw64.iss @ONLY)


set(IsoSpec++_FOUND 1)
set(IsoSpec++_INCLUDE_DIRS "${HOME_DEVEL_DIR}/isospec/development")
set(IsoSpec++_LIBRARIES "${HOME_DEVEL_DIR}/isospec/build-area/mingw64/IsoSpec++/libIsoSpec++.dll")
if(NOT TARGET IsoSpec++::IsoSpec++)
  add_library(IsoSpec++::IsoSpec++ UNKNOWN IMPORTED)
  set_target_properties(IsoSpec++::IsoSpec++ PROPERTIES
    IMPORTED_LOCATION             "${IsoSpec++_LIBRARIES}"
    INTERFACE_INCLUDE_DIRECTORIES "${IsoSpec++_INCLUDE_DIRS}")
endif()


set(PappsoMSpp_FOUND 1)
set(PappsoMSpp_INCLUDE_DIRS "${HOME_DEVEL_DIR}/pappsomspp/development/src")
set(PappsoMSpp_LIBRARIES
  "${HOME_DEVEL_DIR}/pappsomspp/build-area/mingw64/src/libpappsomspp.dll")
if(NOT TARGET PappsoMSpp::Core)

  add_library(PappsoMSpp::Core UNKNOWN IMPORTED GLOBAL)
  set_target_properties(PappsoMSpp::Core PROPERTIES
    IMPORTED_LOCATION ${PappsoMSpp_LIBRARIES}
    INTERFACE_INCLUDE_DIRECTORIES ${PappsoMSpp_INCLUDE_DIRS})

endif()

set(PappsoMSppWidget_FOUND 1)
set(PappsoMSppWidget_LIBRARIES
  "${HOME_DEVEL_DIR}/pappsomspp/build-area/mingw64/src/pappsomspp/widget/libpappsomspp-widget.dll")
if(NOT TARGET PappsoMSpp::Widget)

  add_library(PappsoMSpp::Widget UNKNOWN IMPORTED GLOBAL)
  set_target_properties(PappsoMSpp::Widget PROPERTIES
    IMPORTED_LOCATION ${PappsoMSppWidget_LIBRARIES}
    INTERFACE_INCLUDE_DIRECTORIES ${PappsoMSpp_INCLUDE_DIRS})

endif()

include_directories(${include_directories} ${PappsoMSpp_INCLUDE_DIRS} ${PappsoMSpp_INCLUDE_DIRS})


set(XpertMass_FOUND 1)
set(XpertMass_INCLUDE_DIRS "${HOME_DEVEL_DIR}/xpertmass/development/src/XpertMass/includes")
set(XpertMass_LIBRARIES "${HOME_DEVEL_DIR}/xpertmass/build-area/mingw64/src/XpertMass/libXpertMass.dll")
if(NOT TARGET XpertMass::Core)

  add_library(XpertMass::Core UNKNOWN IMPORTED GLOBAL)
  set_target_properties(XpertMass::Core PROPERTIES
    IMPORTED_LOCATION ${XpertMass_LIBRARIES}
    INTERFACE_INCLUDE_DIRECTORIES ${XpertMass_INCLUDE_DIRS})

endif()

set(XpertMassGui_INCLUDE_DIRS "${HOME_DEVEL_DIR}/xpertmass/development/src/XpertMassGui/includes")
set(XpertMassGui_LIBRARIES "${HOME_DEVEL_DIR}/xpertmass/build-area/mingw64/src/XpertMassGui/libXpertMassGui.dll")
if(NOT TARGET XpertMass::Gui)

  add_library(XpertMass::Gui UNKNOWN IMPORTED GLOBAL)
  set_target_properties(XpertMass::Gui PROPERTIES
    IMPORTED_LOCATION ${XpertMassGui_LIBRARIES}
    INTERFACE_INCLUDE_DIRECTORIES ${XpertMassGui_INCLUDE_DIRS})

endif()

include_directories(${include_directories} ${XpertMass_INCLUDE_DIRS} ${XpertMassGui_INCLUDE_DIRS})


## INSTALL directories
# This is the default on windows, but set it nonetheless.
set(CMAKE_INSTALL_PREFIX "C:/Program Files")
set(BIN_DIR ${CMAKE_INSTALL_PREFIX}/${TARGET})
set(DATA_DIR ${CMAKE_INSTALL_PREFIX}/${TARGET}/data)
# On Win, the doc dir is uppercase.
set(DOC_DIR ${CMAKE_INSTALL_PREFIX}/${TARGET}/doc)

# Resource file for the win icon.
if(NOT CMAKE_RC_COMPILER)
  set(CMAKE_RC_COMPILER windres.exe)
endif()

execute_process(COMMAND ${CMAKE_RC_COMPILER}
  -D GCC_WINDRES
  -I "${CMAKE_CURRENT_SOURCE_DIR}"
  -i "${CMAKE_SOURCE_DIR}/${LOWCASE_PROJECT_NAME}.rc"
  -o "${CMAKE_CURRENT_BINARY_DIR}/${LOWCASE_PROJECT_NAME}.obj"
  WORKING_DIRECTORY	${CMAKE_CURRENT_SOURCE_DIR})


# On Win10 all the code is relocatable.
remove_definitions(-fPIC)


