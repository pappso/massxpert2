# Packager

# Note how the doc/user-manual/DC-user-manual is listed here:
# this is a generated file (configure_file) that needs not be
# in the tarball. However, because it is a configure_file it is
# generated right while doing 'cmake archive', so it is not
# possible that it be absent from source dir!set(CPACK_PACKAGE_NAME "massxpert2")
set(CPACK_PACKAGE_NAME "massxpert2")
set(CPACK_PACKAGE_VENDOR "msXpertSuite")
set(CPACK_PACKAGE_VERSION "${VERSION}")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Software for polymer modelling and mass spectrometry simulations")
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_SOURCE_DIR}/LICENSE")
set(CPACK_RESOURCE_FILE_AUTHORS "${CMAKE_SOURCE_DIR}/AUTHORS")
set(CPACK_SOURCE_GENERATOR "TGZ")
set(CPACK_SOURCE_IGNORE_FILES

  debian/
  .cache/
  .git/
  .gitattributes
  libmass/.git$
  libmassgui/.git$
  .*kdev.*
  compile_commands.json
  doc/user-manual/DC-user-manual
  libmass.a
  libmassgui.a
  *~
  *.xmlformat
  massxpert2-doc.pdf
  winInstaller/.*setup.exe
  winInstaller/.*manifest.txt
  .*-swp

  ${CPACK_SOURCE_IGNORE_FILES})

set(CPACK_SOURCE_PACKAGE_FILE_NAME
  "${LOWCASE_PROJECT_NAME}-${CPACK_PACKAGE_VERSION}")

include(CPack)


