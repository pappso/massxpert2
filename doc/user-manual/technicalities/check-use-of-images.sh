#!/bin/bash

# Test if we are in a project directory:

if [ ! -f "DC-user-manual" ]
then
	echo "Please enter a project directory, either massxpert or minexpert."
	exit 1
fi


echo "For each image dir, the unused images will stored in a \"unused\" directory."

curDir=$(pwd)

printf "Current directory: ${curDir}\n"

pushd images/src/png || (echo "Failed to cd to images/src/png" ; exit 1)

# printf "Now in: $(pwd)\n"

mkdir -p unused

echo "Working in \"images/src/png\""

for file in *.png
do 
	grep ${file} ../../../xml/* > /dev/null 2>&1 
 
	if [ $? != 0 ]
 	then 
		mv -v ${file} unused
 	fi
done

popd


