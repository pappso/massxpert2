xsltproc  --output titlepage.templates.xsl  /usr/share/xml/docbook/stylesheet/docbook-xsl-ns/template/titlepage.xsl titlepage.templates.xml

# Then copy the output file to the proper xsl/fo directory with new name:
echo "#######################################################"
echo ""
echo "now run the following command:"
echo "cp titlepage.templates.xsl ../fo/book.titlepage.templates.xsl"
echo ""
echo "#######################################################"

