project=massxpert2

# This path represents the local file even if it is a symbolic link
# This path should be the source tree, not the binary build tree.
mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
current_dir := $(notdir $(patsubst %/,%,$(dir $(mkfile_path))))

pwd=$(CURDIR)

# This should be the top *source* dir where
# the 'make' command is issued. Typically,
# /home/rusconi/devel/minexpert2/development.
topSrcDir=${pwd}

# This is the build directory that should be passed
# to Daps and where it will ouptut all of its artifacts.
buildDir=$(BUILD_DIR)

okularPid=okular.pid

pdfFilePath=${buildDir}/user-manual
pdfFileName=user-manual_color_en.pdf
pdfFilePathName=${pdfFilePath}/${pdfFileName}
htmlDirPath=${buildDir}/user-manual/html/${project}-user-manual

default: all

all: pdf html

all_f: pdf_f html_f

all_f_r: pdf_f_r html_f_r

.PHONY: reader
reader:
	okular --geometry 1268x1099-0-34 $(pdfFilePathName) & echo $$! > ${pdfFilePath}/okular.pid

# Make the pdf file without forcing without reload
pdf:
	daps --builddir=${buildDir} -d ${topSrcDir}/DC-user-manual --verbosity=3 pdf
	cp -fv $(pdfFilePathName) ${pdfFilePath}/${project}-doc.pdf
	cp -fv $(pdfFilePathName) ${topSrcDir}/${project}-doc.pdf

# Make the pdf file with forcing remaking all the targets and without reload
pdf_f:
	daps --force --builddir=${buildDir} -d ${topSrcDir}/DC-user-manual --verbosity=3 pdf
	cp -fv $(pdfFilePathName) ${pdfFilePath}/${project}-doc.pdf
	cp -fv $(pdfFilePathName) ${topSrcDir}/${project}-doc.pdf

# Reload with okular variant
pdf_r: pdf
	(cd ${pdfFilePath} && qdbus org.kde.okular-$(shell cat ${okularPid}) /okular reload)

# Reload with okular variant
pdf_f_r: pdf_f
	(cd ${pdfFilePath} && qdbus org.kde.okular-$(shell cat ${okularPid}) /okular reload)

.PHONY: html
html:
	daps --builddir=${buildDir} -d ${topSrcDir}/DC-user-manual --verbosity=3 html --static

.PHONY: html_f
html_f: html
	daps --force --builddir=${buildDir} -d ${topSrcDir}/DC-user-manual --verbosity=3 html --static

.PHONY: html_r
html_r: html
	firefox-reload-page.sh

.PHONY: html_f_r
html_f_r: html_f
	firefox-reload-page.sh

package_html:
	daps -d ${topSrcDir}/DC-user-manual package-html

check_dir:
	echo "mkfile_path: $(mkfile_path)"
	echo "current_dir: $(current_dir)"

	if [ -d "xml" ];\
	 	then \
		echo "Not in a user manual top source directory.";\
		exit 1;\
		fi

