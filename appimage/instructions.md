# Creation of AppImage images of the software project.

There are two protocols. The first protocol is to be used when the AppImage recipe file works fine,
even across versions and years.

If something starts failing, maybe time has come to craft a new recipe from scratch. For this use the second protocol, below.

## Creation of an AppImage with a recipe that works

The last time I did the protocol,  this one worked. See below for another one
that once worked.

Make sure all the dependencies are available as Debian packages, in particular
the libpappsomspp and qcustomplot libs. See the list of included packages in the mineXpert2-appimage-recipe.yml.in
recipe in CMakeStuff:

- libc6:amd64
- minexpert2
- minexpert2-doc
- graphicsmagick-imagemagick-compat
- libgomp1
- libfreetype6
- libqt6svg6
- libqt6xml6
- libqt6core5compat6
- libqt6printsupport6
- libqcustomplot2.1-qt6
- libisospec++2t64
- libpappsomspp0
- libpappsomspp-widget0


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Optionally upgrade to latest version of AppImage builder (still 1.1.0 at the time of writing):

cd /all-inst-packages/appimage
wget -O appimage-builder-x86_64.AppImage https://github.com/AppImageCrafters/appimage-builder/releases/download/v1.1.0/appimage-builder-1.1.0-x86_64.AppImage
chmod +x appimage-builder-x86_64.AppImage
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Go to the appimage directory in top source dir, where the .in file has been converted to the rightly versioned recipe file by CMake.

The AppDir directory should be there alone, with these instructions.

Run the command:

% /all-inst-packages/appimage/appimage-builder-x86_64.AppImage --recipe mineXpert2-appimage-recipe.yml

The AppImage file should be created in this same directory.

## Generation of an AppImage recipe

First make sure that as many software dependencies as possible are presently packed and available in testing or in reprepro.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Optionally upgrade to latest version of AppImage builder (still 1.1.0 at the time of writing):

cd /all-inst-packages/
wget -O appimage-builder-x86_64.AppImage https://github.com/AppImageCrafters/appimage-builder/releases/download/v1.1.0/appimage-builder-1.1.0-x86_64.AppImage
chmod +x appimage-builder-x86_64.AppImage
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Then, make the software in build-area/unix as usual, with the documentation prepared also:

% cd ../build-area/unix

% cmake ../../development -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=Release\
-DBUILD_USER_MANUAL=1 -DLOCAL_DEV=0 -DMAKE_TESTS=0 -DWARN_AS_ERROR=0 && make -j20

% make && make usermanual

Create locally an AppDir directory:

% mkdir AppDir

% make install DESTDIR=AppDir

And now instruct the program to generate the recipe on the basis of what it sees in the directory:

% /all-inst-packages/appimage-builder-x86_64.AppImage --generate

Rename the genearated file to a meaningful file name

% mv AppImageBuilder.yml mineXpert2-appimage-recipe.yml

Edit the file so that all the sources.list apt sources in /etc/apt that have no reason to be there are removed.

At the time of writing, this works:

    sources:
    - sourceline: deb http://deb.debian.org/debian/ trixie main contrib non-free non-free-firmware
    - sourceline: deb [trusted=yes] http://reprepro.localhost/ unstable main

At this point, check the first protocol above and verify that the AppImage image is created fine.




